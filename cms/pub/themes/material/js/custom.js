var unknown_form_error = 'An unknown error was encountered or your form token has expired. Please try refreshing this page and submit the form again.';

$(function() {

	// frame buster
    if( self != top ) { 
        top.location = self.location;
	}
	
	// tooltip
	$("body").tooltip({ selector: '[tooltip-toggle=tooltip]' });

    // disable caching of ajax content
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal'); 
        restore_modal();
	});

});


/**
 * handle single image dropzone
 * 
 * @param {*} element 
 * @param {*} endpoint 
 */
function handle_single_image_dropzone(element, endpoint) {
	
    var dropzone_element = $('#'+element+'_dropzone');
     
    if(dropzone_element.length > 0) // element exists
    {  
        // Prevent dropzone autodiscover
        Dropzone.autoDiscover = false;

        dropzone_element.dropzone({
            url: endpoint,
            clickable: true,
            addRemoveLinks: true,
			maxFiles: 1,
			acceptedFile: 'image/*',
            init: function() {

                this.on('success', function(file, data) { // Successfully process the file
 
                    var response = $.parseJSON(data);
					
                    if(response.status == 'success') {
                        $('#'+element).val(response.image);
                    } else {
                        $('#error-'+element).html('<span class="text-danger">'+response.error+'</span>');
                    }

                });

				// Remove file when it exceeds the allowed a
				this.on('maxfilesexceeded', function(file) {
					this.removeFile(file);
				});

                // TODO
                this.on("removedfile", function(file) {
					$('#'+element).val(''); 
					$('#error-'+element).html('');
				});
            }
        });
    }
}

/**
 * handle single image dropzone
 * 
 * @param {*} element 
 * @param {*} endpoint 
 */
function handle_single_image_select_dropzone(element, endpoint) {
	
    var dropzone_element = $('#'+element+'_dropzone');
     
    if(dropzone_element.length > 0) // element exists
    {  
        // Prevent dropzone autodiscover
        Dropzone.autoDiscover = false;

        dropzone_element.dropzone({
            url: endpoint,
            clickable: true,
            addRemoveLinks: true,
			maxFiles: 1,
			maxFilesize: 2,
			acceptedFile: 'image/*',
            init: function() {

                this.on('success', function(file, data) { // Successfully process the file
 
					var response = $.parseJSON(data);
					
                    if(response.status == 'success') {
						$('#'+element).val(response.image);
						$('#photo-wrapper').html('<a href="'+site_url+'files/images/select" data-toggle="modal" data-target="#modal"><img src="'+site_url+response.image+'" class="img-responsive" /></a>');

                    } else {
                        $('#error-'+element).html('<span class="text-danger">'+response.error+'</span>');
					}
					
                });

				// Remove file when it exceeds the allowed a
				this.on('maxfilesexceeded', function(file) {
					this.removeFile(file);
				}); 

                // TODO
                this.on("removedfile", function(file) {
					$('#'+element).val(''); 
					$('#error-'+element).html('');
				});
            }
        });
    }
}

function handle_single_file_dropzone(element, endpoint) {
	
    var dropzone_element = $('#'+element+'_dropzone');
     
    if(dropzone_element.length > 0) // element exists
    {
        // Prevent dropzone autodiscover
        Dropzone.autoDiscover = false;

        dropzone_element.dropzone({
            url: endpoint,
            clickable: true,
            addRemoveLinks: true,
            maxFiles: 1,
            init: function() {

                this.on('success', function(file, data) { // Successfully process the file
 
					var response = $.parseJSON(data);

                    if(response.status == 'success') {
						$('#'+element).val(response.file);
					} else if(response.status == 'failed') {
						alertify.error(response.message);
                    } else {
						alertify.error(response.error);
                        $('#error-'+element).html('<span class="text-danger">'+response.error+'</span>');
					}
					
                });

				// Remove file when it exceeds the allowed files
				this.on('maxfilesexceeded', function(file) {
					this.removeFile(file);
				});

                // TODO
                this.on("removedfile", function(file) {
					$('#error-'+element).html('');
				});
            }
        });
    }
}


/**
 * 
 * @param element 
 * @param type
 * 
 * @author Robert Christian Obias <robertchristianobias@gmail.com> 
 */
function editor(element, type)
{
	var toolbar;
	    
	if(type == 'simple') {
		toolbar = 'undo redo | forecolor bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | mybutton link | paste code | table fontawesome';
	} else {
		toolbar = 'insertfile undo redo | styleselect forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link emoticons mybutton documents videos fontawesome';
	}

	if ( element.length ) {

	}

    // tinymce.init({
	// 	selector:  element,
	// 	theme:     "modern",
	// 	statusbar: true,
	// 	menubar:   false,
	// 	relative_urls: false,
	// 	remove_script_host : false,
	// 	convert_urls : true,
	// 	plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
	// 			  'searchreplace wordcount visualblocks visualchars code',
	// 			  'insertdatetime media nonbreaking save table contextmenu directionality',
	// 			  'emoticons template paste textcolor colorpicker textpattern noneditable'],
	// 	toolbar: toolbar,
	// 	image_advtab: true,
	// 	setup: function (editor) {
	// 		editor.addButton('mybutton', {
	// 			text: '',
	// 			icon: 'image',
	// 			onclick: function () {
	// 				$('#modal').modal({
	// 					remote: site_url + 'files/images/rte/mce'
	// 				})
	// 			}
	// 		});
	// 		editor.addButton('documents', {
	// 			text: '',
	// 			icon: 'newdocument',
	// 			onclick: function () {
	// 				$('#modal').modal({
	// 					remote: site_url + 'files/documents/rte/mce'
	// 				})
	// 			}
	// 		});
	// 		editor.addButton('videos', {
	// 			text: '',
	// 			icon: 'media',
	// 			onclick: function () {
	// 				$('#modal').modal({
	// 					remote: site_url + 'files/videos/rte/mce'
	// 				})
	// 			}
	// 		});
	// 	},
	// 	content_css: site_url + 'themes/aceadmin/css/tinymce.css',
	// 	//content_css: 'components/font-awesome/css/font-awesome.css',
	// 	noneditable_noneditable_class: 'fa',
	// });
}

/**
 * Convert title to slug
 * 
 * @param str
 * 
 * @author Robert Christian Obias <robertchristianobias@gmail.com> 
 */
function slug(str)
{
    var $slug   = '';
    var trimmed = $.trim(str);
	
	$slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
	replace(/^-|-$/g, '');
	
    return $slug.toLowerCase();
}

function restore_modal()
{
	$('.modal-content').empty();

	var html = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">Loading...</h4></div><div class="modal-body"><div class="text-center"><img src="' + site_url + 'ui/images/loading3.gif" alt="Loading..." /><p>Loading...</p></div></div>';

	$('.modal-content').html(html);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



// resolve Select2 and Bootstrap 3 modal conflict
$.fn.modal.Constructor.prototype.enforceFocus =function(){};