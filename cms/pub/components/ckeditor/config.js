/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    
    config.uiColor = '#EEEEEE';
    config.toolbar =
        [
            { name: 'basicstyles', items : [ 'Font','FontSize','Bold','Italic','Underline','Styles','Format'] },
            { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Blockquote','-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
            { name: 'links',       items : [ 'Link', 'Unlink' ] },
            { name: 'insert',      items : [ 'Table', 'Image'] },
            { name: 'colors',      items : [ 'TextColor','BGColor', 'Source'] },
            { name: 'forms',       items : [ 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'Youtube']}
        ];
        
    config.extraPlugins = 'forms';
    config.extraPlugins = 'dialog';
    config.extraPlugins = 'dialogui';
    config.extraPlugins = 'fakeobjects';
    config.extraPlugins = 'youtube';
    config.youtube_responsive = true;
    config.filebrowserImageBrowseUrl = site_url + 'components/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl = site_url + 'components/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl      = site_url + 'components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = site_url + 'components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = site_url + 'components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

};
