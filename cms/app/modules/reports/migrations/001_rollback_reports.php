<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		robertchristianobias@gmail.com
 */
class Migration_Rollback_reports extends CI_Migration 
{

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{

	}

	public function down()
	{
		
	}
}