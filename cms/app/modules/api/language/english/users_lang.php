<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * API Users Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Develop';

// Labels
$lang['username']					= 'Username';
$lang['password']					= 'Password';
$lang['api_key']					= 'API Key';
$lang['status']						= 'Status';

// Buttons
$lang['button_add']					= 'Add API User';
$lang['button_update']				= 'Update API User';
$lang['button_delete']				= 'Delete API User';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'API Users';
$lang['index_subhead']				= 'List of all API Users';
$lang['index_id']					= 'ID';
$lang['index_username']				= 'Username';
$lang['index_password']				= 'Password';
$lang['index_api_key']				= 'API Key';
$lang['index_status']				= 'Status';


// View Function
$lang['view_heading']				= 'View API User';

// Add Function
$lang['add_heading']				= 'Add API User';
$lang['add_success']				= 'API User has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit API User';
$lang['edit_success']				= 'API User has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete API User';
$lang['delete_confirm']				= 'Are you sure you want to delete this API User?';
$lang['delete_success']				= 'API User has been successfully deleted';