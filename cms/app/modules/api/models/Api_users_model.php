<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Api_users_model Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Api_users_model extends BF_Model 
{

	protected $table_name			= 'api_users';
	protected $key					= 'id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'au_created_on';
	protected $created_by_field		= 'au_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'au_modified_on';
	protected $modified_by_field	= 'au_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'au_deleted';
	protected $deleted_by_field		= 'au_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'api_users.id', 
			'api_users.username',
			'api_users.api_key',
			'api_users.status',

			'au_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'au_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)',
		);

		return $this->join('users as creator', 'creator.id = au_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = au_modified_by', 'LEFT')
					->datatables($fields);
	}
}