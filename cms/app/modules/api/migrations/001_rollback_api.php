<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Migration_Rollback_api extends CI_Migration 
{

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{

	}

	public function down()
	{
		
	}
}