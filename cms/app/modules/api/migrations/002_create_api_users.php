<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Migration_Create_api_users extends CI_Migration 
{
	private $_table = 'api_users';

	private $_permissions = array(
		array('API Users Link', 'api.api.link'), 
		array('API Users Link', 'api.users.link'), 
		array('List API Users', 'api.users.list'), 
		array('View API User', 'api.users.view'),
		array('Add API User', 'api.users.add'),
		array('Edit API User', 'api.users.edit'),
		array('Delete API User', 'api.users.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'none', // none if parent or single menu
			'menu_text' 		=> 'API', 
			'menu_link' 		=> 'api', 
			'menu_perm' 		=> 'api.api.link', 
			'menu_icon' 		=> 'fa fa-exchange', 
			'menu_order' 		=> 8, 
			'menu_active' 		=> 1
		),
		array(
			'menu_parent'		=> 'api', // none if parent or single menu
			'menu_text' 		=> 'API Users', 
			'menu_link' 		=> 'api/users', 
			'menu_perm' 		=> 'api.users.link', 
			'menu_icon' 		=> 'fa fa-exchange', 
			'menu_order' 		=> 1, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'id' 				=> array('type' => 'SMALLINT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'username'			=> array('type' => 'VARCHAR', 'constraint' => 40, 'null' => FALSE),
			'password'			=> array('type' => 'VARCHAR', 'constraint' => 40, 'null' => FALSE),
			'api_key'			=> array('type' => 'VARCHAR', 'constraint' => 40, 'null' => FALSE),
			'level'				=> array('type' => 'TINYINT', 'constraint' => 2, 'unsigned' => TRUE, 'null' => FALSE),
			'ignore_limits'		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'is_private_key'	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'ip_addresses'		=> array('type' => 'TEXT', 'null' => FALSE),
			'date_created'		=> array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
			'status'			=> array('type' => 'VARCHAR', 'constraint' => 20, 'null' => FALSE),

			'au_created_by' 		=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'au_created_on' 		=> array('type' => 'DATETIME', 'null' => TRUE),
			'au_modified_by' 		=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'au_modified_on' 		=> array('type' => 'DATETIME', 'null' => TRUE),
			'au_deleted' 			=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE, 'default' => 0),
			'au_deleted_by' 		=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->add_key('username');
		$this->dbforge->add_key('password');
		$this->dbforge->add_key('api_key');
		$this->dbforge->add_key('level');
		$this->dbforge->add_key('ignore_limits');
		$this->dbforge->add_key('is_private_key');
		$this->dbforge->add_key('status');

		$this->dbforge->add_key('au_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}