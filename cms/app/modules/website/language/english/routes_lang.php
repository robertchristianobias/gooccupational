<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Routes Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Routes'; 

// Labels
$lang['route_id']			= 'Id';
$lang['route_slug']			= 'Slug';
$lang['route_controller']			= 'Controller';

// Buttons
$lang['button_add']					= 'Add Route';
$lang['button_update']				= 'Update Route';
$lang['button_delete']				= 'Delete Route';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Routes';
$lang['index_subhead']				= 'List of all Routes';
$lang['index_id']			= 'Id';
$lang['index_slug']			= 'Slug';
$lang['index_controller']			= 'Controller';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Route';

// Add Function
$lang['add_heading']				= 'Add Route';
$lang['add_success']				= 'Route has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Route';
$lang['edit_success']				= 'Route has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Route';
$lang['delete_confirm']				= 'Are you sure you want to delete this route?';
$lang['delete_success']				= 'Route has been successfully deleted';