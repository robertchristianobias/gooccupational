<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Banners Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Website';

// Labels
$lang['banner_banner_group_id']		= 'Banner Group Id';
$lang['banner_title']               = 'Title';
$lang['banner_thumb']				= 'Thumb';
$lang['banner_image']				= 'Image';
$lang['banner_caption']				= 'Caption';
$lang['banner_button']              = 'Button';
$lang['banner_link']				= 'Link';
$lang['banner_target']				= 'Target';
$lang['banner_status']				= 'Status';

// Buttons
$lang['button_add']					= 'Add Banner';
$lang['button_update']				= 'Update Banner';
$lang['button_delete']				= 'Delete Banner';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Banners';
$lang['index_subhead']				= 'Manage your frontend banners here';
$lang['index_id']					= 'ID';
$lang['index_banner_group_id']		= 'Banner Group ID';
$lang['index_thumb']				= 'Thumb';
$lang['index_image']				= 'Image';
$lang['index_caption']				= 'Caption';
$lang['index_link']					= 'Link';
$lang['index_target']				= 'Target';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Banner';

// Add Function
$lang['add_heading']				= 'Add Banner';
$lang['add_success']				= 'Banner has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Banner';
$lang['edit_success']				= 'Banner has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Banner';
$lang['delete_confirm']				= 'Are you sure you want to delete this banner?';
$lang['delete_success']				= 'Banner has been successfully deleted';

// Reorder Function
$lang['reorder_heading']			= 'Reorder Banner';
$lang['reorder_success']			= 'Banner order has been successfully updated';