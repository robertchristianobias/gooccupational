<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Partials Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Website';

// Labels
$lang['partial_title']				= 'Title';
$lang['partial_content']			= 'Content';
$lang['partial_status']				= 'Status';
$lang['partial_photo']              = 'Photo';

// Buttons
$lang['button_add']					= 'Add Partial';
$lang['button_update']				= 'Update Partial';
$lang['button_delete']				= 'Delete Partial';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Partials';
$lang['index_subhead']				= 'Manage your website partials here';
$lang['index_id']					= 'ID';
$lang['index_title']				= 'Title';
$lang['index_content']				= 'Content';
$lang['index_status']				= 'Status';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Partial';

// Add Function
$lang['add_heading']				= 'Add Partial';
$lang['add_success']				= 'Partial has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Partial';
$lang['edit_success']				= 'Partial has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Partial';
$lang['delete_confirm']				= 'Are you sure you want to delete this partial?';
$lang['delete_success']				= 'Partial has been successfully deleted';