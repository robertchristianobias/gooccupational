<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Website';

// Labels
$lang['page_banner_group_id']       = 'Banner Group';
$lang['page_parent_id']				= 'Parent Page';
$lang['page_title']					= 'Page Title';
$lang['page_slug']					= 'Slug';
$lang['page_content']				= 'Content';
$lang['page_posted_on']				= 'Posted On';
$lang['page_status']				= 'Status';
$lang['page_layout']				= 'Page Layout';
$lang['page_sidebar_id']			= 'Page Sidebar';
$lang['page_template']              = 'Template';
$lang['page_photo']                 = 'Photo';

// Buttons
$lang['button_add']					= 'Add Page';
$lang['button_update']				= 'Save Changes';
$lang['button_delete']				= 'Delete Page';
$lang['button_edit_this']			= 'Edit This';
$lang['button_draft']               = 'Draft';
$lang['button_publish']             = 'Publish';

// Index Function
$lang['index_heading']				= 'Pages';
$lang['index_subhead']				= 'Manage your frontend pages here';
$lang['index_id']					= 'ID';
$lang['index_title']				= 'Title';
$lang['index_slug']					= 'Slug';
$lang['index_url']					= 'Slug';
$lang['index_content']				= 'Content';
$lang['index_posted_on']			= 'Posted On';
$lang['index_status']				= 'Status';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Page';

// Add Function
$lang['add_heading']				= 'Add Page';
$lang['add_success']				= 'Page has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Page';
$lang['edit_success']				= 'Page has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Page';
$lang['delete_confirm']				= 'Are you sure you want to delete this page?';
$lang['delete_success']				= 'Page has been successfully deleted';