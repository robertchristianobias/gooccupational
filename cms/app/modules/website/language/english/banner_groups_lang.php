<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Banner Groups Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Website';

// Labels
$lang['banner_group_name']			= 'Name';

// Buttons
$lang['button_add']					= 'Add Banner Group';
$lang['button_update']				= 'Update Banner Group';
$lang['button_delete']				= 'Delete Banner Group';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Banner Groups';
$lang['index_subhead']				= 'List of all banner groups';
$lang['index_id']					= 'ID';
$lang['index_name']					= 'Name';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Banner Group';

// Add Function
$lang['add_heading']				= 'Add Banner Group';
$lang['add_success']				= 'Banner Group has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Banner Group';
$lang['edit_success']				= 'Banner Group has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Banner Group';
$lang['delete_confirm']				= 'Are you sure you want to delete this Banner Group?';
$lang['delete_success']				= 'Banner Group has been successfully deleted';