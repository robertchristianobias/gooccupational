<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Widgets Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Website';

// Labels
$lang['website_name']				= 'Website Name';
$lang['website_email']				= 'Website Email';
$lang['website_url']				= 'Website URL';
$lang['website_theme']				= 'Website Theme';
$lang['recaptcha_secret_key']       = 'Secret Key';
$lang['recaptcha_site_key']         = 'Site Key';

// Settings Function
$lang['settings_heading']			= 'Website Settings';
$lang['settings_subhead']			= 'Configure the website module here';
$lang['settings_success']			= 'Website settings have been successfully updated';