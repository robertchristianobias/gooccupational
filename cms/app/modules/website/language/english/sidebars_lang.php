<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sidebars Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Website';

// Labels
$lang['sidebar_name']				= 'Sidebar Name';

// Buttons
$lang['button_add']					= 'Add Sidebar';
$lang['button_update']				= 'Update Sidebar';
$lang['button_delete']				= 'Delete Sidebar';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Sidebars';
$lang['index_subhead']				= 'Manage your frontend sidebar widgets here';
$lang['index_id']					= 'ID';
$lang['index_name']					= 'Group Name';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Sidebar';

// Add Function
$lang['add_heading']				= 'Add Sidebar';
$lang['add_success']				= 'Sidebar has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Sidebar';
$lang['edit_success']				= 'Sidebar has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Sidebar';
$lang['delete_confirm']				= 'Are you sure you want to delete this sidebar?';
$lang['delete_success']				= 'Sidebar has been successfully deleted';