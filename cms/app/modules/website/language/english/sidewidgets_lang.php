<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sidebars Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Website';

// Labels
$lang['sidewidget_type']			= 'Type';
$lang['sidewidget_name']			= 'Name';
$lang['sidewidget_source_id']		= 'Source';
$lang['sidewidget_status']			= 'Status';

// Buttons
$lang['button_add']					= 'Add Side Widget';
$lang['button_update']				= 'Update Side Widget';
$lang['button_delete']				= 'Delete Side Widget';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Sidebars';
$lang['index_subhead']				= 'Manage your frontend sidebar widgets here';
$lang['index_id']					= 'ID';
$lang['index_name']					= 'Group Name';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Side Widget';

// Add Function
$lang['add_heading']				= 'Add Side Widget';
$lang['add_success']				= 'Side Widget has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Side Widget';
$lang['edit_success']				= 'Side Widget has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Side Widget';
$lang['delete_confirm']				= 'Are you sure you want to delete this sidebar?';
$lang['delete_success']				= 'Side Widget has been successfully deleted';