<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<?php if (isset($sidebars)): ?>
			<?php foreach ($sidebars as $sidebar): ?>
				<li class="<?php echo ($sidebar->sidebar_id == $sidebar_id) ? 'active' : ''; ?>"><a href="<?php echo site_url('website/sidebars/' . $sidebar->sidebar_id); ?>"><?php echo $sidebar->sidebar_name; ?></a></li>
			<?php endforeach; ?>
		<?php endif; ?>
		<li class=""><a data-toggle="modal" data-target="#modal" href="<?php echo site_url('website/sidebars/form/add'); ?>"><i class="fa fa-plus"></i> Add Sidebar</a></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active">
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">Available Widgets</div>
						<div class="panel-body">
							<ul class="list-group" id="available">
								<li class="list-group-item" data-type="Page" data-source="pages" data-sidewidget-id="0">
									<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
									<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
									<span class="list-group-item-text">Page</span>
								</li>
								<li class="list-group-item" data-type="Posts" data-source="categories" data-sidewidget-id="0">
									<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
									<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
									<span class="list-group-item-text">Posts</span>
								</li>
								<li class="list-group-item" data-type="Navigation" data-source="navigroups" data-sidewidget-id="0">
									<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
									<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
									<span class="list-group-item-text">Navigation</span>
								</li>
								<li class="list-group-item" data-type="Banner" data-source="banner_groups" data-sidewidget-id="0">
									<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
									<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
									<span class="list-group-item-text">Banner</span>
								</li>
								<li class="list-group-item" data-type="Partial" data-source="partials" data-sidewidget-id="0">
									<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
									<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
									<span class="list-group-item-text">Partial</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-primary">
						<div class="panel-heading">Active Widgets</div>
						<div class="panel-body">
							<ul class="list-group sortable" data-sidewidget-sidebar-id="<?php echo $sidebar_id ?>">
								<?php if ($sidewidgets) : ?>
								<?php foreach ($sidewidgets as $key => $sidewidget) : ?>
									<li class="list-group-item" data-type="<?php echo $sidewidget->sidewidget_type; ?>" data-source="<?php echo $sidewidget->sidewidget_source_table; ?>" data-sidewidget-id="<?php echo $sidewidget->sidewidget_id; ?>" id="<?php echo $sidewidget->sidewidget_id; ?>">
										<span class="badge badge-danger btn-widget-delete"><i class="fa fa-times"></i></span>
										<span class="badge badge-warning btn-widget-edit"><i class="fa fa-pencil"></i></span>
										<span class="list-group-item-text"><?php echo $sidewidget->sidewidget_type; ?>: <?php echo ($sidewidget->sidewidget_name) ? $sidewidget->sidewidget_name : 'No Title'; ?></span>
									</li>
								<?php endforeach; ?>
								<?php endif; ?>
							</ul>
						</div>
					</div>
					<div class="top-margin4">
						<?php if ($sidebar_id != 1): ?> 
							<a href="<?php echo site_url('website/sidebars/form/edit/' . $sidebar_id); ?>" class="btn btn-warning" data-toggle="modal" data-target="#modal" title="Edit Group"><i class="fa fa-pencil"></i> Edit Sidebar</a>
							<a href="<?php echo site_url('website/sidebars/delete/' . $sidebar_id); ?>" class="btn btn-danger" data-toggle="modal" data-target="#modal" title="Delete Group"><i class="fa fa-trash"></i></a>
						<?php endif; ?>
					</div>
				</div>

				<div class="col-sm-4">
					<h4>Instruction:</h4>
					<ol id="instruction">
						<li>Add the navigation items from Categories, Pages or Custom Link</li>
						<li>Drag and drop the navigation items</li>
						<li><strong>Save changes</strong></li>
					</ol>

					<h4 class="top-margin4">Embed Codes:</h4>
					<p>PHP: <br /><code>&lt;?php echo frontend_sidebars(<?php echo $sidebar_id; ?>); ?&gt;</code></p>
					<p>Content: <br /><code>##frontend_sidebars(<?php echo $sidebar_id; ?>)##</code></p>
				</div>
			</div>
		</div>
	</div>
</div>