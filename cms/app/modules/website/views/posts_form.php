<?php echo form_open(current_url(), 'class="" id="posts-form"'); ?>
	<div class="nav-tabs-custom bottom-margin">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_post" data-toggle="tab"><span class="fa fa-file-text"></span> Content</a></li>
			
			<?php if (isset($record->post_id) AND isset($record->post_metatag_id)): ?>
				<li><a href="<?php echo site_url('metatags/form/website/posts/' . $record->post_id); ?>" data-toggle="modal" data-target="#modal" class="btn btn-info"><span class="fa fa-cog"></span> Meta Tags</a></li>
			<?php endif; ?>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_post">
				<div class="row">

					<div class="col-sm-9">
						<div class="form-group">
							<label class="control-label" for="post_title"><?php echo lang('post_title'); ?>:</label> 
							<div class="row">
								<div class="col-md-11">
									<?php echo form_input(array('id'=>'post_title', 'name'=>'post_title', 'value'=>set_value('post_title', isset($record->post_title) ? $record->post_title : '', FALSE), 'class'=>'form-control meta-title'));?>
								</div>
								<div class="col-md-1">
									<input type="color" name="post_title_color" value="" class="form-control" />
								</div>
							</div>
							<div id="error-post_title"></div>
						</div>
						<div class="form-group">
							<label class="control-label" for="post_excerpt"><?php echo lang('post_excerpt'); ?>:</label>
							<div class="row">
								<div class="col-md-11">
									<?php echo form_textarea(array('id'=>'post_excerpt', 'name'=>'post_excerpt', 'rows'=>'7', 'value'=>set_value('post_excerpt', isset($record->post_excerpt) ? $record->post_excerpt : '', FALSE), 'class'=>'form-control')); ?>
								</div>
								<div class="col-md-1">
									<input type="color" name="post_excerpt_color" value="" class="form-control" />
								</div>
							</div>
							<div id="error-post_excerpt"></div>
						</div>

						<div class="form-group">
							<label class="control-label" for="post_content"><?php echo lang('post_content'); ?>:</label>
							<?php echo form_textarea(array('id'=>'post_content', 'name'=>'post_content', 'rows'=>'15', 'value'=>set_value('post_content', isset($record->post_content) ? $record->post_content : '', FALSE), 'class'=>'form-control meta-description ckeditor')); ?>
							<div id="error-post_content"></div>
						</div>

					</div>

					<div class="col-sm-3">
						<div class="form-group">
							<label class="control-label"><?php echo lang('post_order'); ?>:</label>
							<?php echo form_input('post_order', set_value('post_order', isset($record->post_order) ? $record->post_order : ''), 'class="form-control" id="post_order"'); ?>
							<div id="error-post_order"></div>
						</div>
						<div class="form-group">
							<label class="control-label" for="post_categories"><?php echo lang('post_categories'); ?>:</label>
							<div id="categories">
								<?php if ($categories): ?>
									<?php foreach ($categories as $category): ?>
										<div class="checkbox checkbox-primary">
										<input class="post_categories" name="post_categories[]" type="checkbox" value="<?php echo $category->category_id; ?>" <?php echo set_checkbox('post_categories', 1, (in_array($category->category_id, $current_categories)) ? TRUE : FALSE); ?> /> 

											<label>
											<?php echo $category->category_name; ?>
											</label>
										</div>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
							<div id="error-post_categories"></div>
						</div>

						<div class="form-group">
							<input type="hidden" name="post_photo" id="images_photo" value="<?php echo isset($record->post_photo) ? $record->post_photo : ''; ?>" />
							<label class="control-label bottom-margin" for="post_photo"><?php echo lang('post_photo')?>:</label>
							<?php if( ! empty($record->post_photo)) { ?>
								<div class="custom-dropzone" id="photo-wrapper">
									<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
										<img src="<?php echo site_url($record->post_photo); ?>" class="img-responsive" />
									</a>
								</div>
							<?php } else { ?>
								<div class="custom-dropzone" id="photo-wrapper">
									<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
										<span>Click to upload</span>
									</a>
								</div>
							<?php } ?>
							<div class="help-text">Accepts jpg, png with 2mb filesize.</div>
							<div id="error-post_photo"></div>
						</div>
						<div class="top-margin5">
							<?php if ($action == 'add'): ?>
								<button class="btn btn-success btn-sm submit" id="post" data-status="Published" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-plus"></i> <?php echo lang('button_publish')?>
								</button>
							<?php elseif ($action == 'edit'): ?>
								<button class="btn btn-success btn-sm submit" id="post" data-status="Published" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-save"></i> <?php echo lang('button_publish')?>
								</button>
							<?php endif; ?>
							<button class="btn btn-primary btn-sm submit" data-status="Draft" type="submit" data-loading-text="<?php echo lang('processing')?>">
								<?php echo lang('button_draft')?>
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="tab_seo">
				
			</div>
		</div>
	</div>
<?php echo form_close(); ?>