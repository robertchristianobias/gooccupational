/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

$(function() {

	editor('#post_content', 'simple');


	// handles the post action
	$('#post').click(function(e){
		// change the button to loading state
		var btn = $(this);
			form = $('#posts-form');
			form_data = form.serializeArray();
			form_data.push({
				name:  'post_content',
				value:  CKEDITOR.instances['post_content'].getData()
			});

			post_url = form.attr('action');

		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(post_url, form_data,
		function(data, status) {
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				
				// shows the error message
				swal("Opps!", o.message, "error");

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				if (o.action == 'add') {

					swal("Success", o.message, "success");
					window.location.replace(site_url + 'website/posts/form/edit/' + o.id);
				} else {
					// shows the success message
					swal("Success", o.message, "success");
				}
			}
			// reset the button
			btn.button('reset');

		}).fail(function() {
			// shows the error message
			swal("Opps!", unknown_form_error, "error");

			// reset the button
			btn.button('reset');
		});
	});


});