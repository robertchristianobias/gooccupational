/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Dropzone.autoDiscover = false;

$(function() {


	$('#page_title').blur(function() {
		var page_title = $(this).val();

		$('#page_slug').val(slug(page_title));
	});

	// handles the post action
	$('.submit').click(function(e){

		// change the button to loading state
		var btn = $(this);
			form = $('#pages-form');
			form.push({
				name:  'page_content',
				value:  CKEDITOR.instances['page_content'].getData()
			});

		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(form.attr('action'), form.serializeArray(),
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				
				// shows the error message
				swal("Opps!", o.message, "error");

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				if (o.action == 'add') {
					window.location.replace(app_url + 'website/pages/form/edit/' + o.id);
				} else {
					// shows the success message
					swal("Success", o.message, "success");
				}
			}
			// reset the button
			btn.button('reset');
		}).fail(function() {
			// shows the error message
			alertify.alert('Error', unknown_form_error);

			// reset the button
			btn.button('reset');
		});
	});

	// initialize tinymce
	// tinymce.init({
	// 	selector: "#page_content",
	// 	theme: "modern",
	// 	statusbar: true,
	// 	menubar: true,
	// 	relative_urls: false,
	// 	remove_script_host : false,
	// 	convert_urls : true,
	// 	plugins: [
	// 		'advlist autolink lists link image charmap print preview hr anchor pagebreak',
	// 		'searchreplace wordcount visualblocks visualchars code',
	// 		'insertdatetime media nonbreaking save table contextmenu directionality',
	// 		'emoticons template paste textcolor colorpicker textpattern'
	// 	],
	// 	toolbar1: 'insertfile undo redo | styleselect forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link emoticons images documents videos',
	// 	image_advtab: true,
	// 	setup: function (editor) {
	// 		editor.addButton('images', {
	// 			text: 'Image',
	// 			icon: 'image',
	// 			onclick: function () {
	// 				$('#modal').modal({
	// 					remote: app_url + 'files/images/rte/mce'
	// 				})
	// 			}
	// 		});
	// 		editor.addButton('documents', {
	// 			text: 'Document',
	// 			icon: 'newdocument',
	// 			onclick: function () {
	// 				$('#modal').modal({
	// 					remote: app_url + 'files/documents/rte/mce'
	// 				})
	// 			}
	// 		});
	// 		editor.addButton('videos', {
	// 			text: 'Video',
	// 			icon: 'media',
	// 			onclick: function () {
	// 				$('#modal').modal({
	// 					remote: app_url + 'files/videos/rte/mce'
	// 				})
	// 			}
	// 		});
	// 	},
	// 	content_css: app_url + 'themes/aceadmin/css/tinymce.css'
	// });

	// select page layout
	var layout = $("#page_layout");
	var option = layout.find('option');

	if(option.is(':selected')) page_layout(layout);

	layout.on("change", function(e){
		page_layout($(this));
	});

	function page_layout(elem)
	{
		var layout = elem.find("option:selected").val();		

		if (layout == 'right_sidebar' || layout == 'left_sidebar') {
			$("#frontend_sidebar").removeClass('hide');
		} else {
			$("#frontend_sidebar").addClass('hide');
			$("#frontend_sidebar").find('option:eq(0)').prop('selected', true);
		}
	}
});