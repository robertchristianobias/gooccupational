/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
$(function() {

	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "posts/datatables",
		"lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
		"pagingType": "full_numbers",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next',
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "asc" ]],
		"aoColumnDefs": [
			
			{
				"aTargets": [0],
				"sClass": "col-md-1 text-center",
			},

			{
				"aTargets": [1],
				"mRender": function (data, type, full) {
					return '<a href="posts/form/edit/'+full[0]+'" tooltip-toggle="tooltip" data-placement="top" title="Edit">' + data + '</a>';
				},
			},

			{
				"aTargets": [3],
				 "mRender": function (data, type, full) {
					if (data == 'Posted') {
						return '<div class="badge badge-success">' + data + '</div>';
					}
					else if (data == 'Draft') {
						return '<div class="badge badge-warning">' + data + '</div>';
					}
					else {
						return '<div class="badge badge-default">' + data + '</div>';
					}
				 },
				 "sClass": "col-md-1 text-center",
			},

			{
				"aTargets": [4],
				"bSortable": false,
				 "mRender": function (data, type, full) {
				 	html = '<a href="' + site_url + 'metatags/form/website/posts/' + full[0] + '" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Meta Tags" class="btn btn-info btn-icon-anim btn-circle"><span class="fa fa-cogs"></span></a> ';
					html += '<a href="posts/form/edit/'+full[0]+'" tooltip-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-warning btn-icon-anim btn-circle"><span class="fa fa-pencil"></span></a> ';
					html += '<a href="posts/delete/'+full[0]+'" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-icon-anim btn-circle"><span class="fa fa-trash-o"></span></a>';

					return html;
				},
				"sClass": "col-md-2 text-center",
			},
		]
	});

	// positions the button next to searchbox
	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {        
		// eg. destroys the wysiwyg editor
	});

});