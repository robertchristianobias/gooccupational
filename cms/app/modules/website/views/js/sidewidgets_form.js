/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

$(function() {

	// console.log($(this).children().children().children())

	// handles the submit action
	$('#submit').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(ajax_url, {
			sidewidget_type: $('#sidewidget_type').val(),
			sidewidget_name: $('#sidewidget_name').val(),
			sidewidget_source_id: $('#sidewidget_source_id').val(),
			sidewidget_status: $('#sidewidget_status').val(),
			[csrf_name]: $('input[name=' + csrf_name + ']').val(),
		},
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// shows the error message
				alertify.error(o.message);

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {

				// adds the title and widget id
				var title = ($('#sidewidget_name').val()) ? $('#sidewidget_name').val() : 'No Title';
				$('#' + sidewidget_temp_id + ' .list-group-item-text').text($('#sidewidget_type').val() + ': ' + title);
				$('#' + sidewidget_temp_id).attr('data-widget-id', o.widget_id);

				// submits the order
				var sidewidget_ids = new Array();
				$.each($('#' + sidewidget_temp_id).parent().children('.box'), function(i,e) {
					sidewidget_ids.push($(this).attr('data-widget-id'));
				});
				console.log(sidewidget_ids)

				// submit
				$.post(app_url + 'website/sidewidgets/reorder', { sidewidget_ids: sidewidget_ids } );

				// closes the modal
				$('#modal').modal('hide'); 

				// reload page
				document.location.reload();
			}
		}).fail(function() {
			// shows the error message
			alertify.alert('Error', unknown_form_error);

			// reset the button
			btn.button('reset');
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});

});