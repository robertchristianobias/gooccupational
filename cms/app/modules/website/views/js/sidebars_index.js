/**
 * @package     RainCode
 * @version     1.1
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2016, RC MediaPH
 * @link        http://www.rcmediaph.com
 */
$(function() {
	$( "#available .list-group-item" ).draggable({
		connectToSortable: ".sortable",
		helper: "clone",
		revert: "invalid",
		start: function(event, ui){
			// adds ui-helper class
			$(ui.helper).addClass("ui-helper");
		},
		stop: function(event, ui){
			
			// displays the buttons
			$('.ui-helper .badge').removeClass("hide");

			// adds widget id
			var random = Math.floor((Math.random() * 1000) + 1);
			$(ui.helper).addClass("widget-" + random);
			$(ui.helper).attr("id", "widget-" + random);
			
			var sidewidget_sidebar_id = $(event.toElement).closest('.list-group').attr('data-sidewidget-sidebar-id');

			if (sidewidget_sidebar_id !== undefined) {
				// opens the add widget modal
				$(".widget-" + random + " .btn-widget-edit").trigger( "click" );
			}
		}
	});

	$( ".sortable" ).sortable({
		revert: true,
		connectWith: ".sortable",
		stop: function(event, ui) {
			var sidewidget_sidebar_id = $(event.toElement).closest('.list-group').attr('data-sidewidget-sidebar-id');
			console.log(sidewidget_sidebar_id);

			if (sidewidget_sidebar_id !== undefined) {
				// get the widget ids
				var sidewidget_ids = new Array();
				$.each($(event.toElement).closest('.list-group').children('.list-group-item'), function(i,e) {
					sidewidget_ids.push($(this).attr('data-sidewidget-id'));
				});
				console.log(sidewidget_ids);

				// submit
				$.post(app_url + 'website/sidewidgets/reorder', { sidewidget_ids: sidewidget_ids, sidewidget_sidebar_id: sidewidget_sidebar_id } );
			}
		}
	});

	$( "#available, .sortable" ).disableSelection();
});

// add or edit the widget
$('body').on("click", '.btn-widget-edit', function() {
	sidewidget_type 		= $(this).parent().attr('data-type');
	sidewidget_sidebar_id 	= $(this).parent().parent().attr('data-sidewidget-sidebar-id');
	sidewidget_source 		= $(this).parent().attr('data-source');
	sidewidget_id 			= $(this).parent().attr('data-sidewidget-id');

	sidewidget_temp_id = $(this).parent().attr('id');

	var action = (sidewidget_id !== '0') ? 'edit' : 'add';
	var id = (sidewidget_id !== '0') ? sidewidget_id : 0; 

	$('#modal').modal({
		remote: app_url + 'website/sidewidgets/form/' + action + '/' + sidewidget_type + '/' + sidewidget_sidebar_id + '/' + sidewidget_source + '/' + id 
	})
});

// delete the widget
$('body').on("click", '.btn-widget-delete', function() {
	var sidewidget_id = $(this).parent().attr('data-sidewidget-id');

	if (sidewidget_id > 0) {
		$('#modal').modal({
			remote: app_url + 'website/sidewidgets/delete/' + sidewidget_id 
		})
	} else {
		$(this).parent().remove();
	}
});