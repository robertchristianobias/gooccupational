/**
 * @package     RainCode
 * @version     1.1
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2016, RC MediaPH
 * @link        http://www.rcmediaph.com
 */
$(function() {
	$( "#available .list-group-item" ).draggable({
		connectToSortable: ".sortable",
		helper: "clone",
		revert: "invalid",
		start: function(event, ui){
			// adds ui-helper class
			$(ui.helper).addClass("ui-helper");
		},
		stop: function(event, ui){
			
			// displays the buttons
			$('.ui-helper .badge').removeClass("hide");

			// adds widget id
			var random = Math.floor((Math.random() * 1000) + 1);
			$(ui.helper).addClass("widget-" + random);
			$(ui.helper).attr("id", "widget-" + random);
			
			var section = $(event.toElement).closest('.list-group').attr('data-widget-section');

			if (section !== undefined) {
				// opens the add widget modal
				$(".widget-" + random + " .btn-widget-edit").trigger( "click" );
			}
		}
	});

	$( ".sortable" ).sortable({
		revert: true,
		connectWith: ".sortable",
		stop: function(event, ui) {
			var section = $(event.toElement).closest('.list-group').attr('data-widget-section');
			console.log(section);

			if (section !== undefined) {
				// get the widget ids
				var widget_ids = new Array();
				$.each($(event.toElement).closest('.list-group').children('.list-group-item'), function(i,e) {
					widget_ids.push($(this).attr('data-widget-id'));
				});
				console.log(widget_ids);

				// submit
				$.post(app_url + 'website/widgets/reorder', { widget_ids: widget_ids, widget_section: section } );
			}
		}
	});

	$( "#available, .sortable" ).disableSelection();
});

// add or edit the widget
$('body').on("click", '.btn-widget-edit', function() {
	// console.log($(this).parent().parent().parent().parent().html())
	widget_type = $(this).parent().attr('data-type');
	widget_section = $(this).parent().parent().attr('data-widget-section');
	widget_source = $(this).parent().attr('data-source');
	widget_id = $(this).parent().attr('data-widget-id');
	
	widget_temp_id = $(this).parent().attr('id');

	var action = (widget_id !== '0') ? 'edit' : 'add';
	var id = (widget_id !== '0') ? widget_id : 0; 

	$('#modal').modal({
		remote: app_url + 'website/widgets/form/' + action + '/' + widget_type + '/' + widget_section + '/' + widget_source + '/' + id 
	})
});

// delete the widget
$('body').on("click", '.btn-widget-delete', function() {
	var widget_id = $(this).parent().attr('data-widget-id');

	if (widget_id > 0) {
		$('#modal').modal({
			remote: app_url + 'website/widgets/delete/' + widget_id 
		})
	} else {
		$(this).parent().remove();
	}
});