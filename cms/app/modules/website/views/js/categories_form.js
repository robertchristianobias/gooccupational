/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
$(function() {

	// handles the submit action
	$('#submit').click(function(e){

		// change the button to loading state
		var btn = $(this);
			form = $('#categories-form');
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(form.attr('action'), form.serializeArray(),
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				
				// reset the button
				btn.button('reset');

				// shows the error message
				swal("Opps!", o.message, "error");

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				// refreshes the datatables
				$('#datatables').dataTable().fnDraw();

				// closes the modal
				$('#modal').modal('hide'); 

				// restores the modal content to loading state
				restore_modal(); 

				// shows the success message
				swal("Success", o.message, "success");
			}
		}).fail(function() {
			// shows the error message
			swal("Opps!", o.message, "error");

			// reset the button
			btn.button('reset');
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});

	// select page layout
	var layout = $("#category_layout");
	var option = layout.find('option');

	if(option.is(':selected')) page_layout(layout);

	layout.on("change", function(e){
		page_layout($(this));
	});

	function page_layout(elem)
	{
		var layout = elem.find("option:selected").val();		

		if (layout == 'right_sidebar' || layout == 'left_sidebar') {
			$("#frontend_sidebar").removeClass('hide');
		} else {
			$("#frontend_sidebar").addClass('hide');
			$("#frontend_sidebar").find('option:eq(0)').prop('selected', true);
		}
	}
});