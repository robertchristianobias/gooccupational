/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

$(function() {

	editor('#partial_content', 'simple');

	// handles the post action
	$('#submit').click(function(e) {

		// change the button to loading state
		var btn = $(this);
		var form = $('#partials-form').serializeArray();
			form.push({
				name:  'partial_content',
				value:  CKEDITOR.instances['partial_content'].getData()
			});
			
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(post_url, form,
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				
				// shows the error message
				swal("Opps!", o.message, "error");

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				if (o.action == 'add') {
					swal("Success", o.message, "success");
					window.location.replace(app_url + 'website/partials/form/edit/' + o.id);
				} else {
					// shows the success message
					swal("Success", o.message, "success");
				}
			}
			// reset the button
			btn.button('reset');
		}).fail(function() {
			// shows the error message
			alertify.alert('Error', unknown_form_error);

			// reset the button
			btn.button('reset');
		});
	});

});