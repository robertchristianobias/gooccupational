<?php echo form_open(current_url(), 'id="banners-form"'); ?>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#info"><span class="fa fa-question"></span> Info</a></li>
	</ul>
	<div class="tab-content">
		<div id="info" class="tab-pane fade in active">
			<p>&nbsp;</p>
			<div class="row">
				<div class="col-md-9">
					<div class="form-group">
						<label class="control-label" for="banner_title"><?php echo lang('banner_title')?>:</label>
						<?php echo form_input(array('id'=>'banner_title', 'name'=>'banner_title', 'value'=>set_value('banner_title', isset($record->banner_title) ? $record->banner_title : ''), 'class'=>'form-control'));?>
						<div id="error-banner_title"></div>
					</div>
					<div class="form-group">
						<label class="control-label" for="banner_caption"><?php echo lang('banner_caption')?>:</label>
						<?php echo form_textarea(array('id'=>'banner_caption', 'name'=>'banner_caption', 'value'=>set_value('banner_caption', isset($record->banner_caption) ? $record->banner_caption : ''), 'class'=>'form-control'));?>
						<div id="error-banner_caption"></div>
					</div>
				
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<input type="hidden" name="banner_image" id="images_photo" value="<?php echo isset($record->banner_image) ? $record->banner_image : ''; ?>" />
						<label class="control-label bottom-margin" for="banner_image"><?php echo lang('banner_image')?>:</label>
						<?php if( ! empty($record->banner_image)) { ?>
							<div class="custom-dropzone" id="photo-wrapper">
								<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
									<img src="<?php echo display_image($record->banner_image); ?>" class="img-responsive" />

								</a>
							</div>
						<?php } else { ?>
							<div class="custom-dropzone" id="photo-wrapper">
								<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
									<span>Click to upload</span>
								</a>
							</div>
						<?php } ?>
						<div class="help-text">Accepts jpg, png with 2mb filesize.</div>
						<div id="error-banner_image"></div>
					</div>
					<div class="top-margin5">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							<i class="fa fa-times"></i> <?php echo lang('button_close')?>
						</button>
						<?php if ($action == 'add'): ?>
							<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
								<i class="fa fa-save"></i> <?php echo lang('button_add')?>
							</button>
						<?php elseif ($action == 'edit'): ?>
							<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
								<i class="fa fa-save"></i> <?php echo lang('button_update')?>
							</button>
						<?php else: ?>
							<script>$(".modal-body :input").attr("disabled", true);</script>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>