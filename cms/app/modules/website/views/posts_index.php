<span class="btn-actions">
	<?php if ($this->acl->restrict('website.posts.add', 'return')): ?>
		<a href="<?php echo site_url('website/posts/form/add')?>" class="btn btn-sm btn-success btn-add" id="btn_add"><span class="fa fa-plus"></span> <?php echo lang('button_add')?></a>
	<?php endif; ?>
</span>

<table class="table table-striped table-bordered table-hover dt-responsive" id="datatables">
	<thead>
		<tr>
			<th class="all"><?php echo lang('index_id')?></th>
			<th class="all"><?php echo lang('index_title'); ?></th>
			<th class="none"><?php echo lang('index_url'); ?></th>
			<th class="min-desktop"><?php echo lang('index_status'); ?></th>

			<th class="min-tablet"><?php echo lang('index_action')?></th>
		</tr>
	</thead>
</table>