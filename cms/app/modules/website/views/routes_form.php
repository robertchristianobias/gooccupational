<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">

		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

		<div class="form-group">
			<label class="col-sm-3 control-label" for="route_slug"><?php echo lang('route_slug')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'route_slug', 'name'=>'route_slug', 'value'=>set_value('route_slug', isset($record->route_slug) ? $record->route_slug : ''), 'class'=>'form-control'));?>
				<div id="error-route_slug"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="route_controller"><?php echo lang('route_controller')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'route_controller', 'name'=>'route_controller', 'value'=>set_value('route_controller', isset($record->route_controller) ? $record->route_controller : ''), 'class'=>'form-control'));?>
				<div id="error-route_controller"></div>
			</div>
		</div>



	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>