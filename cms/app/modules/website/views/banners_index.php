<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<?php if ($banner_groups): ?>
			<?php foreach ($banner_groups as $banner_group): ?>
				<li class="<?php echo ($banner_group->banner_group_id == $banner_group_id) ? 'active' : ''; ?>"><a href="<?php echo site_url('website/banners/' . $banner_group->banner_group_id); ?>"><?php echo $banner_group->banner_group_name; ?></a></li>
			<?php endforeach; ?>
		<?php endif; ?>

		<!-- <li class=""><a data-toggle="modal" data-target="#modal" href="<?php echo site_url('website/banner_groups/form/add'); ?>"><i class="fa fa-plus"></i> Add Banner Group</a></a></li> -->
	
	</ul>
	<div class="tab-content">
		<div class="tab-pane active">

			<div class="text-right">
				<a href="<?php echo site_url('website/banners/form/add/' . $banner_group_id); ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add Banner</a>
				<?php if ($banner_group_id != 1): ?>
					<a href="<?php echo site_url('website/banner_groups/form/edit/' . $banner_group_id); ?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit Group</a>
					<a href="<?php echo site_url('website/banner_groups/delete/' . $banner_group_id); ?>" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal"><i class="fa fa-trash"></i> Delete Group</a>
				<?php endif; ?>
			</div>

			<div class="hr dotted"></div>

			<?php if ($banners): ?>
				<ul id="sortable">
					<?php foreach ($banners as $banner): ?>
						<li class="ui-state-default col-xs-6 col-sm-3" data-id="<?php echo $banner->banner_id; ?>">
							<div class="thumbnail">
								<div class="pull-right btn-actions">
									<a data-toggle="modal" class="btn btn-xs btn-success" href="<?php echo site_url('website/banners/form/edit/' . $banner->banner_id); ?>"><div class="fa fa-pencil"></div></a>
									<a data-toggle="modal" data-target="#modal" class="btn btn-xs btn-danger" href="<?php echo site_url('website/banners/delete/' . $banner->banner_id); ?>"><div class="fa fa-trash"></div></a>
								</div>
								<img src="<?php echo site_url($banner->banner_image); ?>" />
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>

			
			<div class="clearfix"></div>
		</div>
	</div>


</div>

<h4 class="top-margin4">Embed Codes:</h4>
<p>PHP: <code>&lt;?php echo frontend_banners(<?php echo $banner_group_id; ?>); ?&gt;</code></p>
<p>Content: <code>##frontend_banners(<?php echo $banner_group_id; ?>)##</code></p>
