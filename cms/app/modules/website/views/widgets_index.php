<div class="row" id="widgets">
	<div class="col-sm-6">
		<div class="panel panel-primary">
			<div class="panel-heading">Available Widgets</div>
			<div class="panel-body">
				<ul class="list-group" id="available">
					<li class="list-group-item" data-type="Page" data-source="pages" data-widget-id="0">
						<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
						<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
						<span class="list-group-item-text">Page</span>
					</li>
					<li class="list-group-item" data-type="Posts" data-source="categories" data-widget-id="0">
						<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
						<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
						<span class="list-group-item-text">Posts</span>
					</li>
					<li class="list-group-item" data-type="Navigation" data-source="navigroups" data-widget-id="0">
						<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
						<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
						<span class="list-group-item-text">Navigation</span>
					</li>
					<li class="list-group-item" data-type="Banner" data-source="banner_groups" data-widget-id="0">
						<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
						<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
						<span class="list-group-item-text">Banner</span>
					</li>
					<li class="list-group-item" data-type="Partial" data-source="partials" data-widget-id="0">
						<span class="badge badge-danger btn-widget-delete hide"><i class="fa fa-times"></i></span>
						<span class="badge badge-warning btn-widget-edit hide"><i class="fa fa-pencil"></i></span>
						<span class="list-group-item-text">Partial</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<?php if ($sections): ?>
			<?php foreach ($sections as $code => $name): ?>
				<div class="panel panel-default">
					<div class="panel-heading"><?php echo $name; ?></div>
					<div class="panel-body">
						<ul class="list-group sortable" data-widget-section="<?php echo $code; ?>">
							<?php if ($widgets): ?>
								<?php foreach ($widgets as $widget): ?>
									<?php if ($widget->widget_section == $code): ?>
										<li class="list-group-item" data-type="<?php echo $widget->widget_type; ?>" data-source="<?php echo $widget->widget_source_table; ?>" data-widget-id="<?php echo $widget->widget_id; ?>" id="<?php echo $widget->widget_id; ?>">
											<span class="badge badge-danger btn-widget-delete"><i class="fa fa-times"></i></span>
											<span class="badge badge-warning btn-widget-edit"><i class="fa fa-pencil"></i></span>
											<span class="list-group-item-text"><?php echo $widget->widget_type; ?>: <?php echo ($widget->widget_name) ? $widget->widget_name : 'No Title'; ?></span>
										</li>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>