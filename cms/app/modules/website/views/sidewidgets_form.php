<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<?php echo form_open(current_url(), 'class="form-horizontal" id="sidewidgets-form"'); ?>

		<div class="form-group hide">
			<label class="col-sm-3 control-label" for="sidewidget_sidebar_id"><?php echo lang('sidewidget_sidebar_id')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'sidewidget_sidebar_id', 'name'=>'sidewidget_sidebar_id', 'value'=>set_value('sidewidget_sidebar_id', $sidewidget_sidebar_id), 'class'=>'form-control'));?>
				<div id="error-sidewidget_sidebar_id"></div>
			</div>
		</div>

		<div class="form-group hide">
			<label class="col-sm-3 control-label" for="sidewidget_type"><?php echo lang('sidewidget_type')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'sidewidget_type', 'name'=>'sidewidget_type', 'value'=>set_value('sidewidget_type', $sidewidget_type), 'class'=>'form-control'));?>
				<div id="error-sidewidget_type"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="sidewidget_name"><?php echo lang('sidewidget_name')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'sidewidget_name', 'name'=>'sidewidget_name', 'value'=>set_value('sidewidget_name', isset($record->sidewidget_name) ? $record->sidewidget_name : ''), 'class'=>'form-control'));?>
				<div id="error-sidewidget_name"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="sidewidget_source_id"><?php echo lang('sidewidget_source_id')?>:</label>
			<div class="col-sm-8">
				<?php echo form_dropdown('sidewidget_source_id', $sidewidget_sources, set_value('sidewidget_source_id', (isset($record->sidewidget_source_id)) ? $record->sidewidget_source_id : ''), 'id="sidewidget_source_id" class="form-control"'); ?>
				<div id="error-sidewidget_source_id"></div>
			</div>
		</div>

		<div class="form-group hide">
			<label class="col-sm-3 control-label" for="sidewidget_status"><?php echo lang('sidewidget_status')?>:</label>
			<div class="col-sm-8">
				<?php $options = create_dropdown('array', 'Active,Disabled'); ?>
				<?php echo form_dropdown('sidewidget_status', $options, set_value('sidewidget_status', (isset($record->sidewidget_status)) ? $record->sidewidget_status : ''), 'id="sidewidget_status" class="form-control"'); ?>
				<div id="error-sidewidget_status"></div>
			</div>
		</div>
		
	<?php echo form_close(); ?>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>