<?php echo form_open('', 'id="navigations-form"'); ?>
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<?php if ($navigroups): ?>
				<?php foreach ($navigroups as $navigroup): ?>
					<li class="<?php echo ($navigroup->navigroup_id == $navigroup_id) ? 'active' : ''; ?>"><a href="<?php echo site_url('website/navigations/' . $navigroup->navigroup_id); ?>"><?php echo $navigroup->navigroup_name; ?></a></li>
				<?php endforeach; ?>
			<?php endif; ?>
			<li class=""><a data-toggle="modal" data-target="#modal" href="<?php echo site_url('website/navigroups/form/add'); ?>"><i class="fa fa-plus"></i> Add Navigation Group</a></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active">
				<div class="row">
					<div class="col-sm-4">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											Categories
										</a>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<?php if ($categories): ?>
										<ul class="list-group">
											<?php foreach ($categories as $category): ?>					
												<li class="list-group-item">
													<a href="javascript:;" class="badge navadd" data-name="<?php echo $category->category_name; ?>" data-link="<?php echo $category->category_uri; ?>" data-res="categories" data-resid="<?php echo $category->category_id; ?>">Add <span class="fa fa-angle-double-right"></span></a>
													<?php echo $category->category_name; ?> - /<?php echo $category->category_uri; ?>
												</li>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
											Pages
										</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<?php if ($pages): ?>
										<ul class="list-group">
											<li class="list-group-item">
												<a href="javascript:;" class="badge navadd" data-name="Home" data-link="">Add <span class="fa fa-angle-double-right"></span></a>
												Home - /
											</li>
											<?php foreach ($pages as $page): ?>
												<?php if ($page->page_uri == 'home') continue; ?>		
												<li class="list-group-item">
													<a href="javascript:;" class="badge navadd" data-name="<?php echo $page->page_title; ?>" data-link="<?php echo $page->page_uri; ?>" data-res="pages" data-resid="<?php echo $page->page_id; ?>">Add <span class="fa fa-angle-double-right"></span></a>
													<?php echo $page->page_title; ?> - <?php echo '/' . $page->page_uri; ?>
												</li>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
											Custom Link
										</a>
									</h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body">
										<div class="form-horizontal">

											<div class="form-group">
												<label class="col-sm-3 control-label" for="custom_nav_name"><?php echo lang('custom_nav_name')?>:</label>
												<div class="col-sm-8">
													<?php echo form_input(array('id'=>'custom_nav_name', 'name'=>'custom_nav_name', 'value'=>set_value('custom_nav_name'), 'class'=>'form-control'));?>
													<div id="error-custom_nav_name"></div>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label" for="custom_nav_link"><?php echo lang('custom_nav_link')?>:</label>
												<div class="col-sm-8">
													<?php echo form_input(array('id'=>'custom_nav_link', 'name'=>'custom_nav_link', 'value'=>set_value('custom_nav_link'), 'class'=>'form-control'));?>
													<div id="error-custom_nav_link"></div>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label" for="custom_nav_target"><?php echo lang('custom_nav_target')?>:</label>
												<div class="col-sm-8">
													<?php $targets = array('_top' => 'Same Window', '_blank' => 'New Window', '_scroll' => 'Scroll'); ?>
													<?php echo form_dropdown('custom_nav_target', $targets, set_value('custom_nav_target'), 'id="custom_nav_target" class="form-control"'); ?>
													<div id="error-custom_nav_target"></div>
												</div>
											</div>

											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-8">
													<a href="javascript:;" class="btn btn-default btn-custom-link">Add Custom Link <span class="fa fa-angle-double-right"></span></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="col-sm-4">
						<div class="dd">
							<?php echo ($navigations) ? $navigations : '<ol class="dd-list outer"></ol>'; ?>
						</div>
						<div class="top-margin4">
							<button href="javascript:;" class="btn btn-success btn-save" data-id="<?php echo $navigroup_id; ?>" data-loading-text="<?php echo lang('processing')?>"><i class="fa fa-save"></i> Save Changes</button>
							<?php if ($navigroup_id != 1): ?> 
								<a href="<?php echo site_url('website/navigroups/form/edit/' . $navigroup_id); ?>" class="btn btn-warning" data-toggle="modal" data-target="#modal" title="Edit Group"><i class="fa fa-pencil"></i> Edit Group</a>
								<a href="<?php echo site_url('website/navigroups/delete/' . $navigroup_id); ?>" class="btn btn-danger" data-toggle="modal" data-target="#modal" title="Delete Group"><i class="fa fa-trash"></i></a>
							<?php endif; ?>
						</div>
					</div>

					<div class="col-sm-4">
						<h4>Instruction:</h4>
						<ol id="instruction">
							<li>Add the navigation items from Categories, Pages or Custom Link</li>
							<li>Drag and drop the navigation items</li>
							<li><strong>Save changes</strong></li>
						</ol>

						<h4 class="top-margin4">Embed Codes:</h4>
						<p>PHP (navbar): <br /><code>&lt;?php echo frontend_navigation(<?php echo $navigroup_id; ?>); ?&gt;</code></p>
						<p>PHP (widget): <br /><code>&lt;?php echo frontend_navigation_widget(<?php echo $navigroup_id; ?>); ?&gt;</code></p>
						<p>Content (navbar): <br /><code>##frontend_navigation(<?php echo $navigroup_id; ?>)##</code></p>
						<p>Content (widget): <br /><code>##frontend_navigation_widget(<?php echo $navigroup_id; ?>)##</code></p>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>