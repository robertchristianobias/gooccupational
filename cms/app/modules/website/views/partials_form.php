<?php echo form_open(current_url(), 'id="partials-form"'); ?>
	<div class="nav-tabs-custom bottom-margin">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_page" data-toggle="tab"><span class="fa fa-file-text"></span> Content</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_page">
				<div class="row">
					<div class="col-sm-9">
						<div class="form-group">
							<label class="control-label" for="partial_content"><?php echo lang('partial_content'); ?>:</label>
							<?php echo form_textarea(array('id'=>'partial_content', 'name'=>'partial_content', 'rows'=>'15', 'value'=>set_value('partial_content', isset($record->partial_content) ? $record->partial_content : '', FALSE), 'class'=>'ckeditor form-control meta-description')); ?>
							<div id="error-partial_content"></div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label class="control-label" for="partial_title"><?php echo lang('partial_title'); ?>:</label>	
							<?php echo form_input(array('id'=>'partial_title', 'name'=>'partial_title', 'value'=>set_value('partial_title', isset($record->partial_title) ? $record->partial_title : ''), 'class'=>'form-control meta-title'));?>
							<div id="error-partial_title"></div>
						</div>
						<!-- <div class="form-group">
							<label class="control-label" for="partial_status"><?php echo lang('partial_status'); ?>:</label>
							<div class="radio top-margin">
								<label class="radio-inline">
									<input class="partial_status" name="partial_status" type="radio" value="Posted" <?php echo set_radio('partial_status', 'Posted', (isset($record->partial_status) && $record->partial_status == 'Posted') ? TRUE : FALSE); ?> /> Posted
								</label>
								<label class="radio-inline">
									<input class="partial_status" name="partial_status" type="radio" value="Draft" <?php echo set_radio('partial_status', 'Draft', ($action == 'add' OR isset($record->partial_status) && $record->partial_status == 'Draft') ? TRUE : FALSE); ?> /> Draft
								</label>
							</div>
							<div id="error-partial_status"></div>
						</div> -->
						<div class="form-group">
							<input type="hidden" name="partial_photo" id="images_photo" value="<?php echo isset($record->partial_photo) ? $record->partial_photo : ''; ?>" />
							<label class="control-label bottom-margin" for="partial_photo"><?php echo lang('partial_photo')?>:</label>
							<?php if( ! empty($record->partial_photo)) { ?>
								<div class="custom-dropzone" id="photo-wrapper">
									<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
										<img src="<?php echo display_image($record->partial_photo); ?>" class="img-responsive" />

									</a>
								</div>
							<?php } else { ?>
								<div class="custom-dropzone" id="photo-wrapper">
									<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
										<span>Click to upload</span>
									</a>
								</div>
							<?php } ?>
							<div class="help-text">Accepts jpg, png with 2mb filesize.</div>
							<div id="error-partial_photo"></div>
						</div>
						<div class="top-margin5">
							<?php if ($action == 'add'): ?>
								<button class="btn btn-success btn-sm" id="submit" data-status="Add" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-plus"></i> <?php echo lang('button_add')?>
								</button>
							<?php elseif ($action == 'edit'): ?>
								<button class="btn btn-success btn-sm submit" data-status="Update" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-save"></i> <?php echo lang('button_update')?>
								</button>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="tab_seo">
				
			</div>
		</div>
	</div>
<?php echo form_close(); ?>
<script>
	var post_url = '<?php echo current_url(); ?>';
</script>