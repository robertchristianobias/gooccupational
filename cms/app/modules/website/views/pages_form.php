
<?php echo form_open(current_url(), 'id="pages-form"'); ?>
	<div class="nav-tabs-custom bottom-margin">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_page" data-toggle="tab"><span class="fa fa-file-text"></span> Content</a></li>
			<li class="pull-right">
				
			</li>
			<?php if (isset($record->page_id) AND isset($record->page_metatag_id)): ?>
				<li><a href="<?php echo site_url('metatags/form/website/pages/' . $record->page_id); ?>" data-toggle="modal" data-target="#modal" class="btn btn-info"><span class="fa fa-cog"></span> Meta Tags</a></li>
			<?php endif; ?>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_page">
				<div class="row">

					<input type="hidden" name="page_photo" id="images_photo" value="<?php echo isset($record->page_photo) ? $record->page_photo : ''; ?>" />

					<div class="col-sm-9">

						<div class="form-group">
							<label class="control-label" for="page_title"><?php echo lang('page_title'); ?>:</label>	
							<?php echo form_input(array('id'=>'page_title', 'name'=>'page_title', 'value'=>set_value('page_title', isset($record->page_title) ? $record->page_title : '', FALSE), 'class'=>'form-control meta-title'));?>
							<div id="error-page_title"></div>
						</div>
		
						<div class="form-group">
							<?php
								$page_uri = '';
								if(isset($record->page_uri))
								{
									$segment = explode('/', $record->page_uri);

									if(count($segment) > 1)
									{
										$page_uri = $segment['0'] . '/';
									}
								}
							?>
							<label class="control-label" for="page_slug"><?php echo lang('page_slug'); ?>:</label>
							<div class="input-group">
								<span class="input-group-addon"><?php echo config_item('website_url') . $page_uri; ?></span>
								<?php echo form_input(array('id'=>'page_slug', 'name'=>'page_slug', 'value'=>set_value('page_slug', isset($record->page_slug) ? $record->page_slug : ''), 'class'=>'form-control'));?>
							</div>
							<div id="error-page_slug"></div>
						</div>

						<div class="form-group">
							<label class="control-label" for="page_content"><?php echo lang('page_content'); ?>:</label>
							<?php echo form_textarea(array('id'=>'page_content', 'name'=>'page_content', 'rows'=>'15', 'value'=>set_value('page_content', isset($record->page_content) ? $record->page_content : '', FALSE), 'class'=>'form-control meta-description ckeditor')); ?>
							<div id="error-page_content"></div>
						</div>

					</div>

					<div class="col-sm-3">
					

						<div class="form-group">
							<label class="control-label" for="page_parent_id"><?php echo lang('page_parent_id')?>:</label>
							<?php echo form_dropdown('page_parent_id', $pages, set_value('page_parent_id', (isset($record->page_parent_id)) ? $record->page_parent_id : ''), 'id="page_parent_id" class="form-control"'); ?>
							<div id="error-page_parent_id"></div>
						</div>


						<div class="top-margin5">
							<?php if ($action == 'add'): ?>
								<button class="btn btn-success btn-sm submit" data-status="Published" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-plus"></i> <?php echo lang('button_publish')?>
								</button>
							<?php elseif ($action == 'edit'): ?>
								<button class="btn btn-success btn-sm submit" data-status="Published" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-save"></i> <?php echo lang('button_publish')?>
								</button>
							<?php endif; ?>
							<button class="btn btn-primary btn-sm submit" data-status="Draft" type="submit" data-loading-text="<?php echo lang('processing')?>">
								<?php echo lang('button_draft')?>
							</button>
						</div>

					</div>

				</div>
			</div>
			<div class="tab-pane" id="tab_seo">
				
			</div>
		</div>
	</div>
<?php echo form_close(); ?>