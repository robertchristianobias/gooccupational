<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Website Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Website extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
		$this->load->model('settings/configs_model');
		$this->load->language('website');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
		
	}

	// --------------------------------------------------------------------

	/**
	 * settings
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function settings()
	{
		$this->acl->restrict('website.website.settings');

		// page title
		$data['page_heading'] = lang('settings_heading');
		$data['page_subhead'] = lang('settings_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('website/settings'));
		$this->breadcrumbs->push(lang('settings_heading'), site_url('website/settings'));

		if ($this->input->post())
		{

			if ($page_id = $this->_save_settings())
			{
				// $this->session->set_flashdata('flash_message',  lang('settings_success'));
				echo json_encode(array('success' => true, 'message' => lang('settings_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'website_name'		=> form_error('website_name'),
					'website_email'		=> form_error('website_email'),
					'website_url'		=> form_error('website_url'),
					'website_theme'		=> form_error('website_theme'),
					'company_map_embed_code' => form_error('company_map_embed_code'),
					'company_phone_no' => form_error('company_phone_no'),
					'company_address'  => form_error('company_address')
				);
				echo json_encode($response);
				exit;
			}
		}

		$website_configs     	= array('website_name', 'website_email', 'website_url', 'website_theme');
		$cms_configs			= array('cms_url');
		$company_configs   		= array('company_map_embed_code', 'company_phone_no', 'company_address');
		$recaptcha_configs 		= array('recaptcha_site_key', 'recaptcha_secret_key');
		$app_version_configs 	= array('ios_version', 'android_version', 'merchant_app_android_version');

		// get the configs
		$data['website_configs'] 	 = $this->configs_model->get_configs($website_configs, 	   'website');
		$data['cms_configs'] 	 	 = $this->configs_model->get_configs($cms_configs, 	   	   'cms');
		$data['company_configs']     = $this->configs_model->get_configs($company_configs,     'company');
		$data['recaptcha_configs']   = $this->configs_model->get_configs($recaptcha_configs,   'recaptcha');
		$data['app_version_configs'] = $this->configs_model->get_configs($app_version_configs, 'app_version');

		
		$this->template->add_js(module_js('website', 'website_settings'));
		$this->template->write_view('content', 'website_settings', $data);
		$this->template->render();
	}


	// --------------------------------------------------------------------

	/**
	 * _save_settings
	 *
	 * @access	private
	 * @param 	array $this->input->post()
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _save_settings()
	{
		// validate inputs
		$this->form_validation->set_rules('website_name', lang('website_name'), 'required');
		$this->form_validation->set_rules('website_email', lang('website_email'), 'required');
		$this->form_validation->set_rules('website_url', lang('website_url'), 'required');
		$this->form_validation->set_rules('website_theme', lang('website_theme'), 'required');
		$this->form_validation->set_rules('company_map_embed_code', lang('company_map_embed_code'), 'required');
		$this->form_validation->set_rules('company_phone_no', lang('company_phone_no'), 'required');
		$this->form_validation->set_rules('company_address', lang('company_address'), 'required');
		$this->form_validation->set_rules('recaptcha_secret_key', lang('recaptcha_secret_key'), 'required');
		$this->form_validation->set_rules('recaptcha_site_key', lang('recaptcha_site_key'), 'required');
		$this->form_validation->set_rules('android_version', lang('android_version'), 'required');
		$this->form_validation->set_rules('ios_version', lang('ios_version'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		foreach ($this->input->post() as $key => $value)
		{
			if ($key == 'submit') break;

			$this->configs_model->update_where('config_name', $key, array('config_value' => $value));
		}

		$this->cache->delete('app_configs');
		$this->cache->delete('config_settings_files');
		$this->cache->delete('config_settings_company');
		$this->cache->delete('config_settings_recaptcha');
		$this->cache->delete('config_settings_app_version');
		$this->cache->delete('app_version_ios');
		$this->cache->delete('app_version_android');

		return TRUE;
	}
}

/* End of file Website.php */
/* Location: ./application/modules/website/controllers/Website.php */