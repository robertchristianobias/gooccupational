<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Widgets Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Widgets extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
		$this->load->config('config');
		$this->load->model('widgets_model');
		$this->load->language('widgets');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
		$this->acl->restrict('website.widgets.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('website/widgets'));
		$this->breadcrumbs->push(lang('index_heading'), site_url('website/widgets'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());

		// get the theme sections
		$data['sections'] = config_item('theme_widget_sections');
		
		// get the active widgets
		$data['widgets'] = $this->widgets_model
			->where('widget_deleted', 0)
			->order_by('widget_order', 'asc')
			->find_all();

		// add plugins
		$this->template->add_css('components/jquery-ui/jquery-ui.theme.min.css');
		// $this->template->add_js('components/jquery-ui/jquery-ui.min.js');
		
		// render the page
		// $this->template->add_css(module_css('website', 'widgets_index'), 'embed');
		
		$this->template->add_js(module_js('website', 'widgets_index'));
		$this->template->write_view('content', 'widgets_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('website.widgets.list');

		echo $this->widgets_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function form($action = 'add', $type, $section, $source, $id = FALSE)
	{
		$this->acl->restrict('website.widgets.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading') . ' (' . $type . ')';
		$data['action'] = $action;
		$data['section'] = $section;
		$data['source'] = $source;

		if ($this->input->post())
		{
			if ($widget_id = $this->_save())
			{
				echo json_encode(array('success' => true, 'widget_id' => $widget_id, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'widget_name'			=> form_error('widget_name'),
					'widget_source_id'		=> form_error('widget_source_id'),
					'widget_status'			=> form_error('widget_status'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->widgets_model->find($id);

		$widget_sources = array();
		switch ($source)
		{
			case 'pages':
				$this->load->model('pages_model');
				$widget_sources = $this->pages_model
					->where('page_deleted', 0)
					->where('page_status', 'Posted')
					->order_by('page_title')
					->format_dropdown('page_id', 'page_title');
				break;

			case 'categories':
				$this->load->model('categories_model');
				$categories = $this->categories_model->get_category_checkboxes();
		
				$cats = array();
				if ($categories)
				{
					foreach ($categories as $category)
					{
						$cats[$category->category_id] = repeater(' - ', $category->category_indent/15) . $category->category_name; 
					}
				}
				$widget_sources = $cats;
				break;

			case 'navigroups':
				$this->load->model('navigroups_model');
				$widget_sources = $this->navigroups_model
					->where('navigroup_deleted', 0)
					->order_by('navigroup_name')
					->format_dropdown('navigroup_id', 'navigroup_name');
				break;

			case 'banner_groups':
				$this->load->model('banner_groups_model');
				$widget_sources = $this->banner_groups_model
					->where('banner_group_deleted', 0)
					->order_by('banner_group_name')
					->format_dropdown('banner_group_id', 'banner_group_name');
				break;

			case 'partials':
				$this->load->model('partials_model');
				$widget_sources = $this->partials_model
					->where('partial_deleted', 0)
					->where('partial_status', 'Posted')
					->order_by('partial_title')
					->format_dropdown('partial_id', 'partial_title');
				break;

		}
		$data['widget_sources'] = $widget_sources;

		$data['widget_type'] = isset($data['record']->widget_type) ? $data['record']->widget_type : $type;

		// render the page
		$this->template->set_template('modal');
		// $this->template->add_css(module_css('website', 'widgets_form'), 'embed');
		
		$this->template->add_js(module_js('website', 'widgets_form'));
		$this->template->write_view('content', 'widgets_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('website.widgets.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		// $data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->widgets_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * reorder
	 *
	 * @access	public
	 * @param	array $this->input->post('widget_ids')
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function reorder()
	{
		$this->acl->restrict('website.widgets.edit', 'modal');

		$widget_ids = $this->input->post('widget_ids');
		$widget_section = $this->input->post('widget_section');

		// get the widgets
		$widgets = $this->widgets_model
			->where_in('widget_id', $widget_ids)
			->find_all();

		if ($widgets && $widget_ids)
		{
			foreach ($widgets as $widget)
			{
				// update the banner
				$data = array('widget_order' => array_search($widget->widget_id, $widget_ids));
				if ($widget_section) $data['widget_section'] = $widget_section;
				$this->widgets_model->update($widget->widget_id, $data);
			}

			echo json_encode(array('success' => true, 'message' => lang('reorder_success'))); exit;
		}

		echo json_encode(array('success' => false)); exit;
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _save()
	{
		// validate inputs
		$this->form_validation->set_rules('widget_name', lang('widget_name'), 'max_length[100]');
		$this->form_validation->set_rules('widget_source_id', lang('widget_source_id'), 'required');
		$this->form_validation->set_rules('widget_status', lang('widget_status'), 'required');
		

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$action = $this->uri->segment(4);
		$type = $this->uri->segment(5);
		$section = $this->uri->segment(6);
		$source = $this->uri->segment(7);
		$id = $this->uri->segment(8);

		switch ($source)
		{
			case 'pages': $function = 'frontend_page_widget'; break;
			case 'categories': $function = 'frontend_posts_widget'; break;
			case 'navigroups': $function = 'frontend_navigation_widget'; break;
			case 'banner_groups': $function = 'frontend_banner_widget'; break;
			case 'partials': $function = 'frontend_partial_widget'; break;
			default: $function = ''; break;

			// add the sources (table name) and function names here for additional widgets
		}

		$data = array(
			'widget_type'			=> $type,
			'widget_name'			=> $this->input->post('widget_name'),
			'widget_section'		=> $section,
			'widget_function'		=> $function . '(' . $this->input->post('widget_source_id') . ')',
			'widget_source_table'	=> $source,
			'widget_source_id'		=> $this->input->post('widget_source_id'),
			'widget_status'			=> $this->input->post('widget_status'),	
		);
		
		if ($action == 'add')
		{
			$data['widget_order'] = 99; // put at the last
			$insert_id = $this->widgets_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$this->widgets_model->update($id, $data);
			$return = $id;
		}

		return $return;

	}
}

/* End of file Widgets.php */
/* Location: ./application/modules/website/controllers/Widgets.php */