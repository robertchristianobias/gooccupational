<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sidebars Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Sidebars extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
		$this->load->model('sidebars_model');
		$this->load->language('sidebars');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index($id = FALSE)
	{
		$this->acl->restrict('website.sidebars.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('sidebars'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());

		// echo $id; exit;
		if (!$id)
		{
			redirect('website/sidebars/1');
		}

		// active navigation group
		$data['sidebar_id'] = $id;

		// get sidebar widgets
		$data["sidebars"] = $this->sidebars_model
			->where('sidebar_deleted', 0)
			->where('sidebar_deleted', 0)
			->find_all();

		// load sidebar widgets model
		$this->load->model('sidewidgets_model');
		// get sidebar widgets
		$data["sidewidgets"] = $this->sidewidgets_model
			->where('sidewidget_sidebar_id', $id)
			->where('sidewidget_deleted', 0)
			->order_by('sidewidget_order', 'ASC')
			->find_all();

		// add plugins
		$this->template->add_css('components/jquery-ui/jquery-ui.theme.min.css');
		
		// render the page
		// $this->template->add_css(module_css('website', 'sidebars_index'), 'embed');

		$this->template->add_js(module_js('website', 'sidebars_index'));
		$this->template->write_view('content', 'sidebars_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * 
	 */
	function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('website.sidebars.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;

		if ($this->input->post())
		{
			if ($id = $this->_save($action, $id))
			{
				echo json_encode(array('success' => true, 'id' => $id, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'sidebar_name'		=> form_error('sidebar_name'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->sidebars_model->find($id);

		// render the page
		$this->template->set_template('modal');
		// $this->template->add_css(module_css('website', 'sidebars_form'), 'embed');
		
		$this->template->add_js(module_js('website', 'sidebars_form'));
		$this->template->write_view('content', 'sidebars_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('website.navigroups.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['redirect_url'] = site_url('website/sidebars');

		if ($this->input->post())
		{
			$this->sidebars_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 *  (modify)
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('sidebar_name', lang('sidebar_name'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'sidebar_name'		=> $this->input->post('sidebar_name'),
		);
		

		if ($action == 'add')
		{
			$insert_id = $this->sidebars_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->sidebars_model->update($id, $data) ? $id : FALSE; // adds return variable $id 
		}

		return $return;

	}
}

/* End of file Navigroups.php */
/* Location: ./application/modules/website/controllers/Navigroups.php */