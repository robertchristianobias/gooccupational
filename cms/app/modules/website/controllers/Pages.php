<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Class
 *
 * @package		RainCode
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Pages extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
		
		// check for dependencies
		if (! $this->db->table_exists('images'))
		{
			$this->session->set_flashdata('flash_error', 'Pages module requires the Files module');
			redirect($this->session->userdata('redirect'), 'refresh');
		}

		$this->load->config('config');
		$this->load->model('pages_model');
		$this->load->model('routes_model');
		$this->load->model('sidebars_model');
		$this->load->model('banner_groups_model');

		$this->load->language('pages');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
		$this->acl->restrict('website.pages.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('website/pages'));
		$this->breadcrumbs->push(lang('index_heading'), site_url('website/pages'));

		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// datatables
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');
		
		// render the page
		// $this->template->add_css(module_css('website', 'pages_index'), 'embed');
		
		$this->template->add_js(module_js('website', 'pages_index'), 'embed');

		$this->template->write_view('content', 'pages_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('website.pages.list');

		echo $this->pages_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('website.pages.' . $action);
		
		// page title
		$data['page_heading']   = lang($action . '_heading');
		$data['page_subhead']   = lang($action . '_subhead');
		$data['action'] 		= $action;
		$data['theme_template'] = config_item('theme_template');
		$data['banners']		= $this->banner_groups_model->get_banner_groups_dropdown('banner_group_id', 'banner_group_name', TRUE);
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('website/pages'));
		$this->breadcrumbs->push(lang('index_heading'), site_url('website/pages'));
		$this->breadcrumbs->push(lang($action . '_heading'), site_url('website/pages/' . $action));

		if ($this->input->post())
		{
			if ($page_id = $this->_save($action, $id))
			{
				$this->session->set_flashdata('flash_message',  lang($action . '_success'));
				echo json_encode(array('success' => true, 'action' => $action, 'id' => $page_id, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'page_parent_id'	=> form_error('page_parent_id'),
					'page_title'		=> form_error('page_title'),
					'page_slug'			=> form_error('page_slug'),
					'page_content'		=> form_error('page_content'),
				);
				echo json_encode($response);
				exit;
			}
		}

		// get the pages
		$data['pages'] = $this->pages_model
			->where('page_deleted', 0)
			->order_by('page_title', 'asc')
			->format_dropdown('page_id', 'page_title', TRUE);

		// get the sidebars
		$data['sidebar'] = $this->sidebars_model
			->where('sidebar_deleted', 0)
			->order_by('sidebar_name', 'asc')
			->format_dropdown('sidebar_id', 'sidebar_name', TRUE);

		if ($action != 'add') $data['record'] = $this->pages_model->find($id);

		// render the page
		$this->template->add_js('components/dropzone/dist/dropzone.js');
		$this->template->add_css('components/dropzone/dist/dropzone.css');

		$this->template->add_js('components/select2/dist/js/select2.min.js');
		$this->template->add_css('components/select2/dist/css/select2.min.css');
		
		$this->template->add_js('components/ckeditor/ckeditor.js');

		if ($action == 'view')
		{
			$this->template->add_js('$(".tab-content :input").attr("disabled", true);', 'embed');
		}

		// $this->template->add_css(module_css('website', 'pages_form'), 'embed');

		$this->template->add_js(module_js('website', 'pages_form'), 'embed');
		$this->template->write_view('content', 'pages_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('website.pages.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->pages_model->delete($id);

			// update the frontend routes
			// $this->_update_routes();

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 * 		 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('page_title', lang('page_title'), 'required');
		$this->form_validation->set_rules('page_slug', lang('page_slug'), 'required');
		$this->form_validation->set_rules('page_content', lang('page_content'), 'required');

		$this->form_validation->set_message('required', 'This field is required.');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		// get the page path
		$path = $this->pages_model->get_page_path($this->input->post('page_parent_id'));
		$template = $this->input->post('page_template');

		$slug = url_title($this->input->post('page_slug'), '-', TRUE);
		if ($path)
		{
			$uri = $path . '/' . $slug;
		}
		else
		{	
			$uri = $slug;
		}

		$data = array(
			'page_title'		=> $this->input->post('page_title'),
			'page_slug'			=> $slug,
			'page_uri'			=> $uri,
			'page_content'		=> $this->input->post('page_content'),
			'page_status'		=> 'Posted',
		);
		
		// $data = $this->security->xss_clean($data);

		if ($action == 'add')
		{
			$insert_id = $this->pages_model->insert($data);
			$return    = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$this->pages_model->update($id, $data);

			// update the navigation uri
			$this->load->model('navigations_model');
			$this->navigations_model->update_where(array(
				'navigation_source' => 'pages',
				'navigation_source_id' => $id
			), NULL, array('navigation_link' => $uri));

			// delete the cache
			$this->output->delete_cache('/' . $uri);
			$this->cache->delete('page_' . $uri);
			$this->cache->delete('page_' . $slug);

			$return = $id;
		}

		// update the frontend routes
		// $this->_update_routes();

		return $return;
	}

	// --------------------------------------------------------------------

	/**
	 * _update_routes
	 *
	 * @access	private
	 * @param	none
	 * @return 	boolean
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _update_routes()
	{
		$pages = $this->pages_model
			->where('page_deleted', 0)
			->where('page_status', 'Posted')
			->find_all();

		$data = array();
		if ($pages)
		{
			$data[] = '<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');';
			foreach ($pages as $page)
			{
				$data[] = '$route[\'' . $page->page_uri . '\'] = \'frontend/page/view/' . $page->page_uri . '\';';
			}

			$output = implode("\n", $data);

            write_file(APPPATH . 'cache/routes.php', $output);
		}

		return TRUE;
	}
}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */