<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sidewidgets Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Sidewidgets extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
		$this->load->model('sidewidgets_model');
		$this->load->language('sidewidgets');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * 
	 */
	public function index($id = FALSE)
	{
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * 
	 */
	function form($action = 'add', $type, $section, $source, $id = FALSE)
	{
		$this->acl->restrict('website.sidebars.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading') . ' (' . $type . ')';
		$data['action'] = $action;
		$data['section'] = $section;
		$data['source'] = $source;

		if ($this->input->post())
		{

			if ($widget_id = $this->_save())
			{
				echo json_encode(array('success' => true, 'widget_id' => $widget_id, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'sidewidget_name'			=> form_error('sidewidget_name'),
					'sidewidget_source_id'		=> form_error('sidewidget_source_id'),
					'sidewidget_status'			=> form_error('sidewidget_status'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->sidewidgets_model->find($id);

		$widget_sources = array();
		switch ($source)
		{
			case 'pages':
				$this->load->model('pages_model');
				$widget_sources = $this->pages_model
					->where('page_deleted', 0)
					->where('page_status', 'Posted')
					->order_by('page_title')
					->format_dropdown('page_id', 'page_title');
				break;

			case 'categories':
				$this->load->model('categories_model');
				$categories = $this->categories_model->get_category_checkboxes();
		
				$cats = array();
				if ($categories)
				{
					foreach ($categories as $category)
					{
						$cats[$category->category_id] = repeater(' - ', $category->category_indent/15) . $category->category_name; 
					}
				}
				$widget_sources = $cats;
				break;

			case 'navigroups':
				$this->load->model('navigroups_model');
				$widget_sources = $this->navigroups_model
					->where('navigroup_deleted', 0)
					->order_by('navigroup_name')
					->format_dropdown('navigroup_id', 'navigroup_name');
				break;

			case 'banner_groups':
				$this->load->model('banner_groups_model');
				$widget_sources = $this->banner_groups_model
					->where('banner_group_deleted', 0)
					->order_by('banner_group_name')
					->format_dropdown('banner_group_id', 'banner_group_name');
				break;

			case 'partials':
				$this->load->model('partials_model');
				$widget_sources = $this->partials_model
					->where('partial_deleted', 0)
					->where('partial_status', 'Posted')
					->order_by('partial_title')
					->format_dropdown('partial_id', 'partial_title');
				break;

		}
		$data['sidewidget_sources'] = $widget_sources;

		$data['sidewidget_type'] = isset($data['record']->sidewidget_type) ? $data['record']->sidewidget_type : $type;

		// render the page
		$this->template->set_template('modal');
		$this->template->add_css(module_css('website', 'sidewidgets_form'));
		$this->template->add_js(module_js('website', 'sidewidgets_form'));
		$this->template->write_view('content', 'sidewidgets_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * 
	 */
	function delete($id)
	{
		$this->acl->restrict('website.navigroups.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		// $data['datatables_id'] = '#datatables';
		$data['redirect_url'] = site_url('website/sidebars');

		if ($this->input->post())
		{
			$this->sidewidgets_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * reorder
	 *
	 * @access	public
	 * @param	array $this->input->post('widget_ids')
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function reorder()
	{
		$this->acl->restrict('website.widgets.edit', 'modal');

		$sidewidget_ids 		= $this->input->post('sidewidget_ids');
		$sidewidget_sidebar_id 	= $this->input->post('sidewidget_sidebar_id');

		// get the widgets
		$sidewidgets = $this->sidewidgets_model
			->where_in('sidewidget_id', $sidewidget_ids)
			->find_all();

		if ($sidewidgets && $sidewidget_ids)
		{
			foreach ($sidewidgets as $sidewidget)
			{
				// update the banner
				$data = array('sidewidget_order' => array_search($sidewidget->sidewidget_id, $sidewidget_ids));
				$this->sidewidgets_model->update($sidewidget->sidewidget_id, $data);
			}

			echo json_encode(array('success' => true, 'message' => lang('reorder_success'))); exit;
		}

		echo json_encode(array('success' => false)); exit;
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * 
	 *  (modify)
	 */
	private function _save($action = 'add', $id = 0)
	{

		// validate inputs
		$this->form_validation->set_rules('sidewidget_name', lang('sidewidget_name'), 'max_length[100]');
		$this->form_validation->set_rules('sidewidget_source_id', lang('sidewidget_source_id'), 'required');
		$this->form_validation->set_rules('sidewidget_status', lang('sidewidget_status'), 'required');
		

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$action 	= $this->uri->segment(4);
		$type 		= $this->uri->segment(5);
		$sidebar_id	= $this->uri->segment(6);
		$source 	= $this->uri->segment(7);
		$id 		= $this->uri->segment(8);

		switch ($source)
		{
			case 'pages': $function = 'frontend_page_widget'; break;
			case 'categories': $function = 'frontend_posts_widget'; break;
			case 'navigroups': $function = 'frontend_navigation_widget'; break;
			case 'banner_groups': $function = 'frontend_banner_widget'; break;
			case 'partials': $function = 'frontend_partial_widget'; break;
			default: $function = ''; break;

			// add the sources (table name) and function names here for additional widgets
		}

		$data = array(
			'sidewidget_sidebar_id'		=> $sidebar_id,
			'sidewidget_type'			=> $type,
			'sidewidget_name'			=> $this->input->post('sidewidget_name'),
			'sidewidget_function'		=> $function . '(' . $this->input->post('sidewidget_source_id') . ')',
			'sidewidget_source_table'	=> $source,
			'sidewidget_source_id'		=> $this->input->post('sidewidget_source_id'),
			'sidewidget_status'			=> $this->input->post('sidewidget_status'),			
		);
		
		if ($action == 'add')
		{
			$data['sidewidget_order'] = 99; // put at the last
			$insert_id = $this->sidewidgets_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$this->sidewidgets_model->update($id, $data);
			$return = $id;
		}

		return $return;


	}
}

/* End of file Navigroups.php */
/* Location: ./application/modules/website/controllers/Navigroups.php */