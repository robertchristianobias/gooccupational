<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Banners Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Banners extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
		$this->load->model('banner_groups_model');
		$this->load->model('banners_model');
		$this->load->language('banners');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index($id = FALSE)
	{
		$this->acl->restrict('website.banners.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('website/banners'));
		$this->breadcrumbs->push(lang('index_heading'), site_url('website/banners'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());

		if (!$id)
		{
			redirect('website/banners/1');
		}
		

		// active banner group
		$data['banner_group_id'] = $id;

		// get the banner groups
		$data['banner_groups'] = $this->banner_groups_model
			->where('banner_group_deleted', 0)
			->find_all();

		// get the banners
		$data['banners'] = $this->banners_model
			->where('banner_deleted', 0)
			->where('banner_banner_group_id', $id)
			->order_by('banner_order', 'asc')
			->order_by('banner_id', 'desc')
			->find_all();


		// add plugins
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');

		// render the page
		// $this->template->add_css(module_css('website', 'banners_index'), 'embed');
		
		$this->template->add_js(module_js('website', 'banners_index'), 'embed');

		$this->template->write_view('content', 'banners_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------
	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('website.banners.list');

		echo $this->banners_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('website.banners.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				$this->session->set_flashdata('flash_message', lang($action . '_success'));
				echo json_encode(array('success' => true, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'banner_title' 			=> form_error('banner_title'),
					'banner_image'			=> form_error('banner_image'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->banners_model->find($id);

		// render the page
		// $this->template->set_template('modal');
		
		$this->template->add_js('components/dropzone/dist/dropzone.js');
		$this->template->add_css('components/dropzone/dist/dropzone.css');

		// $this->template->add_css(module_css('website', 'banners_form'), 'embed');

		$this->template->add_js(module_js('website', 'banners_form'), 'embed');
		$this->template->write_view('content', 'banners_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('website.banners.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		// $data['redirect_url'] = site_url('website/banners/' );

		if ($this->input->post())
		{
			$this->banners_model->delete($id);

			$this->session->set_flashdata('flash_message', lang('delete_success'));
			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * reorder
	 *
	 * @access	public
	 * @param	array $this->input->post('banner_ids')
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function reorder()
	{
		$this->acl->restrict('website.banners.edit', 'modal');

		$banner_ids = $this->input->post('banner_ids');

		// get the banners
		$banners = $this->banners_model
			->where_in('banner_id', $banner_ids)
			->find_all();

		if ($banners)
		{
			foreach ($banners as $banner)
			{
				// update the banner
				$this->banners_model->update($banner->banner_id, array(
					'banner_order' => array_search($banner->banner_id, $banner_ids)
				));
			}
		}

		echo json_encode(array('success' => true, 'message' => lang('reorder_success'))); exit;
	}



	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('banner_title', lang('banner_title'), 'required');
		$this->form_validation->set_rules('banner_image', lang('banner_image'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'banner_title' 			=> $this->input->post('banner_title'),
			'banner_thumb'			=> '',
			'banner_image'			=> $this->input->post('banner_image'),
			'banner_caption'		=> $this->input->post('banner_caption'),
			'banner_button'			=> '',
			'banner_link'			=> '',
			'banner_target'			=> '',
			'banner_status'			=> 'Active',
		);
		
		if ($action == 'add')
		{
			$data['banner_banner_group_id'] = $id;
			$data['banner_order'] = $id;
			$insert_id = $this->banners_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->banners_model->update($id, $data);
		}

		return $return;

	}
}

/* End of file Banners.php */
/* Location: ./application/modules/website/controllers/Banners.php */