<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Posts Class
 *
 * @package		RainCode
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Posts extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
		
		// check for dependencies
		if (! $this->db->table_exists('images'))
		{
			$this->session->set_flashdata('flash_error', 'Posts module requires the Files module');
			redirect($this->session->userdata('redirect'), 'refresh');
		}

		$this->load->config('config');
		$this->load->model('post_categories_model');
		$this->load->model('categories_model');
		$this->load->model('posts_model');
		$this->load->model('sidebars_model');
		$this->load->language('posts');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
		$this->acl->restrict('website.posts.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('website/posts'));
		$this->breadcrumbs->push(lang('index_heading'), site_url('website/posts'));

		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');

		// render the page
		// $this->template->add_css(module_css('website', 'posts_index'), 'embed');

		$this->template->add_js(module_js('website', 'posts_index'), 'embed');
		$this->template->write_view('content', 'posts_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('website.posts.list');

		echo $this->posts_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('website.posts.' . $action);

		// page title
		$data['page_heading'] = lang($action . '_heading');
		$data['page_subhead'] = lang($action . '_subhead');
		$data['action'] = $action;
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('website/posts'));
		$this->breadcrumbs->push(lang('index_heading'), site_url('website/posts'));
		$this->breadcrumbs->push(lang($action . '_heading'), site_url('website/posts/' . $action));

		if ($this->input->post())
		{
			if ($post_id = $this->_save($action, $id))
			{
				
				$this->session->set_flashdata('flash_message',  lang($action . '_success'));
				echo json_encode(array('success' => true, 'action' => $action, 'id' => $post_id, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'post_title'		=> form_error('post_title'),
					'post_content'		=> form_error('post_content'),
					'post_order'		=> form_error('post_order'),
					'post_categories'	=> form_error('post_categories[]'),
					//'post_photo'		=> form_error('post_photo')
				);
				echo json_encode($response);
				exit;
			}
		}

		$current_categories = array();
		if ($action != 'add') 
		{
			$data['record'] = $this->posts_model->find($id);
			$current_categories = $this->post_categories_model->get_current_categories($id);
		}

		$data['categories'] = $this->categories_model->get_category_checkboxes();
		$data['current_categories'] = array_keys($current_categories);


		$this->template->add_js('components/dropzone/dist/dropzone.js');
		$this->template->add_css('components/dropzone/dist/dropzone.css');

		$this->template->add_js('components/select2/dist/js/select2.min.js');
		$this->template->add_css('components/select2/dist/css/select2.min.css');
		
		$this->template->add_js('components/ckeditor/ckeditor.js');
		
		$this->template->add_js(module_js('website', 'posts_form'), 'embed');
		$this->template->write_view('content', 'posts_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function delete($id)
	{
		$this->acl->restrict('website.posts.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->posts_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('post_title', lang('post_title'), 'required');
		$this->form_validation->set_rules('post_content', lang('post_content'), 'required');
		$this->form_validation->set_rules('post_order', lang('post_order'), 'required');
		$this->form_validation->set_rules('post_categories[]', lang('post_categories'), 'required');
		//$this->form_validation->set_rules('post_photo', lang('post_photo'), 'required');

		$this->form_validation->set_message('required', 'This field is required.');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'post_title'		 => $this->input->post('post_title'),
			'post_title_color'	 => $this->input->post('post_title_color'),
			'post_slug'			 => url_title($this->input->post('post_title'), '-', TRUE),
			'post_excerpt'		 => $this->input->post('post_excerpt'),
			'post_excerpt_color' => $this->input->post('post_excerpt_color'),
			'post_content'		 => $this->input->post('post_content'),
			'post_order'		 => $this->input->post('post_order'),
			'post_posted_on'	 => date('Y-m-d'),
			'post_layout'		 => '',
			'post_sidebar_id'	 => '',
			'post_status'		 => 'Posted',
			'post_photo'		 => $this->input->post('post_photo')
		);

		if ($action == 'add')
		{
			$id = $this->posts_model->insert($data);
			$return = (is_numeric($id)) ? $id : FALSE;
		}
		else if ($action == 'edit')
		{
			$this->posts_model->update($id, $data);

			// delete the existing categories
			$this->post_categories_model->delete_where(array('post_category_post_id' => $id));

			// delete the cache
			$this->output->delete_cache('/post/' . url_title($this->input->post('post_title'), '-', TRUE));

			$return = $id;
		}

		// add the categories
		$categories = $this->input->post('post_categories');
		// pr($categories);
		if ($categories)
		{
			foreach ($categories as $category)
			{
				$data = array(
					'post_category_post_id' => $id,
					'post_category_category_id' => $category
				);
				$this->post_categories_model->insert($data);
			}
		}



		return $return;

	}
}

/* End of file Posts.php */
/* Location: ./application/modules/website/controllers/Posts.php */