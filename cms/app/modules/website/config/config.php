<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// load the theme's config
// require FCPATH . 'themes/frontend/' . config_item('website_theme') . '/conf/config.php';

$config['theme_layouts'] = array(
	'full_width' => 'Full Width',
	'narrow_width' => 'Narrow Width',
	'right_sidebar' => 'Right Sidebar',
	'left_sidebar' => 'Left Sidebar',
);

$config['theme_widget_sections'] = array(
	'home_col_left'		  => 'Homepage Column Left',	
	'home_col_right' 	  => 'Homepage Column Right',
	'who_we_are'		  => 'Who we are',
	'about'				  => 'About',
	'sidebar' 			  => 'Sidebar',
	'footer1' 			  => 'Footer 1',
	'footer2' 			  => 'Footer 2',
	'footer3' 			  => 'Footer 3',
	'footer4' 			  => 'Footer 4',
	'footer5'			  => 'Footer 5'
);

$config['theme_templates'] = array(
	'default' => 'Default',
	'eplife'  => 'ePLife',
);