<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Partials_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Partials_model extends BF_Model 
{

	protected $table_name			= 'partials';
	protected $key					= 'partial_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'partial_created_on';
	protected $created_by_field		= 'partial_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'partial_modified_on';
	protected $modified_by_field	= 'partial_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'partial_deleted';
	protected $deleted_by_field		= 'partial_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'partial_id', 
			'partial_title',
			'partial_status'
		);

		return $this->datatables($fields);
	}
}