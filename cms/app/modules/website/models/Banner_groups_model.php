<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Banner_groups_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Banner_groups_model extends BF_Model 
{

	protected $table_name			= 'banner_groups';
	protected $key					= 'banner_group_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'banner_group_created_on';
	protected $created_by_field		= 'banner_group_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'banner_group_modified_on';
	protected $modified_by_field	= 'banner_group_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'banner_group_deleted';
	protected $deleted_by_field		= 'banner_group_deleted_by';

	/**
	 * get departments dropdown
	 *
	 * @param mixed $key
	 * @param mixed $val
	 * @param boolean $blank
	 * @author Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_banner_groups_dropdown($key, $val, $blank = FALSE)
	{
		if ( ! $banner_groups_dropdown = $this->cache->get('banners_dropdown'))
        {
			$banner_groups_dropdown = $this->where('banner_group_deleted', 0)
										   ->order_by('banner_group_name', 'ASC')
										   ->format_dropdown($key, $val, $blank);

			$this->cache->save('banners_dropdown', $banner_groups_dropdown, 300); // TTL in seconds
		}

		return $banner_groups_dropdown;
	}
}