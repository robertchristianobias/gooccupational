<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sidebar_widgets_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Sidewidgets_model extends BF_Model 
{

	protected $table_name			= 'sidewidgets';
	protected $key					= 'sidewidget_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'sidewidget_created_on';
	protected $created_by_field		= 'sidewidget_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'sidewidget_modified_on';
	protected $modified_by_field	= 'sidewidget_modified_by';

	protected $soft_deletes			= FALSE;
	protected $deleted_field		= 'sidewidget_deleted';
	protected $deleted_by_field		= 'sidewidget_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * 
	 */
	public function get_datatables()
	{
		$fields = array(
			'sidebar_widget_id', 
			'sidebar_widget_name',

			'sidebar_widget_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'sidebar_widget_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = sidebar_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = sidebar_modified_by', 'LEFT')
					->datatables($fields);
	}

	// --------------------------------------------------------------------

	
}