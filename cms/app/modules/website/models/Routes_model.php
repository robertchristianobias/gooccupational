<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Routes_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Routes_model extends BF_Model {

	protected $table_name			= 'routes';
	protected $key					= 'route_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'route_created_on';
	protected $created_by_field		= 'route_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'route_modified_on';
	protected $modified_by_field	= 'route_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'route_deleted';
	protected $deleted_by_field		= 'route_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'route_id',
			'route_slug',
			'route_controller',

			'route_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'route_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = route_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = route_modified_by', 'LEFT')
					->datatables($fields);
	}
}