<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Edit_posts_003 extends CI_Migration 
{
	private $_table = 'posts';

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{
		$fields = array(
			'post_order' => array('type' => 'INT', 'constraint' => 11, 'null' => FALSE, 'default' => '0'),
		);
		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		// drop the columns
		$this->dbforge->drop_column($this->_table, 'post_order');
	}
}