<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Add_configs_02 extends CI_Migration 
{
	var $table = 'configs';

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{
		$this->db->insert($this->table, array('config_type'  => 'textarea', 'config_label'  => 'Map Embed Code', 	'config_name' => 'company_map_embed_code', 'config_value' => '', 'config_notes' => 'Company Map embed code'));
		$this->db->insert($this->table, array('config_type'  => 'input', 	'config_label'  => 'Company Phone No.', 'config_name' => 'company_phone_no', 	   'config_value' => '', 'config_notes' => 'Company Phone No.'));
		$this->db->insert($this->table, array('config_type'  => 'textarea', 'config_label'  => 'Company Address', 	'config_name' => 'company_address', 	   'config_value' => '', 'config_notes' => 'Company Address'));
	}

	public function down()
	{
		$this->db->delete($this->table, array('config_name' => 'company_map_embed_code'));
		$this->db->delete($this->table, array('config_name' => 'company_phone_no'));
		$this->db->delete($this->table, array('config_name' => 'company_address'));
	}
}