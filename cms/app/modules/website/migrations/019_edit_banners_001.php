<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Edit_banners_001 extends CI_Migration 
{
	private $_table = 'banners';

	public function __construct() 
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'banner_title' => array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE, 'after' => 'banner_banner_group_id'),
		);
		
		$this->dbforge->add_column($this->_table, $fields);
		$this->db->query('ALTER TABLE ' . $this->_table . ' ADD INDEX `banner_title` (`banner_title`)');
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'banner_title');
	}
}