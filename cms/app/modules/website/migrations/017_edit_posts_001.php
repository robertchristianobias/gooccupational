<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Edit_posts_001 extends CI_Migration 
{
	private $_table = 'posts';

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{
		$fields = array(
			'post_sidebar_id' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
		);

		$this->dbforge->add_column($this->_table, $fields);
		$this->db->query('ALTER TABLE ' . $this->_table . ' ADD INDEX `post_sidebar_id` (`post_sidebar_id`)');
	}

	public function down()
	{
		// drop the columns
		$this->dbforge->drop_column($this->_table, 'post_sidebar_id');
	}
}