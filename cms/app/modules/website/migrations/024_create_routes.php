<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_routes extends CI_Migration {

	private $_table = 'routes';

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		// create the table
		$fields = array(
			'route_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'route_slug'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'route_controller'	=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'route_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'route_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'route_modified_by' => array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'route_modified_on' => array('type' => 'DATETIME', 'null' => TRUE),
			'route_deleted' 	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE, 'default' => 0),
			'route_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('route_id', TRUE);
		$this->dbforge->add_key('route_slug');

		$this->dbforge->add_key('route_deleted');
		$this->dbforge->create_table($this->_table, TRUE);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);
	}
}