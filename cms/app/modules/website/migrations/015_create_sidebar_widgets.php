<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_sidebar_widgets extends CI_Migration 
{
	private $_table = 'sidewidgets';

	private $_permissions = array(
		array('Sidebar Widgets Link', 'website.sidewidgets.link'),
		array('Sidebar Widgets List', 'website.sidewidgets.list'),
		array('View Sidebar Widgets', 'website.sidewidgets.view'),
		array('Add Sidebar Widgets', 'website.sidewidgets.add'),
		array('Edit Sidebar Widgets', 'website.sidewidgets.edit'),
		array('Delete Sidebar Widgets', 'website.sidewidgets.delete'),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'sidewidget_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'sidewidget_sidebar_id'		=> array('type' => 'SMALLINT', 'constraint' => 5, 'unsigned' => TRUE, 'null' => FALSE),
			'sidewidget_type'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => FALSE),
			'sidewidget_name'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => FALSE),
			'sidewidget_function'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'sidewidget_source_table'	=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'sidewidget_source_id'		=> array('type' => 'INT', 'constraint' => 5, 'unsigned' => TRUE, 'null' => FALSE),
			'sidewidget_order'			=> array('type' => 'TINYINT', 'constraint' => 2, 'unsigned' => TRUE, 'null' => FALSE),
			'sidewidget_status'			=> array('type' => 'SET("Active","Disabled")', 'null' => FALSE),

			'sidewidget_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'sidewidget_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'sidewidget_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'sidewidget_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'sidewidget_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE, 'default' => 0),
			'sidewidget_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('sidewidget_id', TRUE);
		$this->dbforge->add_key('sidewidget_sidebar_id');
		$this->dbforge->add_key('sidewidget_type');
		$this->dbforge->add_key('sidewidget_name');
		$this->dbforge->add_key('sidewidget_function');
		$this->dbforge->add_key('sidewidget_source_table');
		$this->dbforge->add_key('sidewidget_source_id');
		$this->dbforge->add_key('sidewidget_order');
		$this->dbforge->add_key('sidewidget_status');

		$this->dbforge->add_key('sidewidget_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the initial values
		$data = array(
			array(
				'sidewidget_sidebar_id'  	=> '1',
				'sidewidget_type'  			=> 'Partial',
				'sidewidget_name'  			=> 'Sample',
				'sidewidget_function'  		=> 'frontend_partial_widget(1)',
				'sidewidget_source_table' 	=> 'partials',
				'sidewidget_source_id'  	=> '1',
				'sidewidget_order'  		=> '0',
				'sidewidget_status'  		=> 'Active'
			)
		);
		$this->db->insert_batch($this->_table, $data);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, true);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);
	}
}