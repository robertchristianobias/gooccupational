<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Edit_posts_004 extends CI_Migration 
{
	private $_table = 'posts';

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{
		$fields = array(
			'post_title_color' 	 => array('type' => 'VARCHAR', 'constraint' => 20, 'null' => FALSE, 'default' => "#000000", 'after' => 'post_title'),
			'post_excerpt_color' => array('type' => 'VARCHAR', 'constraint' => 20, 'null' => FALSE, 'default' => "#000000", 'after' => 'post_excerpt'),

		);
		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		// drop the columns
		$this->dbforge->drop_column($this->_table, 'post_title_color');
		$this->dbforge->drop_column($this->_table, 'post_excerpt_color');
	}
}