<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Edit_pages_003 extends CI_Migration 
{
	private $_table = 'pages';

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{
		$fields = array(
			'page_banner_group_id' => array('type' => 'INT', 'constraint' => 10, 'null' => FALSE, 'after' => 'page_id'),
		);
		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'page_banner_group_id');
	}
}