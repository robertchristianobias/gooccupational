<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_partials extends CI_Migration 
{
	private $_table = 'partials';

	private $_permissions = array(
		array('Partials Link', 'website.partials.link'),
		array('Partials List', 'website.partials.list'),
		array('View Partial', 'website.partials.view'),
		array('Add Partial', 'website.partials.add'),
		array('Edit Partial', 'website.partials.edit'),
		array('Delete Partial', 'website.partials.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'website', // 'none' if parent menu or single menu; or menu_link of parent
			'menu_text' 		=> 'Partials', 
			'menu_link' 		=> 'website/partials', 
			'menu_perm' 		=> 'website.partials.link', 
			'menu_icon' 		=> 'fa fa-th', 
			'menu_order' 		=> 6, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'partial_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'partial_title'			=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'partial_content'		=> array('type' => 'TEXT', 'null' => FALSE),
			'partial_status'		=> array('type' => 'SET("Posted","Draft")', 'null' => FALSE),

			'partial_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'partial_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'partial_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'partial_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'partial_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE, 'default' => 0),
			'partial_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('partial_id', TRUE);
		$this->dbforge->add_key('partial_title');
		$this->dbforge->add_key('partial_status');

		$this->dbforge->add_key('partial_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);

		// add the initial values
		$data = array(
			array('partial_title'  => 'Sample Partial Content', 'partial_content' => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.', 'partial_status' => 'Posted', 'partial_created_by' => 1)
		);
		$this->db->insert_batch($this->_table, $data);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}