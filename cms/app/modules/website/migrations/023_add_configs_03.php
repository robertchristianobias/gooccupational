<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Add_configs_03 extends CI_Migration 
{
	var $table = 'configs';

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{
		$this->db->insert($this->table, array('config_type'  => 'input', 	'config_label'  => 'CMS URL', 'config_name' => 'cms_url', 'config_value' => site_url(), 'config_notes' => 'CMS URL'));
	}

	public function down()
	{
		$this->db->delete($this->table, array('config_name' => 'cms_url'));
	}
}