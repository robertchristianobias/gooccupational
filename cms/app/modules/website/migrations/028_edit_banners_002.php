<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Edit_banners_002 extends CI_Migration 
{
	private $_table = 'banners';

	public function __construct() 
	{
		parent::__construct();
	}
	
	public function up()
	{
		$fields = array(
			'banner_button' => array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE, 'after' => 'banner_caption'),
		);
		
		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'banner_button');
	}
}