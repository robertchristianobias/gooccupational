<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Edit_navigations_001 extends CI_Migration 
{
	private $_table = 'navigations';

	public function __construct() 
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$this->db->query("ALTER TABLE " . $this->_table . " CHANGE COLUMN `navigation_target` `navigation_target` SET('_top','_blank','_scroll') NOT NULL DEFAULT '_top' AFTER `navigation_link`");
	}

	public function down() {}
}