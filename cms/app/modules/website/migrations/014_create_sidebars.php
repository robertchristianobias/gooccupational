<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_sidebars extends CI_Migration 
{
	private $_table = 'sidebars';

	private $_permissions = array(
		array('Sidebars Link', 'website.sidebars.link'),
		array('Sidebars List', 'website.sidebars.list'),
		array('View Sidebars', 'website.sidebars.view'),
		array('Add Sidebars', 'website.sidebars.add'),
		array('Edit Sidebars', 'website.sidebars.edit'),
		array('Delete Sidebars', 'website.sidebars.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'website', // 'none' if parent menu or single menu; or menu_link of parent
			'menu_text' 		=> 'Sidebars', 
			'menu_link' 		=> 'website/sidebars', 
			'menu_perm' 		=> 'website.sidebars.link', 
			'menu_icon' 		=> 'fa fa-list-alt', 
			'menu_order' 		=> 7, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'sidebar_id' 			=> array('type' => 'SMALLINT', 'constraint' => 5, 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'sidebar_name'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => FALSE),

			'sidebar_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'sidebar_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'sidebar_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'sidebar_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'sidebar_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE, 'default' => 0),
			'sidebar_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('sidebar_id', TRUE);
		$this->dbforge->add_key('sidebar_name');

		$this->dbforge->add_key('sidebars_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);

		// add the initial values
		$data = array(
			array('sidebar_name'  => 'Default'),
		);
		$this->db->insert_batch($this->_table, $data);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, true);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}