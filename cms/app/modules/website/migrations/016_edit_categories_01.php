<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Edit_categories_01 extends CI_Migration 
{
	private $_table = 'categories';

	public function __construct() 
	{
		parent::__construct();

		// check for dependencies
		if (! $this->db->table_exists('posts'))
		{
			echo json_encode(array('success' => FALSE, 'message' => 'This upgrade requires the Websites module')); exit;
		}

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'category_sidebar_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
		);
		
		$this->dbforge->add_column($this->_table, $fields);
		$this->db->query('ALTER TABLE ' . $this->_table . ' ADD INDEX `category_sidebar_id` (`category_sidebar_id`)');
	
		// set default sidebar
		$data = array('category_sidebar_id'  => '1');
		$this->db->where_in('category_layout', array('right_sidebar', 'left_sidebar'));
		$this->db->update($this->_table, $data);
	}

	public function down()
	{
		// $this->dbforge->drop_column($this->_table, 'post_metatag_id');
	}
}