<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h5 class="modal-title" id="modalLabel"><?php echo $page_heading?></h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="">
						<div class="panel-wrapper collapse in">
							<div class="panel-body pa-0">
								<div class="col-sm-12 col-xs-12">
									<div class="form-wrap">
										<?php echo form_open(current_url(), array('class'=>'form', 'id'=>'schedules_form'));?>
											<div class="form-body overflow-hide">
												<div class="form-group">
													<label class="control-label mb-10" for="schedule_title"><?php echo lang('schedule_title'); ?></label>
                                                    <?php echo form_input('schedule_title', set_value('schedule_title', isset($record->schedule_title) ? $record->schedule_title : ''), 'class="form-control" '); ?>
                                                    <div id="error-schedule_title"></div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="schedule_description"><?php echo lang('schedule_description'); ?></label>
                                                    <?php echo form_textarea('schedule_description', set_value('schedule_description', isset($record->schedule_description) ? $record->schedule_description : '', TRUE), 'id="ckeditor" class="form-control ckeditor" '); ?>
                                                    <div id="error-schedule_description"></div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="schedule_time"><?php echo lang('schedule_time'); ?></label>
													<div class="row">
														<div class="col-md-4">
															<label class="control-label">From </label>
															<input type="time" name="schedule_time_from" class="form-control">
															<div id="error-schedule_time_from"></div>
														</div>
														<div class="col-md-4">
															<label class="control-label">To </label>
															<input type="time" name="schedule_time_to" class="form-control">
															<div id="error-schedule_time_to"></div>
														</div>
													</div>
												</div>
											</div>
										<?php echo form_close(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">
				<i class="fa fa-times"></i> <?php echo lang('button_close')?>
			</button>
			<button id="submit" class="btn btn-success btn-anim" type="submit" data-loading-text="<?php echo lang('processing')?>">
                <i class="fa fa-save"></i> 
                <span class="btn-text"><?php echo lang('button_add'); ?></span>
            </button>
		</div>
	</div>
</div>