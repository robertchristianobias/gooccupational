<?php echo form_open(current_url(), 'id="schedules_form"'); ?>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#info"><i class="fa fa-info-circle" aria-hidden="true"></i> Info</a></li>
	</ul>
	<div class="tab-content">
		<div id="info" class="tab-pane fade in active">
			<div class="row">
				<div class="col-md-9">
					<div class="form-group">
						<label class="control-label" for="schedule_date"><?php echo lang('schedule_date')?>:</label>
						<?php
							$schedule_date = '';
							if ( isset($record->schedule_date_from) && isset($record->schedule_date_to) )
							{
								$schedule_date = date('F d, Y', strtotime($record->schedule_date_from)) . ' - ' . date('F d, Y', strtotime($record->schedule_date_to));
							}
							echo form_input(array('id'=>'schedule_date', 'name'=>'schedule_date', 'value'=>set_value('schedule_date', $schedule_date), 'class'=>'form-control datepicker', 'readonly'=>'readonly'));
						?>
						<div id="error-schedule_date"></div>
					</div>
					<div class="form-group">
						<label class="control-label" for="schedule_title"><?php echo lang('schedule_title')?>:</label>
						<?php echo form_input(array('id'=>'schedule_title', 'name'=>'schedule_title', 'value'=>set_value('schedule_title', isset($record->schedule_title) ? $record->schedule_title : '', FALSE), 'class'=>'form-control'));?>
						<div id="error-schedule_title"></div>
					</div>
					
					<div class="form-group">
						<label class="control-label" for="schedule_description"><?php echo lang('schedule_description')?>:</label>
						<?php echo form_textarea(array('id'=>'schedule_description', 'name'=>'schedule_description', 'rows'=>'3', 'value'=>set_value('schedule_description', isset($record->schedule_description) ? $record->schedule_description : '', FALSE), 'class'=>'form-control ckeditor')); ?>
						<div id="error-schedule_description"></div>
					</div>
					
				</div>
				<div class="col-md-3">
					
				
					
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label class="control-label" for="schedule_time"><?php echo lang('schedule_time')?>:</label>
							</div>
							<div class="col-md-6">
								<?php echo form_input(array('id'=>'schedule_time_from', 'name'=>'schedule_time_from', 'value'=>set_value('schedule_time_from', isset($record->schedule_time_from) ? ($record->schedule_time_from != '00:00:00' ? date('h:ia', strtotime($record->schedule_time_from)) : '') : ''), 'placeholder'=>'From', 'class'=>'form-control timepicker'));?>
								<div id="error-schedule_time_from"></div>
							</div>
							<div class="col-md-6">
								<?php echo form_input(array('id'=>'schedule_time_to', 'name'=>'schedule_time_to', 'value'=>set_value('schedule_time_to', isset($record->schedule_time_to) ? ($record->schedule_time_to != '00:00:00' ? date('h:ia', strtotime($record->schedule_time_to)) : '' ) : ''), 'placeholder'=>'To', 'class'=>'form-control timepicker'));?>
								<div id="error-schedule_time_to"></div>
							</div>
						</div>
					</div>

					<div class="top-margin5">
						<?php if ($action == 'add'): ?>
							<button class="btn btn-success btn-sm submit" type="submit" data-status="Published" data-loading-text="<?php echo lang('processing')?>">
								<i class="fa fa-plus"></i> <?php echo lang('button_publish')?>
							</button>
						<?php elseif ($action == 'edit'): ?>
							<button class="btn btn-success btn-sm submit" type="submit" data-status="Published" data-loading-text="<?php echo lang('processing')?>">
								<i class="fa fa-save"></i> <?php echo lang('button_publish')?>
							</button>
						<?php endif; ?>
						<button class="btn btn-primary btn-sm submit" type="submit" data-status="Draft" data-loading-text="<?php echo lang('processing')?>">
							<i class="fa fa-plus"></i> <?php echo lang('button_draft')?>
						</button>
					</div>
				</div>
			</div>
		</div>
		
	</div>

<?php echo form_close(); ?>
