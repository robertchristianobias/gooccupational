<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h5 class="modal-title" id="modalLabel"><?php echo $page_heading?></h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="">
						<div class="panel-wrapper collapse in">
							<div class="panel-body pa-0">
								<div class="col-sm-12 col-xs-12">
									<div class="form-wrap">
										<?php echo form_open(current_url(), array('class'=>'form', 'id'=>'schedules_form'));?>
											<div class="form-body overflow-hide">
												<div class="form-group">
													<label class="control-label mb-10" for="schedule_title"><?php echo lang('schedule_title'); ?></label>
                                                    <div>
                                                        <?php echo $schedule->schedule_title; ?>
                                                    </div>
                                                </div>
												<div class="form-group">
													<label class="control-label mb-10" for="schedule_description"><?php echo lang('schedule_description'); ?></label>
                                                    <div>
                                                        <?php echo $schedule->schedule_description; ?>
                                                    </div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="schedule_date"><?php echo lang('schedule_date'); ?></label>
                                                    <div>
                                                        <?php echo date('F d, Y', strtotime($schedule->schedule_date_from)) . ' - ' . date('F d, Y', strtotime($schedule->schedule_date_to )); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
													<label class="control-label mb-10" for="schedule_date"><?php echo lang('schedule_time'); ?></label>
                                                    <div>
                                                        <?php echo date('H:i a', strtotime($schedule->schedule_time_from)); ?> - <?php echo date('H:i a', strtotime($schedule->schedule_time_to)); ?>
                                                    </div>
												</div>
											</div>
														
										<?php echo form_close(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</div>
</div>