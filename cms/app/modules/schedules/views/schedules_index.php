<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark"><?php echo $page_heading; ?></h3>
				</div>
				<div class="pull-right">
					<?php if ($this->acl->restrict('schedules.schedules.add', 'return')): ?>
						<a href="<?php echo site_url('schedules/schedules/form/add')?>" class="btn btn-sm btn-success btn-add" id="btn_add"><span class="fa fa-plus"></span> <?php echo lang('button_add')?></a>
					<?php endif; ?>
				</div>
				<div class="clearfix"></div>
			</div> 
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<textarea id="events" class="hidden"><?php echo $events; ?></textarea>

					<div class="calendar-wrap mt-40">
						<div id="calendar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<a href="<?php echo site_url('schedules/schedule_modal'); ?>" data-target="#modal" data-toggle="modal" id="modal_schedule"></a> 
<a href="<?php echo site_url('schedules/view'); ?>" data-target="#modal" data-toggle="modal" id="schedule_view"></a> 
