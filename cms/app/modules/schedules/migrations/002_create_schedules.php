<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Migration_Create_schedules extends CI_Migration {

	private $_table = 'schedules';

	private $_permissions = array(
		array('Schedules Link', 'schedules.schedules.link'),
		array('Schedules List', 'schedules.schedules.list'),
		array('View Schedule', 'schedules.schedules.view'),
		array('Add Schedule', 'schedules.schedules.add'),
		array('Edit Schedule', 'schedules.schedules.edit'),
		array('Delete Schedule', 'schedules.schedules.delete'),
		array('Schedules Setting', 'schedules.schedules.settings')
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'none', // none if parent or single menu
			'menu_text' 		=> 'Schedules', 
			'menu_link' 		=> 'schedules', 
			'menu_perm' 		=> 'schedules.schedules.link', 
			'menu_icon' 		=> 'fa fa-bullhorn', 
			'menu_order' 		=> 4, 
			'menu_active' 		=> 1
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'schedule_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'schedule_title'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'schedule_description'	=> array('type' => 'TEXT', 'null' => FALSE),
			'schedule_date_from'	=> array('type' => 'DATE', 'null' => FALSE),
			'schedule_date_to'		=> array('type' => 'DATE', 'null' => FALSE),

			'schedule_time_from'	=> array('type' => 'TIME', 'null' => FALSE),
			'schedule_time_to'		=> array('type' => 'TIME', 'null' => FALSE),
			'schedule_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'schedule_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'schedule_modified_by' => array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'schedule_modified_on' => array('type' => 'DATETIME', 'null' => TRUE),
			'schedule_deleted' 	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'schedule_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('schedule_id', TRUE);
		$this->dbforge->add_key('schedule_title');

		$this->dbforge->add_key('schedule_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		//$this->dbforge->drop_table($this->_table, TRUE);
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}