<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Appointments Language File (English)
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']			= 'Appointments'; 

// Labels
$lang['schedule_id']			    = 'ID';
$lang['schedule_title']			= 'Title';
$lang['schedule_description']		= 'Description';
$lang['schedule_time_from']        = 'From';
$lang['schedule_time_to']          = 'To';
$lang['schedule_date'] = 'Date';
$lang['schedule_time']        = 'Time';

// Buttons
$lang['button_add']				= 'Add appointment';
$lang['button_update']			= 'Update appointment';
$lang['button_delete']			= 'Delete appointment';
$lang['button_edit_this']		= 'Edit This';
$lang['button_publish'] = 'Publish';
$lang['button_draft'] = 'Save as Draft';

// Index Function
$lang['index_heading']			= 'Appointments';
$lang['index_subhead']			= 'List of all Appointments';
$lang['index_id']			    = 'Id';
$lang['index_date']			    = 'Date';
$lang['index_title']			= 'Title';

$lang['index_created_on']		= 'Created On';
$lang['index_created_by']		= 'Created By';
$lang['index_modified_on']		= 'Modified On';
$lang['index_modified_by']		= 'Modified By';
$lang['index_status']			= 'Status';
$lang['index_action']			= 'Action';

// View Function
$lang['view_heading']			= 'View appointment';

// Add Function
$lang['add_heading']	   = 'Add appointment';
$lang['add_success']	   = 'appointment has been successfully added';

// Edit Function
$lang['edit_heading']	   = 'Edit appointment';
$lang['edit_success']	   = 'appointment has been successfully updated';

// Delete Function
$lang['delete_heading']	   = 'Delete appointment';
$lang['delete_confirm']	   = 'Are you sure you want to delete this appointment?';
$lang['delete_success']	   = 'appointment has been successfully deleted';

$lang['attendees_heading']     = 'Attendees';
$lang['attendees_subhead']     = 'List of all appointment attendees';
$lang['crumb_attendees']       = 'Attendees';

$lang['index_attendees_employee_no'] = 'Employee No.';
$lang['index_attendees_first_name'] = 'First Name';
$lang['index_attendees_last_name']  = 'Last Name';
$lang['index_attendees_email'] = 'Email';
$lang['index_attendees_will_attend'] = 'Will Attend';