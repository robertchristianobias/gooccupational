<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Schedules_model Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Schedules_model extends BF_Model {

	protected $table_name			= 'schedules';
	protected $key					= 'schedule_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'schedule_created_on';
	protected $created_by_field		= 'schedule_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'schedule_modified_on';
	protected $modified_by_field	= 'schedule_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'schedule_deleted';
	protected $deleted_by_field		= 'schedule_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'schedule_id',
			'schedule_title'
		);

		return $this->datatables($fields);
	}

	public function get_schedules()
	{
		$schedules = $this->schedules_model->where('schedule_deleted', 0)
										   ->find_all();

		return $schedules;
	}
}