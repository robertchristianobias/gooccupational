/**
 * @package     Codeigniter
 * @version     1.0
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2014-2015, Robert Christian Obias
 * @link        robertchristianobias@gmail.com
 */
$(function() {
    
    var oTable = $('#datatables').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "roles/datatables",
        "lengthMenu": [[50, 100, 300, -1], [50, 100, 300, "All"]],
        "pagingType": "full_numbers",
        "language": {
            "paginate": {
                "previous": 'Prev',
                "next": 'Next',
            }
        },
        "bAutoWidth": false,
        "aaSorting": [[ 0, "asc" ]],
        "aoColumnDefs": [
            
            {
                "aTargets": [0],
                "sClass": "text-center",
            },

            {
                "aTargets": [3],
                "bSortable": false,
                "mRender": function (data, type, full) {

                    html = '<a href="roles/edit/'+full[0]+'" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-default btn-icon-anim btn-circle"><i class="fa fa-pencil"></i></a>';
                    html += ' <a href="roles/access/'+full[0]+'" tooltip-toggle="tooltip" data-placement="top" title="Permissions" class="btn btn-info btn-icon-anim btn-circle"><i class="fa fa-lock"></i></a>';

                    if (full[0] != 1) {
                        html += ' <a href="roles/delete/'+full[0]+'" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-icon-anim btn-circle"><i class="fa fa-trash-o"></i></a>';
                    }
                    
                    return html;
                },
                "sClass": "col-md-2 text-center",
            },
        ]
    });

    // positions the button next to searchbox
    $('.btn-actions').prependTo('div.dataTables_filter');

    // executes functions when the modal closes
    $('body').on('hidden.bs.modal', '.modal', function () {        
        // eg. destroys the wysiwyg editor
    });

} );
