$(function() {

    $('#signup').click(function(e) {

        e.preventDefault();
        var $form     = $('#form');
            $ajax_url = $form.attr('action');
            
        $.post($ajax_url, $form.serializeArray(), function(data) {
            var $response = $.parseJSON(data);

            if ($response.errors) {
                for (var form_name in $response.errors) {
                    $('#error-' + form_name).html($response.errors[form_name]);
                }
            } else {
                // closes the modal
                $('#modal').modal('hide'); 
                
                swal({
                    type:  'success',
                    title: 'Registration',
                    text:  $response.message
                });

                window.location.replace($response.redirect);
            }
        });
    })
});