$(function() {
    $('#submit').click(function(e) {

        e.preventDefault();
        var $form     = $('#form-invite');
            $ajax_url = $form.attr('action');
            
        $.post($ajax_url, $form.serializeArray(), function(data) {
            var $response = $.parseJSON(data);

            if ($response.errors) {
                for (var form_name in $response.errors) {
                    $('#error-' + form_name).html($response.errors[form_name]);
                }
            } else {
                // closes the modal
                $('#modal').modal('hide'); 
                
                $('#datatables').dataTable().fnDraw();
                
                swal({
                    type:  'success',
                    title: 'Invite User',
                    text:  $response.message
                });
            }
        });
    })
});