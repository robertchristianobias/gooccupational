<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h5 class="modal-title" id="modalLabel"><?php echo $page_heading?></h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="">
						<div class="panel-wrapper collapse in">
							<div class="panel-body pa-0">
								<div class="col-sm-12 col-xs-12">
									<div class="form-wrap">
										<?php echo form_open(current_url(), 'id="form-invite"');?>
											<div class="form-body overflow-hide">
												<div class="form-group">
													<label class="control-label mb-10" for="email">Email address</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
														<?php echo form_input('email', set_value('email', isset($record->email) ? $record->email : ''), 'class="form-control"'); ?>
                                                    </div>
                                                    <div id="error-email"></div>
												</div>
											</div>
										<?php echo form_close(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">
				<i class="fa fa-times"></i> <?php echo lang('button_close')?>
			</button>
			<button id="submit" class="btn btn-success btn-anim" type="submit" data-loading-text="<?php echo lang('processing')?>">
                <i class="fa fa-save"></i> 
                <span class="btn-text">Invite User</span>
            </button>
		</div>
	</div>
</div>