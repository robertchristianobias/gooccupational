<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark"><?php echo $page_heading; ?></h3>
				</div>
				<div class="pull-right">
					<a href="<?php echo site_url('users/roles/add')?>" data-toggle="modal" data-target="#modal" class="btn btn-sm btn-primary btn-add" id="btn_add"><span class="fa fa-plus"></span> <?php echo lang('button_add')?></a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<table class="table table-hover table-bordered display mb-30" id="datatables">
						<thead> 
							<tr>
								<th class="all"><?php echo lang('index_th_id')?></th>
								<th class="all"><?php echo lang('index_th_role')?></th>
								<th class="min-tablet"><?php echo lang('index_th_description')?></th>
								<th class="min-tablet"><?php echo lang('index_th_action')?></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
