<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h5 class="modal-title" id="modalLabel"><?php echo $page_heading?></h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="">
						<div class="panel-wrapper collapse in">
							<div class="panel-body pa-0">
								<div class="col-sm-12 col-xs-12">
									<div class="form-wrap">
										<?php echo form_open(current_url(), array('class'=>'form'));?>
											<div class="form-body overflow-hide">
												<div class="form-group">
													<label class="control-label mb-10" for="first_name">First Name</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-user"></i></div>
														<?php echo form_input('first_name', set_value('first_name', isset($record->first_name) ? $record->first_name : ''), 'class="form-control" '); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="last_name">Last Name</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-user"></i></div>
														<?php echo form_input('last_name', set_value('last_name', isset($record->last_name) ? $record->last_name : ''), 'class="form-control" '); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="email">Email address</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
														<?php echo form_input('email', set_value('email', isset($record->email) ? $record->email : ''), 'class="form-control"'); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="company">Company</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
														<?php echo form_input('company', set_value('company', isset($record->company) ? $record->company : ''), 'class="form-control"'); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="phone">Contact number</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-phone"></i></div>
														<?php echo form_input('phone', set_value('phone', isset($record->phone) ? $record->phone : ''), 'class="form-control"'); ?>
													</div>
												</div>
												<div class="form-group">
													<label for="groups"><?php echo lang('groups')?>:</label>
													<select id="groups" class="select2 select2-multiple" multiple="multiple" data-placeholder="Choose">
														<?php foreach ($groups as $group):?>
															<option value="<?php echo $group->id?>"<?php echo (in_array($group->id, $current_groups)) ? ' selected': ''; ?>><?php echo $group->name?></option>
														<?php endforeach; ?>
													</select>
													<div id="error-groups"></div>
												</div>
											
											</div>
														
										<?php echo form_close(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">
				<i class="fa fa-times"></i> <?php echo lang('button_close')?>
			</button>
			<?php if ($page_type == 'add'): ?>
				<button id="submit" class="btn btn-success btn-anim" type="submit" data-loading-text="<?php echo lang('processing')?>">
					<i class="fa fa-save"></i> 
					<span class="btn-text"><?php echo lang('button_add'); ?></span>
				</button>
			<?php else: ?>
				<button id="submit" class="btn btn-success btn-anim" type="submit" data-loading-text="<?php echo lang('processing')?>">
					<i class="fa fa-save"></i> 
					<span class="btn-text"><?php echo lang('button_update'); ?></span>
				</button>
			<?php endif; ?>
		</div>
	</div>
</div>