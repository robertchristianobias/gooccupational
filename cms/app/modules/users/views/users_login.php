
<div class="page-wrapper pa-0 ma-0 auth-page">
	<div class="container-fluid">
		<div class="table-struct full-width full-height">
			<div class="table-cell vertical-align-middle auth-form-wrap">
				<div class="auth-form  ml-auto mr-auto no-float">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<div class="mb-30">
								<h3 class="text-center txt-dark mb-10">Sign in to <?php echo config_item('app_name'); ?></h3>
								<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
							</div>	
							<div class="form-wrap">
								<?php $return = ($this->input->get('return')) ? '?return=' . urlencode($this->input->get('return')) : ''; ?>
								<?php echo form_open(current_url() . $return, 'id="form"'); ?>
									<div class="form-group">
										<label class="control-label mb-10" for="identity">Email address</label>
										<input type="email" name="identity" class="form-control" required="" id="email" placeholder="Enter email">
									</div>
									<div class="form-group">
										<label class="pull-left control-label mb-10" for="password">Password</label>
										<a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="">forgot password ?</a>
										<div class="clearfix"></div>
										<input type="password" name="password" class="form-control" required="" id="password" placeholder="Enter password">
									</div>
									<!-- <div class="form-group">
										<div class="checkbox checkbox-primary pr-10 pull-left">
											<input id="checkbox_2" required="" type="checkbox">
											<label for="checkbox_2"> Keep me logged in</label>
										</div>
										<div class="clearfix"></div>
									</div> -->
									<div class="form-group text-center">
										<button type="submit" class="btn btn-success btn-rounded" id="submit">sign in</button>
									</div>
								<?php echo form_close(); ?>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
