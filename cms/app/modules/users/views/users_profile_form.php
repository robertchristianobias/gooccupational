<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h5 class="modal-title" id="myModalLabel">Edit Profile</h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="">
						<div class="panel-wrapper collapse in">
							<div class="panel-body pa-0">
								<div class="col-sm-12 col-xs-12">
									<div class="form-wrap">
										<form action="#">
											<div class="form-body overflow-hide">
												<div class="form-group">
													<label class="control-label mb-10" for="first_name">First Name</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-user"></i></div>
														<?php echo form_input('first_name', set_value('first_name', isset($record->first_name) ? $record->first_name : ''), 'class="form-control" '); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="last_name">Last Name</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-user"></i></div>
														<?php echo form_input('last_name', set_value('last_name', isset($record->last_name) ? $record->last_name : ''), 'class="form-control" '); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="email">Email address</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
														<?php echo form_input('email', set_value('email', isset($record->email) ? $record->email : ''), 'class="form-control"'); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="phone">Contact number</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-phone"></i></div>
														<?php echo form_input('phone', set_value('phone', isset($record->phone) ? $record->phone : ''), 'class="form-control"'); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputpwd_1">Password</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="icon-lock"></i></div>
														<input type="password" class="form-control" id="exampleInputpwd_1" placeholder="Enter pwd" value="password">
													</div>
												</div>
											
											</div>
														
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success waves-effect" data-dismiss="modal">Save</button>
			<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
		</div>
	</div>
</div>