<div class="page-wrapper pa-0 ma-0 auth-page">
    <div class="container-fluid">
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <h3 class="text-center txt-dark mb-10">Sign up to <?php echo config_item('app_name'); ?></h3>
                                <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                            </div>	
                            <div class="form-wrap">
                                <?php echo form_open(current_url(), 'id="form"'); ?>
                                    <div class="form-group">
                                        <label class="control-label mb-10" for="first_name"><?php echo lang('first_name'); ?></label>
                                        <?php echo form_input('first_name', set_value('first_name'), 'class="form-control" placeholder="First Name"'); ?>
                                        <div id="error-first_name"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label mb-10" for="last_name"><?php echo lang('last_name'); ?></label>
                                        <?php echo form_input('last_name', set_value('last_name'), 'class="form-control" placeholder="Last Name"'); ?>
                                        <div id="error-last_name"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="pull-left control-label mb-10" for="password">Password</label>
                                        <input type="password" name="password" class="form-control" placeholder="Enter password">
                                        <div id="error-password"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="pull-left control-label mb-10" for="confirm_password">Confirm Password</label>
                                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirm password">
                                        <div id="error-confirm_password"></div>
                                    </div>
                                    
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-success btn-rounded" id="signup">sign Up</button>
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->	
    </div>
</div>