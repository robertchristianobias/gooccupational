<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Newsfeed_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Newsfeed_model extends BF_Model {

	protected $table_name			= 'newsfeed';
	protected $key					= 'newsfeed_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'newsfeed_created_on';
	protected $created_by_field		= 'newsfeed_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'newsfeed_modified_on';
	protected $modified_by_field	= 'newsfeed_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'newsfeed_deleted';
	protected $deleted_by_field		= 'newsfeed_deleted_by';
}