<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_newsfeed extends CI_Migration {

	private $_table = 'newsfeed';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'newsfeed_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'newsfeed_user_id'		=> array('type' => 'INT', 'constraint' => 11, 'unsigned' => TRUE, 'null' => TRUE),
			'newsfeed_message'		=> array('type' => 'TEXT', 'null' => FALSE),
			
			'newsfeed_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'newsfeed_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'newsfeed_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'newsfeed_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'newsfeed_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'newsfeed_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('newsfeed_id', TRUE);
		$this->dbforge->add_key('newsfeed_user_id');

		$this->dbforge->add_key('newsfeed_deleted');
		$this->dbforge->create_table($this->_table, TRUE);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);
	}
}