<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_events extends CI_Migration {

	private $_table = 'events';

	private $_permissions = array(
		array('Events Link', 'events.events.link'),
		array('Events List', 'events.events.list'),
		array('View Event', 'events.events.view'),
		array('Add Event', 'events.events.add'),
		array('Edit Event', 'events.events.edit'),
		array('Delete Event', 'events.events.delete'),
		array('Events Settings', 'events.events.settings')
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'none', // none if parent or single menu
			'menu_text' 		=> 'Events', 
			'menu_link' 		=> 'events', 
			'menu_perm' 		=> 'events.events.link',
			'menu_icon' 		=> 'fa fa-clock-o', 
			'menu_order' 		=> 3, 
			'menu_active' 		=> 1
		)
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'event_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'event_title'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'unsigned' => TRUE, 'null' => TRUE),
			'event_author'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'event_content'	    => array('type' => 'TEXT', 'null' => FALSE),
			'event_datetime' 	=> array('type' => 'DATETIME'),
			'event_photo'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'event_status'		=> array('type' => 'VARCHAR', 'constraint' => 15, 'null' => FALSE),
			
			'event_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'event_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'event_modified_by' => array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'event_modified_on' => array('type' => 'DATETIME', 'null' => TRUE),
			'event_deleted' 	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'event_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		//$this->dbforge->add_key('event_id', TRUE);
		$this->dbforge->add_key('event_id', TRUE);

		$this->dbforge->add_key('event_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		//$this->dbforge->drop_table($this->_table, TRUE);
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}