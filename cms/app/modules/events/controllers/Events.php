<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Events Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

use PHPHtmlParser\Dom;

class Events extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('events_model');

		$this->load->language('events');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
		$this->acl->restrict('events.events.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('events'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');
	
		// render the page
		// $this->template->add_css(module_css('events', 'events_index'), 'embed');

		$this->template->add_js(module_js('events', 'events_index'), 'embed');
		$this->template->write_view('content', 'events_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('events.events.list');

		echo $this->events_model->get_datatables();
	}

	public function import()
	{
		$this->load->library('csv_reader');

		$data['page_heading'] = 'Import CSV';

		if ( $this->input->post() )
		{
			$this->form_validation->set_rules('event_csv', lang('event_csv'), 'required');

			$this->form_validation->set_message('required', 'This field is required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors']  = array(
					'event_csv' => form_error('event_csv')
				);
			}
			else
			{
				$csv	= $this->input->post('event_csv');
				$data 	= $this->csv_reader->parse_file($csv);

	
				if ( $data )
				{
					set_time_limit(0);

					// Start Transaction
					$this->db->trans_start();
					
					foreach ( $data as $key => $val )
					{
						if ( ! empty($val['URL'])) 
						{
							$url = $val['URL'];
							
							// process url
							$event_data = $this->__fetch_events($url);

							$this->events_model->insert(array(
								'event_title' => $event_data['title']
							));

							$this->db->trans_complete();

							if($this->db->trans_status() === FALSE)
							{
								log_message('error', 'CSV importing failed.');
							}
						}
					
					}

					// delete the csv
					if ( file_exists($csv) )
					{
						unlink($csv);						
					}
							
					$response = array(
						'success'  			=> TRUE,
						'message'  			=> 'Successfully imported the events',
						'redirect' 			=> site_url('events'),
					);
				}
				else
				{
					$response['success'] = FALSE;
					$response['message'] = lang('validation_error');
					$response['errors']  = array();
				}
			}

			echo json_encode($response); exit;
		}


		$this->template->set_template('modal');

		$this->template->add_js('components/dropzone/dist/dropzone.js');
		$this->template->add_css('components/dropzone/dist/dropzone.css');

		$this->template->add_css(module_css('events', 'events_import'), 'embed');
		$this->template->add_js(module_js('events', 'events_import'), 'embed');

		$this->template->write_view('content', 'events_import', $data);
		$this->template->render();
	}
	
	public function upload_csv()
	{
		$this->load->module('files/files');
		$this->files->allowed_file_types = 'csv';

		$response = $this->files->upload();

		echo $response;
	}

	private function __fetch_events($url)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Accept: */*",
				"Cache-Control: no-cache",
				"Connection: keep-alive",
				"Host: www.facebook.com",
				"Postman-Token: f447c767-1fe6-491c-b723-036809ec7ef3,7978dde9-969a-4c72-bc14-dbdd2062ce4e",
				"User-Agent: PostmanRuntime/7.15.0",
				"accept-encoding: gzip, deflate",
				"cache-control: no-cache"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) 
		{
			echo "cURL Error #:" . $err;
		} 
		else 
		{
			// echo $response;

			$html = new DOMDocument();
			@$html->loadHTML($response);

			$xpath 	 = new DOMXPath($html);
			$content = $xpath->query('//script[@type="application/ld+json"]');

			if ( $content )
			{
				foreach ($content as $tag) {
					$nodes[] = $tag;
				}

				if ( $nodes )
				{
					$text_content = json_decode($nodes['0']->textContent);

					$event_data = array(
						'start_date'  => date('F d, Y h:i a', strtotime($text_content->startDate)),
						'end_date'    => date('F d, Y h:i a', strtotime($text_content->endDate)),
						'title' 	  => $text_content->name,
						'url'         => $text_content->url,
						'address'     => $text_content->location->name . ' ' . $text_content->location->address->streetAddress . ' ' . $text_content->location->address->addressLocality,
						'description' => $text_content->description,
						'photo'		  => $text_content->image
					);
				}
			}

			pr($text_content);
			pr($event_data);

			
			

			//$content  = $xpath->query('//div[@class="global-module-wrapper"]');
			// $dom = new Dom;
			// $dom->load($response);

			// $title = $dom->find('<script type="application/ld+json"></script>')['0'];

			// pr($title);
			// exit;



			// $event_data = array(
			// 	'title' => $title->text
			// );

			// return $event_data;
		}
	}

	public function test()
	{
		/**
		 * fields
		 * 
		 * ID, NAME, Description, Place, Start Time, Cover
		 */
		$pageID = '179106292455265';
		
		//$url = 'https://www.facebook.com/events/343770132951000/';
		$url = 'https://www.facebook.com/events/364402064217136/';
		$event_data = $this->__fetch_events($url);


		pr($event_data);
		exit;
	}

	// --------------------------------------------------------------------
	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('events.events.' . $action);
		
		$data['page_heading']   = lang($action . '_heading');
		$data['page_subhead']   = lang($action . '_subhead');
		$data['action'] = $action;
		$data['article_categories'] = $this->article_categories_model->get_categories_dropdown('article_category_id', 'article_category_name', TRUE);

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('events'));
		$this->breadcrumbs->push(lang($action . '_heading'), site_url('events/form/' . $action));

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(
					array(
						'success'  => true, 
						'message'  => lang($action . '_success'),
						'redirect' => site_url('events/events') 
					)
				); 
				exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'article_category_id'	=> form_error('article_category_id'),
					'article_title'			=> form_error('article_title'),
				 	'article_slug'          => form_error('article_slug'),
					'article_excerpt'		=> form_error('article_excerpt'),
					'article_content'		=> form_error('article_content'),
					'article_status'		=> form_error('article_status'),
					'article_photo'			=> form_error('article_photo')
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->events_model->find($id);

		// render the page
		//$this->template->set_template('modal');

		$this->template->add_js('components/dropzone/dist/dropzone.js');
		$this->template->add_css('components/dropzone/dist/dropzone.css');

		$this->template->add_js('components/select2/dist/js/select2.min.js');
		$this->template->add_css('components/select2/dist/css/select2.min.css');
		
		$this->template->add_js('components/ckeditor/ckeditor.js');
		
		$this->template->add_css(module_css('events', 'events_form'), 'embed');
		$this->template->add_js(module_js('events', 'events_form'), 'embed');

		$this->template->write_view('content', 'events_form', $data);
		$this->template->render();
	}


	// --------------------------------------------------------------------
	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function delete($id)
	{
		$this->acl->restrict('events.events.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->events_model->delete($id);

			$this->cache->delete('events');
			$this->cache->delete('events_latest');

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------
	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		$this->form_validation->set_rules('article_category_id', lang('article_category_id'), 'required');
		$this->form_validation->set_rules('article_title', lang('article_title'), 'required');
		$this->form_validation->set_rules('article_excerpt', lang('article_excerpt'), 'required');
		$this->form_validation->set_rules('article_content', lang('article_content'), 'required');
		$this->form_validation->set_rules('article_status', lang('article_status'), 'required');
		// $this->form_validation->set_rules('article_photo', lang('article_photo'), 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ( $this->form_validation->run($this) == FALSE )
		{
			return FALSE;
		}
		$data = array(
			'article_category_id'	=> $this->input->post('article_category_id'),
			'article_title'			=> $this->input->post('article_title'),
			'article_slug' 			=> url_title($this->input->post('article_title'), '-', true),
			'article_excerpt'	    => $this->input->post('article_excerpt'),
			'article_content'		=> $this->input->post('article_content'),
			'article_status'		=> $this->input->post('article_status'),
			'article_photo'			=> $this->input->post('article_photo'),
		);

		if ($action == 'add')
		{
			$insert_id = $this->events_model->insert($data);
			// $return = (is_numeric($insert_id)) ? $insert_id : FALSE;

			// Add metatags
			$meta_data = array(
				'metatag_title'				  => $this->input->post('article_title'),
				'metatag_description'   	  => $this->input->post('article_excerpt'),
				'metatag_og_title'			  => $this->input->post('article_title'),
				'metatag_og_image'			  => '',
				'metatag_og_description' 	  => $this->input->post('article_excerpt'),
				'metatag_twitter_title'	 	  => $this->input->post('article_title'),
				'metatag_twitter_image'		  => '',
				'metatag_twitter_description' => $this->input->post('article_excerpt')
			);

			$meta_id = $this->metatags_model->insert($meta_data);
			
			// Update events
			$return = $this->events_model->update($insert_id, array(
				'article_metatag_id' => $meta_id
			));

			$id = $insert_id;
		}
		else if ($action == 'edit')
		{
			$this->cache->delete('article_'.$data['article_slug']);

			$this->events_model->update($id, $data);

			$record = $this->events_model->find($id);

			// update metatag
			$meta_data = array(
				'metatag_title'				  => $this->input->post('article_title'),
				'metatag_description'   	  => $this->input->post('article_excerpt'),
				'metatag_og_title'			  => $this->input->post('article_title'),
				'metatag_og_image'			  => '',
				'metatag_og_description' 	  => $this->input->post('article_excerpt'),
				'metatag_twitter_title'	 	  => $this->input->post('article_title'),
				'metatag_twitter_image'		  => '',
				'metatag_twitter_description' => $this->input->post('article_excerpt')
			);

			$return = $this->metatags_model->update($record->article_metatag_id, $meta_data);
		}

		// Delete Cache
		$this->cache->delete('events');
		
		return $return;
	}
}

/* End of file Articles.php */
/* Location: ./application/modules/events/controllers/Articles.php */