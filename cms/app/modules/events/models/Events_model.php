<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Events_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Events_model extends BF_Model {

	protected $table_name			= 'events';
	protected $key					= 'event_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'event_created_on';
	protected $created_by_field		= 'event_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'event_modified_on';
	protected $modified_by_field	= 'event_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'event_deleted';
	protected $deleted_by_field		= 'event_deleted_by';


	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'event_id',
			'event_title',
			'event_status'
		);

		return $this->datatables($fields);
	}
}