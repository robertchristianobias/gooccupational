<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h5 class="modal-title" id="modalLabel"><?php echo $page_heading?></h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="">
						<div class="panel-wrapper collapse in">
							<div class="panel-body pa-0">
								<div class="col-sm-12 col-xs-12">
									<div class="form-wrap">
										<?php echo form_open(current_url(), array('class'=>'form', 'id'=>'events_import'));?>
											<div class="form-body overflow-hide">
												<div class="form-group">
													<input type="hidden" name="event_csv" id="event_csv" value="" />
													<label class="control-label bottom-margin" for="event_csv"><?php echo lang('event_csv')?>:</label>
													<?php if( ! empty($record->event_csv)) { ?>
														<div class="custom-dropzone" id="photo-wrapper">
															<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
																<img src="<?php echo display_image($record->event_csv); ?>" class="img-responsive" />
															</a>
														</div>
													<?php } else { ?>
														<div class="dropzone" id="event_csv_dropzone"></div>
													<?php } ?>
													<div class="help-text">Accepts csv.</div>
													<div id="error-event_csv"></div>
												</div>
											
											</div>
														
										<?php echo form_close(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">
				<i class="fa fa-times"></i> <?php echo lang('button_close')?>
			</button>
			<button id="submit_csv" class="btn btn-success btn-anim" type="submit" data-loading-text="<?php echo lang('processing')?>">
				<i class="fa fa-save"></i> 
				<span class="btn-text"><?php echo lang('button_import'); ?></span>
			</button>
		</div>
	</div>
</div>