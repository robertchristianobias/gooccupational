<?php echo form_open(current_url(), 'class="" id="articles_form" data-form-action="'.$action.'"'); ?>

	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#info"><span class="fa fa-question"></span> Info</a></li>
		<!-- <?php if ( isset( $record->article_id ) ) { ?>
			<li><a href="<?php echo site_url('metatags/form/articles/articles/' . $record->article_id); ?>" data-toggle="modal" data-target="#modal" class="btn btn-info"><span class="fa fa-cog"></span> Meta Tags</a></li>
		<?php } ?> -->
	</ul>

	<div class="tab-content">
		<div id="info" class="tab-pane fade in active">
			<p>&nbsp;</p>
			<div class="row">
				<div class="col-md-9">
					<div class="form-group">
						<label class="control-label" for="article_title"><?php echo lang('article_title')?>:</label>
						<?php echo form_input(array('name'=>'article_title', 'value'=>set_value('article_title', isset($record->article_title) ? $record->article_title : '', FALSE), 'class'=>'article_title form-control'));?>
						<div id="error-article_title"></div>
					</div>
					<div class="form-group">
						<label class="control-label" for="article_excerpt"><?php echo lang('article_excerpt')?>:</label>
						<?php echo form_textarea(array('name'=>'article_excerpt', 'rows'=>'3', 'value'=>set_value('article_excerpt', isset($record->article_excerpt) ? $record->article_excerpt : '', FALSE), 'class'=>'article_excerpt form-control')); ?>
						<div id="error-article_excerpt"></div>
					</div>
					<div class="form-group">
						<label class="control-label" for="article_content"><?php echo lang('article_content')?>:</label>
						<?php echo form_textarea(array('name'=>'article_content', 'rows'=>'3', 'value'=>set_value('article_content', isset($record->article_content) ? $record->article_content : '', FALSE), 'class'=>'ckeditor form-control', 'id'=>'article_content')); ?>
						<div id="error-article_content"></div>
					</div>
				</div>
				<div class="col-md-3">
				
					<div class="form-group">
						<label class="control-label" for="article_category_id"><?php echo lang('article_category_id')?>:</label>
						<?php echo form_dropdown('article_category_id', $article_categories, isset($record->article_category_id) ? $record->article_category_id : '', 'class="form-control select2" id="article_category_id"'); ?>
						<div id="error-article_category_id"></div>
					</div>
					<div class="form-group">
						<input type="hidden" name="article_photo" id="images_photo" value="<?php echo isset($record->article_photo) ? $record->article_photo : ''; ?>" />
						<label class="control-label bottom-margin" for="article_photo"><?php echo lang('article_photo')?>:</label>
						<?php if( ! empty($record->article_photo)) { ?>
							<div class="custom-dropzone" id="photo-wrapper">
								<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
									<img src="<?php echo display_image($record->article_photo); ?>" class="img-responsive" />

								</a>
							</div>
						<?php } else { ?>
							<div class="custom-dropzone" id="photo-wrapper">
								<a href="<?php echo site_url('files/images/select'); ?>" data-toggle="modal" data-target="#modal" >
									<span>Click to upload</span>
								</a>
							</div>
						<?php } ?>
						<div class="help-text">Accepts jpg, png with 2mb filesize.</div>
						<div id="error-article_photo"></div>
					</div>
					<div class="top-margin5">
						<?php if ($action == 'add'): ?>
							<button class="btn btn-success btn-sm submit" data-status="Published" type="submit" data-loading-text="<?php echo lang('processing')?>">
								<i class="fa fa-plus"></i> <?php echo lang('button_publish')?>
							</button>
						<?php elseif ($action == 'edit'): ?>
							<button class="btn btn-success btn-sm submit" data-status="Published" type="submit" data-loading-text="<?php echo lang('processing')?>">
								<i class="fa fa-save"></i> <?php echo lang('button_publish')?>
							</button>
						<?php endif; ?>
						<button class="btn btn-primary btn-sm submit" data-status="Draft" type="submit" data-loading-text="<?php echo lang('processing')?>">
							<?php echo lang('button_draft')?>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>