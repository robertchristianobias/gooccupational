/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

$(function() {
	
	//handle_single_image_dropzone('event_csv', site_url + 'files/images/upload');
	handle_single_file_dropzone('event_csv', site_url + 'events/upload_csv');

	// handles the submit action
	$('#submit_csv').click(function(e) {

		// change the button to loading state
		var btn  = $(this);
		var status = btn.data('status');

		var form = $('#events_import').serializeArray();
			
			
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post($('#events_import').attr('action'), form,
		function(data, status) {
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// reset the button
				btn.button('reset');
				
				// shows the error message
				swal("Opps!", o.message, "error");

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				
				swal("Success", o.message, "success");
				window.location.replace(o.redirect);
			}
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});


});