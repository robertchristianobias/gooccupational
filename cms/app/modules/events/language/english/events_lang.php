<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Events Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']			= 'Events'; 
$lang['crumb_settings']         = 'Settings';

// Labels
$lang['event_id']			    = 'Id';
$lang['event_csv'] = 'CSV';
$lang['event_category_id']	= 'Category';
$lang['event_type']			= 'Type';
$lang['event_title']			= 'Title';
$lang['event_slug']           = 'Slug';
$lang['event_excerpt']        = 'Excerpt';
$lang['event_content']		= 'Content';
$lang['event_photo']			= 'Featured Image';
$lang['event_link']           = 'Link';
$lang['event_pdf']            = 'PDF';
$lang['event_status'] = 'Status';
$lang['event_filter'] = 'Filtered by';

// Buttons
$lang['button_import']          = 'Import CSV';
$lang['button_add']				= 'Add Event';
$lang['button_update']			= 'Update Event';
$lang['button_draft']			= 'Save as draft';
$lang['button_publish']         = 'Publish';
$lang['button_delete']			= 'Delete Event';
$lang['button_edit_this']		= 'Edit This';

// Index Function
$lang['index_heading']			= 'Events';
$lang['index_subhead']			= 'List of all Events';
$lang['index_id']			    = 'Id';
$lang['index_category_id']		= 'Category Id';
$lang['index_type']			    = 'Type';
$lang['index_title']			= 'Title';
$lang['index_content']			= 'Content';
$lang['index_photo']			= 'Photo';
$lang['index_status']           = 'Status';

$lang['index_created_on']		= 'Created On';
$lang['index_created_by']		= 'Created By';
$lang['index_modified_on']		= 'Modified On';
$lang['index_modified_by']		= 'Modified By';
$lang['index_status']			= 'Status';
$lang['index_action']			= 'Action';

// View Function
$lang['view_heading']			= 'View Event';

// Add Function
$lang['add_heading']			= 'Add Event';
$lang['add_success']			= 'Event has been successfully added';

// Edit Function
$lang['edit_heading']			= 'Edit Event';
$lang['edit_success']			= 'Event has been successfully updated';

// Delete Function
$lang['delete_heading']			= 'Delete Event';
$lang['delete_confirm']			= 'Are you sure you want to delete this article?';
$lang['delete_success']			= 'Event has been successfully deleted';

// Settings
$lang['settings_heading']      = 'Settings';
$lang['settings_subhead']      = 'Events Settings';