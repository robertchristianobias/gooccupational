<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Settings Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Settings extends CI_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index() 
	{
		// this class was created just for the navigation menu
	}
}

/* End of file settings.php */
/* Location: ./application/modules/settings/controllers/settings.php */