<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Appliation Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		robertchristianobias@gmail.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Settings';

// Index Function
$lang['index_heading']				= 'Application Settings';
$lang['index_subhead']				= 'This page shall contain various application settings';
