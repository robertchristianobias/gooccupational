<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Schedules Class
 *
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Documentation extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('schedules_model');

		$this->load->language('schedules');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('schedules.schedules.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('schedules'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');

		$this->template->add_js(module_js('schedules', 'schedules_index'), 'embed');
		$this->template->write_view('content', 'schedules_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('schedules.schedules.list');

		echo $this->schedules_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('schedules.schedules.' . $action);

		$data['page_heading']  = lang($action . '_heading');
		$data['page_subhead']  = lang($action . '_subhead');
		$data['action'] 	   = $action;

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('schedules'));
		$this->breadcrumbs->push(lang($action . '_heading'), site_url('schedules/form/' . $action));

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(array(
					'success'  => true, 
					'message'  => lang($action . '_success'),
					'redirect' => site_url('schedules')
				)); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'schedule_title'		=> form_error('schedule_title'),
					'schedule_description'	=> form_error('schedule_description'),			
					'schedule_date'		=> form_error('schedule_date'),
				);

				echo json_encode($response);
				exit;
			}
		}

		if ( $action != 'add' ) 
		{
			$data['record'] 	  = $this->schedules_model->find($id);			
		}

		// render the page
		// $this->template->set_template('modal');

		$this->template->add_js('components/bootstrap-daterangepicker/moment.min.js');	
		$this->template->add_js('components/bootstrap-daterangepicker/daterangepicker.js');
		$this->template->add_js('components/clockpicker/js/jquery-clockpicker.min.js');
		$this->template->add_js('components/clockpicker/js/bootstrap-clockpicker.min.js');

		$this->template->add_js('components/tinymce/tinymce.min.js');
		
		$this->template->add_js(module_js('schedules', 'schedules_form'), 'embed');
		$this->template->write_view('content', 'schedules_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('schedules.schedules.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->schedules_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('schedule_title', lang('schedule_title'), 'required');
		$this->form_validation->set_rules('schedule_description', lang('schedule_description'), 'required');
		$this->form_validation->set_rules('schedule_date', lang('schedule_date'), 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}
		
		list($date_from, $date_to) = explode(' - ', $this->input->post('schedule_date'));

		$schedule_date_from = date('Y-m-d', strtotime($date_from));
		$schedule_date_to   = date('Y-m-d', strtotime($date_to));
		
		$data = array(
			'schedule_title'		  => $this->input->post('schedule_title'),
			'schedule_description'	  => $this->input->post('schedule_description'),
			'schedule_date_from'	  => $schedule_date_from,
			'schedule_date_to'		  => $schedule_date_to,
			'schedule_time_from'	  => $this->input->post('schedule_time_from') ? date('H:i a', strtotime($this->input->post('schedule_time_from'))) : '00:00:00',
			'schedule_time_to'		  => $this->input->post('schedule_time_to')   ? date('H:i a', strtotime($this->input->post('schedule_time_to'))) 	 : '00:00:00',
		);

		if ($action == 'add')
		{
			$id = $this->schedules_model->insert($data);
			$return = (is_numeric($id)) ? $id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->schedules_model->update($id, $data);
		}

		return $return;
	}

	/**
	 * schedules Settings
	 *
	 * @param none
	 * @author Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function settings()
	{
		$this->acl->restrict('schedules.schedules.list');

		$this->load->model('settings/configs_model');

		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('schedules'));
		$this->breadcrumbs->push(lang('crumb_settings'), site_url('schedules/settings'));

		// page title
		$data['page_heading'] = lang('settings_heading');
		$data['page_subhead'] = lang('settings_subhead');

		$sections  = array('schedules_section_header');

		$data['sections'] = $this->configs_model->get_configs($sections, 'schedules_section');
		
		if($this->input->post())
		{
			$this->form_validation->set_rules('schedules_section_header', lang('schedules_section_header'), 'required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors']  = array(					
					'schedules_section_header'	=> form_error('schedules_section_header'),
				);
			}
			else
			{
				foreach($sections as $config)
				{
					$post_val = $this->input->post($config);
					
					$this->configs_model->update_where('config_name', $config, array(
						'config_value' => $post_val
					));
				}

				$response['success']  = TRUE;
				$response['message']  = lang('success_setting_update');
				$response['redirect'] = site_url('schedules/settings');

				// Delete cache
				$this->cache->delete('app_configs');
				$this->cache->delete('config_settings_schedules_section');
			}

			echo json_encode($response); exit;
		}

		$this->template->add_js(module_js('schedules', 'schedule_settings'));

		$this->template->write_view('content', 'schedule_settings', $data);
		$this->template->render();
	}


}

/* End of file schedules.php */
/* Location: ./application/modules/schedules/controllers/schedules.php */