<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Documents Class
 *
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Documents extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('documents_model');
		$this->load->model('templates_model');

		$this->load->language('documents');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('documentations.documents.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('documentations/documents'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');

		$this->template->add_js(module_js('documentations', 'document_index'), 'embed');
		$this->template->write_view('content', 'document_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('documentations.documents.list');

		echo $this->documents_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('documentations.documents.' . $action);

		$data['page_heading']  = lang($action . '_heading');
		$data['page_subhead']  = lang($action . '_subhead');
		$data['action'] 	   = $action;
		$data['templates_dropdown'] = $this->templates_model->get_template_dropdown();

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('documentations'));
		$this->breadcrumbs->push(lang($action . '_heading'), site_url('documentations/form/' . $action));

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(array(
					'success'  => true, 
					'message'  => lang($action . '_success'),
					'redirect' => site_url('documentations/documents')
				)); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'document_template_id' => form_error('document_template_id'),
					'document_name'		   => form_error('document_name'),
					'document_content'	   => form_error('document_content'),
					'document_status'	   => form_error('document_status')	
				);

				echo json_encode($response);
				exit;
			}
		}

		if ( $action != 'add' ) 
		{
			$data['record'] 	  = $this->documents_model->find($id);			
		}

		// render the page
		// $this->template->set_template('modal');

		$this->template->add_js('components/ckeditor/ckeditor.js');
		
		$this->template->add_js(module_js('documentations', 'document_form'), 'embed');
		$this->template->write_view('content', 'document_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function delete($id)
	{
		$this->acl->restrict('documentations.documents.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->documents_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('document_template_id', lang('document_template_id'), 'required');
		$this->form_validation->set_rules('document_name', lang('document_name'), 'required');
		$this->form_validation->set_rules('document_content', lang('document_content'), 'required');
		$this->form_validation->set_rules('document_status', lang('document_status'), 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}
		
		$data = array(
			'document_template_id' => $this->input->post('document_template_id'),
			'document_name'		   => $this->input->post('document_name'),
			'document_content'	   => $this->input->post('document_content'),
			'document_status'	   => $this->input->post('document_status')
		);

		if ($action == 'add')
		{
			$id = $this->documents_model->insert($data);
			$return = (is_numeric($id)) ? $id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->documents_model->update($id, $data);
		}

		return $return;
	}

	/**
	 * documentations Settings
	 *
	 * @param none
	 * @author Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function settings()
	{
		$this->acl->restrict('documentations.documents.list');

		$this->load->model('settings/configs_model');

		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('documentations'));
		$this->breadcrumbs->push(lang('crumb_settings'), site_url('documentations/settings'));

		// page title
		$data['page_heading'] = lang('settings_heading');
		$data['page_subhead'] = lang('settings_subhead');

		$sections  = array('documentations_section_header');

		$data['sections'] = $this->configs_model->get_configs($sections, 'documentations_section');
		
		if($this->input->post())
		{
			$this->form_validation->set_rules('documentations_section_header', lang('documentations_section_header'), 'required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors']  = array(					
					'documentations_section_header'	=> form_error('documentations_section_header'),
				);
			}
			else
			{
				foreach($sections as $config)
				{
					$post_val = $this->input->post($config);
					
					$this->configs_model->update_where('config_name', $config, array(
						'config_value' => $post_val
					));
				}

				$response['success']  = TRUE;
				$response['message']  = lang('success_setting_update');
				$response['redirect'] = site_url('documentations/settings');

				// Delete cache
				$this->cache->delete('app_configs');
				$this->cache->delete('config_settings_documentations_section');
			}

			echo json_encode($response); exit;
		}

		$this->template->add_js(module_js('documentations', 'documentation_document_settings'));

		$this->template->write_view('content', 'documentation_document_settings', $data);
		$this->template->render();
	}

	public function process_template()
	{
		$template_id = (int) $this->input->post('template_id');
		$template = $this->templates_model->get_template($template_id);

		echo $template->documentation_template_content;
	}

}

/* End of file documentations.php */
/* Location: ./application/modules/documentations/controllers/documentations.php */