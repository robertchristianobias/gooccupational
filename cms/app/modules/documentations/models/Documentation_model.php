<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Documentation_model Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Documentation_model extends BF_Model {

	protected $table_name			= 'documentation';
	protected $key					= 'documentation_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'documentation_created_on';
	protected $created_by_field		= 'documentation_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'documentation_modified_on';
	protected $modified_by_field	= 'documentation_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'documentation_deleted';
	protected $deleted_by_field		= 'documentation_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'documentation_id',
			'documentation_title',

			'documentation_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'documentation_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = documentation_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = documentation_modified_by', 'LEFT')
					->datatables($fields, $callback);
	}


}