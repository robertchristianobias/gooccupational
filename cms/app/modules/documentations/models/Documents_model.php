<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Documents_model Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Documents_model extends BF_Model {

	protected $table_name			= 'documents';
	protected $key					= 'document_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'document_created_on';
	protected $created_by_field		= 'document_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'document_modified_on';
	protected $modified_by_field	= 'document_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'document_deleted';
	protected $deleted_by_field		= 'document_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'document_id',
			'document_name',
			'document_status'
		);

		return $this->datatables($fields);
	}
}