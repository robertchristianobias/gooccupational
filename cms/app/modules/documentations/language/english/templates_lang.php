<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Templates Language File (English)
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']			= 'Templates'; 

// Labels
$lang['documentation_template_id']			= 'ID';
$lang['documentation_template_title']	    = 'Title';
$lang['documentation_template_content']		= 'Content';

// Buttons
$lang['button_add']				= 'Add Template';
$lang['button_update']			= 'Update Template';
$lang['button_delete']			= 'Delete Template';
$lang['button_edit_this']		= 'Edit This';
$lang['button_publish']         = 'Publish';
$lang['button_draft']           = 'Save as Draft';

// Index Function
$lang['index_heading']			= 'Templates';
$lang['index_subhead']			= 'List of all documentation templates';
$lang['index_id']			    = 'Id';
$lang['index_title']			= 'Title';
$lang['index_status']   = 'Status';

$lang['index_action']			= 'Action';

// View Function
$lang['view_heading']			= 'View Template';

// Add Function
$lang['add_heading']	   = 'Add Template';
$lang['add_success']	   = 'Template has been successfully added';

// Edit Function
$lang['edit_heading']	   = 'Edit Template';
$lang['edit_success']	   = 'Template has been successfully updated';

// Delete Function
$lang['delete_heading']	   = 'Delete Template';
$lang['delete_confirm']	   = 'Are you sure you want to delete this Template?';
$lang['delete_success']	   = 'Template has been successfully deleted';

$lang['attendees_heading']     = 'Attendees';
$lang['attendees_subhead']     = 'List of all Template attendees';
$lang['crumb_attendees']       = 'Attendees';
