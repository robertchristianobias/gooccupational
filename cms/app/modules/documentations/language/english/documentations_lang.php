<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Documentations Language File (English)
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']			= 'Documentations'; 

// Labels
$lang['document_id']			= 'ID';
$lang['document_name']	    = 'Title';
$lang['document_content']		= 'Content';

// Buttons
$lang['button_add']				= 'Add Document';
$lang['button_update']			= 'Update Document';
$lang['button_delete']			= 'Delete Document';
$lang['button_edit_this']		= 'Edit This';

// Index Function
$lang['index_heading']			= 'Documents';
$lang['index_subhead']			= 'List of all document';
$lang['index_id']			    = 'Id';
$lang['index_title']			= 'Title';

$lang['index_action']			= 'Action';

// View Function
$lang['view_heading']			= 'View Document';

// Add Function
$lang['add_heading']	   = 'Add Document';
$lang['add_success']	   = 'Document has been successfully added';

// Edit Function
$lang['edit_heading']	   = 'Edit Document';
$lang['edit_success']	   = 'Document has been successfully updated';

// Delete Function
$lang['delete_heading']	   = 'Delete Document';
$lang['delete_confirm']	   = 'Are you sure you want to delete this Document?';
$lang['delete_success']	   = 'Document has been successfully deleted';
