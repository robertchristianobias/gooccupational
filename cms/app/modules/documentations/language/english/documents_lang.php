<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Case Files Language File (English)
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']			= 'Case Files'; 

// Labels
$lang['document_id']			= 'ID';
$lang['document_template_id']   = 'Template';
$lang['document_name']			= 'Title';
$lang['document_content']	    = 'Content';
$lang['document_status']        = 'Status';

// Buttons
$lang['button_add']				= 'Add case file';
$lang['button_update']			= 'Update case file';
$lang['button_delete']			= 'Delete case file';
$lang['button_edit_this']		= 'Edit This';
$lang['button_publish']         = 'Publish';
$lang['button_draft']           = 'Save as Draft';

// Index Function
$lang['index_heading']			= 'Case Files';
$lang['index_subhead']			= 'List of all documents';
$lang['index_id']			    = 'Id';
$lang['index_title']			= 'Title';
$lang['index_status']           = 'Status';

$lang['index_created_on']		= 'Created On';
$lang['index_created_by']		= 'Created By';
$lang['index_modified_on']		= 'Modified On';
$lang['index_modified_by']		= 'Modified By';
$lang['index_action']			= 'Action';

// View Function
$lang['view_heading']			= 'View case file';

// Add Function
$lang['add_heading']	   = 'Add case file';
$lang['add_success']	   = 'case file has been successfully added';

// Edit Function
$lang['edit_heading']	   = 'Edit case file';
$lang['edit_success']	   = 'case file has been successfully updated';

// Delete Function
$lang['delete_heading']	   = 'Delete case file';
$lang['delete_confirm']	   = 'Are you sure you want to delete this case file?';
$lang['delete_success']	   = 'case file has been successfully deleted';