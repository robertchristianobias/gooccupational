<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Migration_Create_documentations_template extends CI_Migration {

	private $_table = 'documentations_template';

	private $_permissions = array(
		array('Template Link', 'documentations.template.link'),
		array('Template List', 'documentations.template.list'),
		array('View Template', 'documentations.template.view'),
		array('Add Template', 'documentations.template.add'),
		array('Edit Template', 'documentations.template.edit'),
		array('Delete Template', 'documentations.template.delete'),
	);

	private $_menus = array(
	
		array(
			'menu_parent'		=> 'documentations', // none if parent or single menu
			'menu_text' 		=> 'Templates', 
			'menu_link' 		=> 'documentations/templates', 
			'menu_perm' 		=> 'documentations.template.list', 
			'menu_icon' 		=> 'fa fa-pencil', 
			'menu_order' 		=> 2, 
			'menu_active' 		=> 1
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'documentation_template_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'documentation_template_title'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'documentation_template_content'	=> array('type' => 'LONGTEXT', 'null' => FALSE),

			'documentation_template_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'documentation_template_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'documentation_template_modified_by' => array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'documentation_template_modified_on' => array('type' => 'DATETIME', 'null' => TRUE),
			'documentation_template_deleted' 	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'documentation_template_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('documentation_template_id', TRUE);
		$this->dbforge->add_key('documentation_template_title');

		$this->dbforge->add_key('documentation_template_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		//$this->dbforge->drop_table($this->_table, TRUE);
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}