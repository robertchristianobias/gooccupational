<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Migration_Create_documentations extends CI_Migration {

	private $_table = 'documentations';

	private $_permissions = array(
		array('Documentations Link', 'documentations.documentation.link'),
		array('Documentations List', 'documentations.documentation.list'),
		array('View Documentation', 'documentations.documentation.view'),
		array('Add Documentation', 'documentations.documentation.add'),
		array('Edit Documentation', 'documentations.documentation.edit'),
		array('Delete Documentation', 'documentations.documentation.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'none', // none if parent or single menu
			'menu_text' 		=> 'Documentations', 
			'menu_link' 		=> 'documentations', 
			'menu_perm' 		=> 'documentations.documentation.link', 
			'menu_icon' 		=> 'fa fa-folder-open', 
			'menu_order' 		=> 3, 
			'menu_active' 		=> 1
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'documentation_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'documentation_template_id'	=> array('type' => 'TEXT', 'null' => FALSE),
			'documentation_title'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'documentation_content'	=> array('type' => 'DATE', 'null' => FALSE),

			'documentation_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'documentation_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'documentation_modified_by' => array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'documentation_modified_on' => array('type' => 'DATETIME', 'null' => TRUE),
			'documentation_deleted' 	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'documentation_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('documentation_id', TRUE);
		$this->dbforge->add_key('documentation_title');

		$this->dbforge->add_key('documentation_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		//$this->dbforge->drop_table($this->_table, TRUE);
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}