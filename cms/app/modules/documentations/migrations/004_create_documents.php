<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Migration_Create_documents extends CI_Migration {

	private $_table = 'documents';

	private $_permissions = array(
		array('Documents Link', 'documentations.documents.link'),
		array('Documents List', 'documentations.documents.list'),
		array('View Document', 'documentations.documents.view'),
		array('Add Document', 'documentations.documents.add'),
		array('Edit Document', 'documentations.documents.edit'),
		array('Delete Document', 'documentations.documents.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'documentations', // none if parent or single menu
			'menu_text' 		=> 'Documents', 
			'menu_link' 		=> 'documentations/documents', 
			'menu_perm' 		=> 'documentations.documents.list', 
			'menu_icon' 		=> 'fa fa-filetext', 
			'menu_order' 		=> 1, 
			'menu_active' 		=> 1
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'document_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'document_template_id'	=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'document_name'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'document_content'		=> array('type' => 'DATE', 'null' => FALSE),

			'document_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'document_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'document_modified_by' => array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'document_modified_on' => array('type' => 'DATETIME', 'null' => TRUE),
			'document_deleted' 	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'document_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('document_id', TRUE);
		$this->dbforge->add_key('document_template_id');
		$this->dbforge->add_key('document_name');

		$this->dbforge->add_key('document_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		//$this->dbforge->drop_table($this->_table, TRUE);
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}