<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Migration_Edit_documents_001 extends CI_Migration {

	private $_table = 'documents';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'document_template_id'	=> array('type' => 'INT', 'constraint' => '11', 'null' => FALSE),
			'document_content'		=> array('type' => 'DATE', 'null' => FALSE)
		);

		$this->dbforge->add_column($this->_table, $fields);
		
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'document_template_id');
		$this->dbforge->drop_column($this->_table, 'document_content');
	}
}