<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Migration_Create_shared_templates extends CI_Migration {

	private $_table = 'shared_templates';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'st_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'st_template_id'	=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'st_contact_id'		=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'st_shared_by_id'	=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),

			'st_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'st_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'st_modified_by' => array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'st_modified_on' => array('type' => 'DATETIME', 'null' => TRUE),
			'st_deleted' 	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'st_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('st_id', TRUE);
		$this->dbforge->add_key('st_template_id');
		$this->dbforge->add_key('st_contact_id');
		$this->dbforge->add_key('st_shared_by_id');

		$this->dbforge->add_key('st_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

	}

	public function down()
	{
		// drop the table
		//$this->dbforge->drop_table($this->_table, TRUE);
		$this->dbforge->drop_table($this->_table, TRUE);

	}
}