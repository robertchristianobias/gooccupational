<?php echo form_open(current_url(), 'id="form"'); ?>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label" for="documentation_template_title"><?php echo lang('documentation_template_title')?>:</label>
				<?php echo form_input(array('id'=>'documentation_template_title', 'name'=>'documentation_template_title', 'value'=>set_value('documentation_template_title', isset($record->documentation_template_title) ? $record->documentation_template_title : '', FALSE), 'class'=>'form-control'));?>
				<div id="error-documentation_template_title"></div>
			</div>
					
			<div class="form-group">
				<label class="control-label" for="documentation_template_content"><?php echo lang('documentation_template_content')?>:</label>
				<?php echo form_textarea(array('id'=>'documentation_template_content', 'name'=>'documentation_template_content', 'rows'=>'3', 'value'=>set_value('documentation_template_content', isset($record->documentation_template_content) ? $record->documentation_template_content : '', FALSE), 'class'=>'form-control ckeditor')); ?>
				<div id="error-documentation_template_content"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<?php if ($action == 'add'): ?>
					<button class="btn btn-success btn-sm submit" data-status="Published" type="submit" >
						<span><?php echo lang('button_publish')?></span>
					</button>
				<?php elseif ($action == 'edit'): ?>
					<button class="btn btn-success btn-sm submit" data-status="Published" type="submit">
						<span><?php echo lang('button_publish')?></span>
					</button>
				<?php endif; ?>
				<button class="btn btn-primary btn-sm submit" data-status="Draft" type="submit" >
					<span><?php echo lang('button_draft')?></span>
				</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>
