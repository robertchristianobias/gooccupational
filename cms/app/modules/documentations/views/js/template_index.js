

/**
 * @version     1.0
 * @author      Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright   Copyright (c) 2017, GoOccupational
 * @link        http://www.rchristianobias.com
 */
$(function() {
	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": site_url + "documentations/templates/datatables",
		"lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
		"pagingType": "full_numbers",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next', 
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "desc" ]],
		"aoColumnDefs": [
			{
				"aTargets": [0],
				"sClass": "col-md-1 text-center",
			},
			{
				"aTargets": [2],
				"sClass": "col-md-1 text-center",
				"mRender": function (data, type, full) {
					if ( full[2] == 'Draft' ) {
						html = '<span class="badge badge-secondary">'+full[2]+'</span>';
					} else {
						html = '<span class="badge badge-success">'+full[2]+'</span>';
					}

					return html;
				}
			},
			{
				"aTargets": [3],
				"bSortable": false,
				"mRender": function (data, type, full) {
					html  = '<a href="' + site_url + 'documentations/templates/form/edit/'+full[0]+'" title="Edit" class="btn btn-primary btn-icon-anim btn-circle"><i class="fa fa-pencil"></i></a> ';
					html += '<a href="' + site_url + 'documentations/templates/delete/'+full[0]+'" title="Delete" class="btn btn-danger btn-icon-anim btn-circle"><i class="fa fa-trash-o"></i></a>';

					return html;
				},
				"sClass": "col-md-2 text-center",
			},
		]
	});

	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {        
		// eg. destroys the wysiwyg editor
	});

});