/**
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */

$(function() {

	var $document_template_id = $('#document_template_id');
		$document_template_id.change(function() {
			var $template_id = $(this).val();
			$.post(site_url + 'documentations/documents/process_template', {
				template_id: $template_id
			}, function(data) {
				CKEDITOR.instances['document_content'].setData(data);
			});
		});

	// handles the submit action
	$('.submit').click(function(e){
		// change the button to loading state
		var btn = $(this);
		var status = btn.data('status');
		var frm = $('#form').serializeArray();
			frm.push({
				name:  'document_content',
				value:  CKEDITOR.instances['document_content'].getData()
			}, {
				name: 'document_status',
				value: status
			});
			
			ajax_url = $('#form').attr('action');
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(ajax_url, frm,
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// reset the button
				btn.button('reset');
				
				// shows the error message
				// alertify.error(o.message);
				swal("Opps!", o.message, "error");

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {

				// shows the success message
				swal("Good job!", o.message, "success");

				window.location.replace(o.redirect);
			}
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
});