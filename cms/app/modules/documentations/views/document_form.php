<?php echo form_open(current_url(), 'id="form"'); ?>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#info"><i class="fa fa-info-circle" aria-hidden="true"></i> Info</a></li>
	</ul>
	<div class="tab-content">
		<div id="info" class="tab-pane fade in active">
			<div class="row">
				<div class="col-md-9">
					<div class="form-group">
						<label class="control-label" for="document_name"><?php echo lang('document_name')?>:</label>
						<?php echo form_input(array('id'=>'document_name', 'name'=>'document_name', 'value'=>set_value('document_name', isset($record->document_name) ? $record->document_name : '', FALSE), 'class'=>'form-control'));?>
						<div id="error-document_name"></div>
					</div>
							
					<div class="form-group">
						<label class="control-label" for="document_content"><?php echo lang('document_content')?>:</label>
						<?php echo form_textarea(array('id'=>'document_content', 'name'=>'document_content', 'rows'=>'3', 'value'=>set_value('document_content', isset($record->document_content) ? $record->document_content : '', FALSE), 'class'=>'form-control ckeditor')); ?>
						<div id="error-document_content"></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label" for="document_template_id"><?php echo lang('document_template_id')?>:</label>
						<?php echo form_dropdown('document_template_id', $templates_dropdown, isset($record->document_template_id) ? $record->document_template_id : '', 'class="form-control select2" id="document_template_id"'); ?>
						<div id="error-document_template_id"></div>
					</div>
					<div class="top-margin5">
						<?php if ($action == 'add'): ?>
							<button class="btn btn-success btn-sm submit" data-status="Published" type="submit" >
								<span><?php echo lang('button_publish')?></span>
							</button>
						<?php elseif ($action == 'edit'): ?>
							<button class="btn btn-success btn-sm submit" data-status="Published" type="submit">
								<span><?php echo lang('button_publish')?></span>
							</button>
						<?php endif; ?>
						<button class="btn btn-primary btn-sm submit" data-status="Draft" type="submit" >
							<span><?php echo lang('button_draft')?></span>
						</button>
					</div>
				</div>
			</div>
		</div>
		
	</div>
<?php echo form_close(); ?>
