<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">
	<?php echo form_open(current_url(), 'class="form-horizontal" id="article_category_form"'); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="article_category_name"><?php echo lang('article_category_name')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'article_category_name', 'name'=>'article_category_name', 'value'=>set_value('article_category_name', isset($record->article_category_name) ? $record->article_category_name : ''), 'class'=>'form-control'));?>
				<div id="error-article_category_name"></div>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>