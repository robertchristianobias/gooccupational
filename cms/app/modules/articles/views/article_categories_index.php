<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark"><?php echo $page_heading; ?></h3>
				</div>
				<div class="pull-right">
					<?php if ($this->acl->restrict('articles.article_categories.add', 'return')): ?>
						<a href="<?php echo site_url('articles/article_categories/form/add')?>" data-toggle="modal" data-target="#modal" class="btn btn-sm btn-success btn-add" id="btn_add"><span class="fa fa-plus"></span> <?php echo lang('button_add')?></a>
					<?php endif; ?>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<table class="table table-striped table-bordered table-hover dt-responsive" id="datatables">
						<thead>
							<tr>
								<th class="all"><?php echo lang('index_id'); ?></th>
								<th class="min-desktop"><?php echo lang('index_name'); ?></th>

								<th class="min-tablet"><?php echo lang('index_action')?></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>