/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

$(function() {
	
	handle_single_image_dropzone('article_photo', site_url + 'files/images/upload');
	editor('#article_content', 'simple');

	$('.select2').select2();

	// $('.close-x').click(function() {
	// 	$(this).closest('#dropzone-holder').find('.dropzone').show();
	// 	$(this).closest('#dropzone-holder').find('#dropzone-image').hide();
	// });
	
	// handles the submit action
	$('.submit').click(function(e) {
		// change the button to loading state
		var btn  = $(this);
		var status = btn.data('status');

		var form = $('#articles_form').serializeArray();
			form.push({
				name:  'article_content',
				value:  CKEDITOR.instances['article_content'].getData()
			}, {
				name: 'article_status',
				value: status
			});

		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post($('#articles_form').attr('action'), form,
		function(data, status) {
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// reset the button
				btn.button('reset');
				
				// shows the error message
				swal("Opps!", o.message, "error");

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				
				swal("Success", o.message, "success");
				window.location.replace(o.redirect);
			}
		});
	});

	$('.article_title').blur(function() {
		var title = $(this).val();

		$('.article_slug').val(slug(title));
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});

	var action = $('#articles_form').data('form-action');
	if(action == 'view') {
		$("#articles_form :input").attr("disabled", true);	
	}

});