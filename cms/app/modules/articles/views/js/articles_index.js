/**
 * @package     RainCode
 * @version     1.0
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2017, RC MediaPH
 * @link        http://www.rcmediaph.com
 */
$(function() {

	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "articles/datatables",
		"lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
		"pagingType": "full_numbers",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next', 
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "desc" ]],
		"aoColumnDefs": [
			{
				"aTargets": [0],
				"sClass": "col-md-1 text-center",
			},
			{
				"aTargets": [1],
				"sClass": "col-md-8"
			},
			{
				"aTargets": [2],
				"sClass": "col-md-2",
				"mRender": function (data, type, full) {
					if ( full[2] == 'Draft' ) {
						html = '<span class="badge badge-secondary">'+full[2]+'</span>';
					} else {
						html = '<span class="badge badge-success">'+full[2]+'</span>';
					}

					return html;
				}
			},
			{
				"aTargets": [3],
				"bSortable": false,
				"mRender": function (data, type, full) {
					html  = '<a href="articles/form/edit/'+full[0]+'" tooltip-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-icon-anim btn-circle"><i class="fa fa-pencil"></i></a> ';
					html += '<a href="articles/delete/'+full[0]+'" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-icon-anim btn-circle"><i class="fa fa-trash-o"></i></a>';

					return html;
				},
				"sClass": "col-md-2 text-center",
			},
		]
	});

	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {        
		// eg. destroys the wysiwyg editor
	});
});