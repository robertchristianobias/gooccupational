/**
 * @package     RainCode
 * @version     1.0
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2017, RC MediaPH
 * @link        http://www.rcmediaph.com
 */
$(function() {
	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": site_url + "articles/article_categories/datatables",
		"lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
		"pagingType": "full_numbers",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next', 
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "asc" ]],
		"aoColumnDefs": [
			
			{
				"aTargets": [0],
				"sClass": "col-md-1 text-center",
			},
			{
				"aTargets": [2],
				"bSortable": false,
				"mRender": function (data, type, full) {
					html  = '<a href="' + site_url + 'articles/article_categories/form/edit/'+full[0]+'" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-icon-anim btn-circle"><i class="fa fa-pencil"></i></a> ';
					html += '<a href="' + site_url + 'articles/article_categories/delete/'+full[0]+'" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-icon-anim btn-circle"><i class="fa fa-trash-o"></i></a>';

					return html;
				},
				"sClass": "col-md-2 text-center",
			},
		]
	});

	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {        
		// eg. destroys the wysiwyg editor
	});

});