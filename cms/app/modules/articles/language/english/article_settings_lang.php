<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Article Settings Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

$lang['article_tags']           = 'Enable Article Tags';
$lang['article_per_page']       = 'Article Per Page';
$lang['article_social_icons']   = 'Social Icons';
$lang['facebook_page_id']       = 'Page ID';

$lang['facebook_app_id'] = 'APP ID';
$lang['facebook_app_secret'] = 'APP Secret';
$lang['facebook_api_version'] = 'API Version';

$lang['articles_section_header'] = 'Header Image';

$lang['success_setting_update'] = 'Setting successfully updated';