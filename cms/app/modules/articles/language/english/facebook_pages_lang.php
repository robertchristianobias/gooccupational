<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Facebook Pages Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2018, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Facebook Pages'; 

// Labels
$lang['facebook_page_id']			= 'Id';
$lang['facebook_page_api_version']			= 'Api Version';
$lang['facebook_page_app_id']			= 'App Id';
$lang['facebook_page_app_secret']			= 'App Secret';
$lang['facebook_page_page_id']			= 'Page ID';

// Buttons
$lang['button_add']					= 'Add Facebook Page';
$lang['button_update']				= 'Update Facebook Page';
$lang['button_delete']				= 'Delete Facebook Page';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Facebook Pages';
$lang['index_subhead']				= 'List of all Facebook Pages';
$lang['index_id']			= 'Id';
$lang['index_api_version']			= 'Api Version';
$lang['index_app_id']			= 'App Id';
$lang['index_app_secret']			= 'App Secret';
$lang['index_page_id']			= 'Page ID';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Facebook Page';

// Add Function
$lang['add_heading']				= 'Add Facebook Page';
$lang['add_success']				= 'Facebook Page has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Facebook Page';
$lang['edit_success']				= 'Facebook Page has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Facebook Page';
$lang['delete_confirm']				= 'Are you sure you want to delete this facebook_page?';
$lang['delete_success']				= 'Facebook Page has been successfully deleted';