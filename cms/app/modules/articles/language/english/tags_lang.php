<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Tags Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Tags'; 

// Labels
$lang['tag_id']			= 'Id';
$lang['tag_name']			= 'Name';

// Buttons
$lang['button_add']					= 'Add Tag';
$lang['button_update']				= 'Update Tag';
$lang['button_delete']				= 'Delete Tag';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Tags';
$lang['index_subhead']				= 'List of all Tags';
$lang['index_id']			= 'Id';
$lang['index_name']			= 'Name';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']					= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Tag';

// Add Function
$lang['add_heading']				= 'Add Tag';
$lang['add_success']				= 'Tag has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Tag';
$lang['edit_success']				= 'Tag has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Tag';
$lang['delete_confirm']				= 'Are you sure you want to delete this tag?';
$lang['delete_success']				= 'Tag has been successfully deleted';