<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Articles Language File (English)
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

// Breadcrumbs
$lang['crumb_module']			= 'Articles'; 
$lang['crumb_settings']         = 'Settings';

// Labels
$lang['article_id']			    = 'Id';
$lang['article_category_id']	= 'Category';
$lang['article_type']			= 'Type';
$lang['article_title']			= 'Title';
$lang['article_slug']           = 'Slug';
$lang['article_excerpt']        = 'Excerpt';
$lang['article_content']		= 'Content';
$lang['article_photo']			= 'Featured Image';
$lang['article_link']           = 'Link';
$lang['article_pdf']            = 'PDF';
$lang['article_status'] = 'Status';
$lang['article_filter'] = 'Filtered by';

// Buttons
$lang['button_add']				= 'Add Article';
$lang['button_update']			= 'Update Article';
$lang['button_draft']			= 'Save as draft';
$lang['button_publish']         = 'Publish';
$lang['button_delete']			= 'Delete Article';
$lang['button_edit_this']		= 'Edit This';

// Index Function
$lang['index_heading']			= 'Articles';
$lang['index_subhead']			= 'List of all Articles';
$lang['index_id']			    = 'Id';
$lang['index_category_id']		= 'Category Id';
$lang['index_type']			    = 'Type';
$lang['index_title']			= 'Title';
$lang['index_content']			= 'Content';
$lang['index_photo']			= 'Photo';
$lang['index_status']           = 'Status';

$lang['index_created_on']		= 'Created On';
$lang['index_created_by']		= 'Created By';
$lang['index_modified_on']		= 'Modified On';
$lang['index_modified_by']		= 'Modified By';
$lang['index_status']			= 'Status';
$lang['index_action']			= 'Action';

// View Function
$lang['view_heading']			= 'View Article';

// Add Function
$lang['add_heading']			= 'Add Article';
$lang['add_success']			= 'Article has been successfully added';

// Edit Function
$lang['edit_heading']			= 'Edit Article';
$lang['edit_success']			= 'Article has been successfully updated';

// Delete Function
$lang['delete_heading']			= 'Delete Article';
$lang['delete_confirm']			= 'Are you sure you want to delete this article?';
$lang['delete_success']			= 'Article has been successfully deleted';

// Settings
$lang['settings_heading']      = 'Settings';
$lang['settings_subhead']      = 'Articles Settings';