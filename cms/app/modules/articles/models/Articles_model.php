<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Articles_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Articles_model extends BF_Model {

	protected $table_name			= 'articles';
	protected $key					= 'article_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'article_created_on';
	protected $created_by_field		= 'article_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'article_modified_on';
	protected $modified_by_field	= 'article_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'article_deleted';
	protected $deleted_by_field		= 'article_deleted_by';

	public $metatag_key				= 'article_metatag_id';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'article_id',
			'article_title',
			'article_status',
			
			'article_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'article_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = article_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = article_modified_by', 'LEFT')
					->datatables($fields);
	}

	/**
	 * get_articles
	 *
	 * @access public
	 * @param  none
	 * @author Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_articles()
	{
		if( ! $articles = $this->cache->get('articles'))
		{
			$articles = $this->where('article_deleted', 0)
						 	 ->order_by('article_created_on', 'DESC')
						 	 ->find_all();
		
			$this->cache->save('articles', $articles, 300);
		}

		return $articles;
	}

	/**
	 * get_article_by_slug
	 *
	 * @param [string] $slug
	 * 
	 * @author Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_article_by_slug($slug)
	{
		if( !$article = $this->cache->get('article_'.$slug))
		{
			$article = $this->where('article_deleted', 0)
							->find_by('article_slug', $slug);

			$this->cache->save('article_'.$slug, $article, 300);
		}

		return $article;
	}

	/**
	 * get_latest_articles
	 *
	 * @param [int] $limit
	 * @author Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_latest_articles($limit)
	{
		if( ! $articles_latest = $this->cache->get('articles_latest'))
		{
			$articles_latest = $this->where('article_deleted', 0)
									->order_by('article_created_on', 'DESC')
									->limit($limit)
									->find_all();
		
			$this->cache->save('articles_latest', $articles_latest, 300);
		}

		return $articles_latest;
	}
}