<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Article_categories_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Article_categories_model extends BF_Model {

	protected $table_name			= 'article_categories';
	protected $key					= 'article_category_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'article_category_created_on';
	protected $created_by_field		= 'article_category_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'article_category_modified_on';
	protected $modified_by_field	= 'article_category_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'article_category_deleted';
	protected $deleted_by_field		= 'article_category_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'article_category_id',
			'article_category_name',

			'article_category_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'article_category_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = article_category_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = article_category_modified_by', 'LEFT')
					->datatables($fields);
	}

	/**
	 * Article Categories
	 *
	 * @author Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_all_categories()
	{
		if( ! $article_categories = $this->cache->get('article_categories'))
		{
			$article_categories = $this->where('article_category_deleted', 0)
				 					   ->find_all();

			$this->cache->save('article_categories', $article_categories, 300);
		}

		return $article_categories;
	}

	/**
	 * Article Categories Dropdown
	 *
	 * @param string $key
	 * @param string $val
	 * @param boolean $blank
	 * @author Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_categories_dropdown($key, $val, $blank = FALSE)
	{
		if( ! $article_categories_dropdown = $this->cache->get('categories_dropdown'))
		{
			$article_categories_dropdown = $this->where('article_category_deleted', 0)
										->order_by('article_category_name')
										->format_dropdown($key, $val, $blank);
			
			$this->cache->save('article_categories_dropdown', $article_categories_dropdown, 300);
		}

		return $article_categories_dropdown;
	}

}