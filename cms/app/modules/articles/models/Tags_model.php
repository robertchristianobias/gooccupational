<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Tags_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Tags_model extends BF_Model {

	protected $table_name			= 'tags';
	protected $key					= 'tag_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'tag_created_on';
	protected $created_by_field		= 'tag_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'tag_modified_on';
	protected $modified_by_field	= 'tag_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'tag_deleted';
	protected $deleted_by_field		= 'tag_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'tag_id',
			'tag_name',

			'tag_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'tag_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = tag_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = tag_modified_by', 'LEFT')
					->datatables($fields);
	}
}