<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Articles_filter_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Articles_filter_model extends BF_Model {

	protected $table_name			= 'articles_filter';
	protected $key					= 'article_filter_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'article_filter_created_on';
	protected $created_by_field		= 'article_filter_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'article_filter_modified_on';
	protected $modified_by_field	= 'article_filter_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'article_filter_deleted';
	protected $deleted_by_field		= 'article_filter_deleted_by';

	public function get_articles_filter($article_id)
	{
		if( ! $articles_filter = $this->cache->get('articles_filter_' . $article_id))
		{
			$articles_filter = $this->where('article_filter_deleted', 0)
								    ->where('article_filter_article_id', $article_id)
						 	 		->order_by('article_filter_created_on', 'DESC')
						 	 		->find_all();
		
			$this->cache->save('articles_filter_' . $article_id, $articles_filter, 300);
		}

		return $articles_filter;
	}
}