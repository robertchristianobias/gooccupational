<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Facebook_pages_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2018, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Facebook_pages_model extends BF_Model {

	protected $table_name			= 'facebook_pages';
	protected $key					= 'facebook_page_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'facebook_page_created_on';
	protected $created_by_field		= 'facebook_page_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'facebook_page_modified_on';
	protected $modified_by_field	= 'facebook_page_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'facebook_page_deleted';
	protected $deleted_by_field		= 'facebook_page_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'facebook_page_id',
			'facebook_page_page_id',

			'facebook_page_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'facebook_page_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = facebook_page_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = facebook_page_modified_by', 'LEFT')
					->datatables($fields);
	}
}