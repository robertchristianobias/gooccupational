<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_article_categories extends CI_Migration {

	private $_table = 'article_categories';

	private $_permissions = array(
		array('Article Categories Link', 'articles.article_categories.link'),
		array('Article Categories List', 'articles.article_categories.list'),
		array('View Article Category', 'articles.article_categories.view'),
		array('Add Article Category', 'articles.article_categories.add'),
		array('Edit Article Category', 'articles.article_categories.edit'),
		array('Delete Article Category', 'articles.article_categories.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'articles',
			'menu_text' 		=> 'Categories',    
			'menu_link' 		=> 'articles/article_categories', 
			'menu_perm' 		=> 'articles.article_categories.link', 
			'menu_icon' 		=> 'fa fa-leaf', 
			'menu_order' 		=> 1, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'article_category_id'		=> array('type' => 'TINYINT', 'constraint' => 1, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'article_category_name'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),

			'article_category_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'article_category_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'article_category_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'article_category_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'article_category_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'article_category_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		//$this->dbforge->add_key('article_category_id', TRUE);
		$this->dbforge->add_key('article_category_id', TRUE);
		$this->dbforge->add_key('article_category_name');

		$this->dbforge->add_key('article_category_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		//$this->dbforge->drop_table($this->_table, TRUE);
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}