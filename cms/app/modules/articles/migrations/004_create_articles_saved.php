<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_articles_saved extends CI_Migration {

	private $_table = 'articles_saved';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'article_saved_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'article_saved_user_id'	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => TRUE),
			'article_saved_article_id'			=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			
			'article_saved_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'article_saved_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'article_saved_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'article_saved_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'article_saved_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'article_saved_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('article_saved_id', TRUE);
		$this->dbforge->add_key('article_saved_user_id');
		$this->dbforge->add_key('article_saved_article_id');

		$this->dbforge->add_key('article_saved_deleted');
		$this->dbforge->create_table($this->_table, TRUE);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);
	}
}