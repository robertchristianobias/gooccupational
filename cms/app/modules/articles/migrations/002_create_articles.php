<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_articles extends CI_Migration {

	private $_table = 'articles';

	private $_permissions = array(
		array('Articles Link', 'articles.articles.link'),
		array('Articles List', 'articles.articles.list'),
		array('View Article', 'articles.articles.view'),
		array('Add Article', 'articles.articles.add'),
		array('Edit Article', 'articles.articles.edit'),
		array('Delete Article', 'articles.articles.delete'),
		array('Articles Settings', 'articles.articles.settings')
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'none', // none if parent or single menu
			'menu_text' 		=> 'Articles', 
			'menu_link' 		=> 'articles', 
			'menu_perm' 		=> 'articles.articles.link',
			'menu_icon' 		=> 'fa fa-paperclip', 
			'menu_order' 		=> 3, 
			'menu_active' 		=> 1
		),
		array(
			'menu_parent'		=> 'articles',
			'menu_text' 		=> 'Articles',    
			'menu_link' 		=> 'articles/articles', 
			'menu_perm' 		=> 'articles.articles.link', 
			'menu_icon' 		=> 'fa fa-newspaper-o', 
			'menu_order' 		=> 2, 
			'menu_active' 		=> 1
		)
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'article_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'article_category_id'	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => TRUE),
			'article_title'			=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'article_slug'	    	=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE, 'after' => 'article_title'),
			'article_excerpt' 		=> array('type' => 'TINYTEXT', 'after' => 'article_slug'),
			'article_content'		=> array('type' => 'TEXT', 'null' => FALSE),
			'article_photo'			=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'article_status'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'article_metatag_id'    => array('type' => 'INT', 'constraint' => 10, 'null' => TRUE, 'default' => 0, 'after' => 'article_photo'),
			
			'article_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'article_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'article_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'article_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'article_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'article_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		//$this->dbforge->add_key('article_id', TRUE);
		$this->dbforge->add_key('article_id', TRUE);
		$this->dbforge->add_key('article_category_id');
		$this->dbforge->add_key('article_title');

		$this->dbforge->add_key('article_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		//$this->dbforge->drop_table($this->_table, TRUE);
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}