<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Articles Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */


class Articles extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('articles_model');
		$this->load->model('article_categories_model');
		$this->load->model('metatags/metatags_model');

		$this->load->language('articles');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
		$this->acl->restrict('articles.articles.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('articles'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');
	
		// render the page
		// $this->template->add_css(module_css('articles', 'articles_index'), 'embed');

		$this->template->add_js(module_js('articles', 'articles_index'), 'embed');
		$this->template->write_view('content', 'articles_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('articles.articles.list');

		echo $this->articles_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('articles.articles.' . $action);
		
		$data['page_heading']   = lang($action . '_heading');
		$data['page_subhead']   = lang($action . '_subhead');
		$data['action'] = $action;
		$data['article_categories'] = $this->article_categories_model->get_categories_dropdown('article_category_id', 'article_category_name', TRUE);

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('articles'));
		$this->breadcrumbs->push(lang($action . '_heading'), site_url('articles/form/' . $action));

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(
					array(
						'success'  => true, 
						'message'  => lang($action . '_success'),
						'redirect' => site_url('articles/articles') 
					)
				); 
				exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'article_category_id'	=> form_error('article_category_id'),
					'article_title'			=> form_error('article_title'),
				 	'article_slug'          => form_error('article_slug'),
					'article_excerpt'		=> form_error('article_excerpt'),
					'article_content'		=> form_error('article_content'),
					'article_status'		=> form_error('article_status'),
					'article_photo'			=> form_error('article_photo')
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->articles_model->find($id);

		// render the page
		//$this->template->set_template('modal');

		$this->template->add_js('components/dropzone/dist/dropzone.js');
		$this->template->add_css('components/dropzone/dist/dropzone.css');

		$this->template->add_js('components/select2/dist/js/select2.min.js');
		$this->template->add_css('components/select2/dist/css/select2.min.css');
		
		$this->template->add_js('components/ckeditor/ckeditor.js');
		
		$this->template->add_css(module_css('articles', 'articles_form'), 'embed');
		$this->template->add_js(module_js('articles', 'articles_form'), 'embed');

		$this->template->write_view('content', 'articles_form', $data);
		$this->template->render();
	}


	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function delete($id)
	{
		$this->acl->restrict('articles.articles.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->articles_model->delete($id);

			$this->cache->delete('articles');
			$this->cache->delete('articles_latest');

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		$this->form_validation->set_rules('article_category_id', lang('article_category_id'), 'required');
		$this->form_validation->set_rules('article_title', lang('article_title'), 'required');
		$this->form_validation->set_rules('article_excerpt', lang('article_excerpt'), 'required');
		$this->form_validation->set_rules('article_content', lang('article_content'), 'required');
		$this->form_validation->set_rules('article_status', lang('article_status'), 'required');
		// $this->form_validation->set_rules('article_photo', lang('article_photo'), 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ( $this->form_validation->run($this) == FALSE )
		{
			return FALSE;
		}
		$data = array(
			'article_category_id'	=> $this->input->post('article_category_id'),
			'article_title'			=> $this->input->post('article_title'),
			'article_slug' 			=> url_title($this->input->post('article_title'), '-', true),
			'article_excerpt'	    => $this->input->post('article_excerpt'),
			'article_content'		=> $this->input->post('article_content'),
			'article_status'		=> $this->input->post('article_status'),
			'article_photo'			=> $this->input->post('article_photo'),
		);

		if ($action == 'add')
		{
			$insert_id = $this->articles_model->insert($data);
			// $return = (is_numeric($insert_id)) ? $insert_id : FALSE;

			// Add metatags
			$meta_data = array(
				'metatag_title'				  => $this->input->post('article_title'),
				'metatag_description'   	  => $this->input->post('article_excerpt'),
				'metatag_og_title'			  => $this->input->post('article_title'),
				'metatag_og_image'			  => '',
				'metatag_og_description' 	  => $this->input->post('article_excerpt'),
				'metatag_twitter_title'	 	  => $this->input->post('article_title'),
				'metatag_twitter_image'		  => '',
				'metatag_twitter_description' => $this->input->post('article_excerpt')
			);

			$meta_id = $this->metatags_model->insert($meta_data);
			
			// Update articles
			$return = $this->articles_model->update($insert_id, array(
				'article_metatag_id' => $meta_id
			));

			$id = $insert_id;
		}
		else if ($action == 'edit')
		{
			$this->cache->delete('article_'.$data['article_slug']);

			$this->articles_model->update($id, $data);

			$record = $this->articles_model->find($id);

			// update metatag
			$meta_data = array(
				'metatag_title'				  => $this->input->post('article_title'),
				'metatag_description'   	  => $this->input->post('article_excerpt'),
				'metatag_og_title'			  => $this->input->post('article_title'),
				'metatag_og_image'			  => '',
				'metatag_og_description' 	  => $this->input->post('article_excerpt'),
				'metatag_twitter_title'	 	  => $this->input->post('article_title'),
				'metatag_twitter_image'		  => '',
				'metatag_twitter_description' => $this->input->post('article_excerpt')
			);

			$return = $this->metatags_model->update($record->article_metatag_id, $meta_data);
		}

		// Delete Cache
		$this->cache->delete('articles');
		
		return $return;
	}
}

/* End of file Articles.php */
/* Location: ./application/modules/articles/controllers/Articles.php */