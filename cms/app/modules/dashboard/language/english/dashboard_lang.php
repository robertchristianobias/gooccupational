<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Dashboard Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		robertchristianobias@gmail.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Dashboard';

// Labels


// Buttons


// Index Function
$lang['index_heading']				= 'Dashboard';
$lang['index_subhead']				= 'It all starts here';