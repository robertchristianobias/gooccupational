<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Contacts Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Contacts';

// Labels


// Buttons
$lang['button_add']					= 'Add Contact';
$lang['button_update']				= 'Update Contact';
$lang['button_delete']				= 'Delete Contact';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Contacts';
$lang['index_subhead']				= 'List of all Contacts';
$lang['index_id']					= 'ID';
$lang['index_username']				= 'Username';
$lang['index_password']				= 'Password';
$lang['index_api_key']				= 'API Key';
$lang['index_status']				= 'Status';


// View Function
$lang['view_heading']				= 'View Contact';

// Add Function
$lang['add_heading']				= 'Add Contact';
$lang['add_success']				= 'Contact has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Contact';
$lang['edit_success']				= 'Contact has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Contact';
$lang['delete_confirm']				= 'Are you sure you want to delete this Contact?';
$lang['delete_success']				= 'Contact has been successfully deleted';