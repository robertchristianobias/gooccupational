<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Migration_Create_contacts extends CI_Migration 
{
	private $_table = 'contacts';

	private $_permissions = array(
		array('ContactsLink', 'contacts.contacts.link'), 
		array('List Contacts', 'contacts.contacts.list'), 
		array('View Contact', 'contacts.contacts.view'),
		array('Add Contact', 'contacts.contacts.add'),
		array('Edit Contact', 'contacts.contacts.edit'),
		array('Delete Contact', 'contacts.contacts.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'none', // none if parent or single menu
			'menu_text' 		=> 'Contacts', 
			'menu_link' 		=> 'contacts', 
			'menu_perm' 		=> 'contacts.contacts.link', 
			'menu_icon' 		=> 'fa fa-address-card', 
			'menu_order' 		=> 2, 
			'menu_active' 		=> 1
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'contact_id' 				=> array('type' => 'SMALLINT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'contact_user_id'			=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'contact_by_user_id'		=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'contact_created_by' 		=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'contact_created_on' 		=> array('type' => 'DATETIME', 'null' => TRUE),
			'contact_modified_by' 		=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'contact_modified_on' 		=> array('type' => 'DATETIME', 'null' => TRUE),
			'contact_deleted' 			=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE, 'default' => 0),
			'contact_deleted_by' 		=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('contact_id', TRUE);
		$this->dbforge->add_key('contact_user_id');

		$this->dbforge->add_key('contact_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}