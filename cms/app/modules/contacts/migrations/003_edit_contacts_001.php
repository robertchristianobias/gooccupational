<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Migration_Edit_contacts_001 extends CI_Migration {

	private $_table = 'contacts';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'contact_status' => array('type' => 'TINYINT', 'constraint' => '1', 'null' => FALSE, 'after' => 'contact_by_user_id', 'comment' => '1 = Active, 2 = Pending'),
		);

		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'contact_status');
	}
}