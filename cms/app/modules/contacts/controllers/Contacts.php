<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Contacts Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Contacts extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->library('users/acl');
		$this->load->language('contacts');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
		$this->acl->restrict('contacts.contacts.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('contacts'));
		$this->breadcrumbs->push(lang('index_heading'), site_url('contacts'));
		
		// add plugins
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');
		
		// render the page
		$this->template->add_css(module_css('contacts', 'contacts_index'), 'embed');
		$this->template->add_js(module_js('contacts', 'contacts_index'), 'embed');
		$this->template->write_view('content', 'contacts_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('contacts.contacts.list');

		echo $this->contacts_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('contacts.contacts.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(array('success' => true, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'username'		=> form_error('username'),
					'password'		=> form_error('password'),
					'contacts_key'	=> form_error('contacts_key'),
					'status'		=> form_error('status'),
				);
				echo json_encode($response);
				exit;
			}
		}

		// random password and contacts key
		$data['new_password'] = random_string('sha1');
		$data['new_contacts_key'] = random_string('sha1');

		if ($action != 'add') $data['record'] = $this->contacts_model->find($id);

		// render the page
		$this->template->set_template('modal');
		$this->template->add_css(module_css('contacts', 'contacts_form'), 'embed');
		$this->template->add_js(module_js('contacts', 'contacts_form'), 'embed');
		$this->template->write_view('content', 'contacts_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('contacts.contacts.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->contacts_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../modules/core/views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('username', lang('username'), 'required');
		$this->form_validation->set_rules('password', lang('password'), 'required');
		$this->form_validation->set_rules('contacts_key', lang('contacts_key'), 'required');
		$this->form_validation->set_rules('status', lang('status'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'username'		=> $this->input->post('username'),
			'password'		=> $this->input->post('password'),
			'contacts_key'		=> $this->input->post('contacts_key'),
			'status'		=> $this->input->post('status'),
		);
		

		if ($action == 'add')
		{
			$insert_id = $this->contacts_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->contacts_model->update($id, $data);
		}

		return $return;

	}
}

/* End of file Users.php */
/* Location: ./application/modules/contacts/controllers/Users.php */