<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Migration_Create_messages extends CI_Migration {

	private $_table = 'messages';

	private $_permissions = array(
		array('Messages Link', 'messages.messages.link'),
		array('Messages List', 'messages.messages.list'),
		array('View Message', 'messages.messages.view'),
		array('Add Message', 'messages.messages.add'),
		array('Edit Message', 'messages.messages.edit'),
		array('Delete Message', 'messages.messages.delete'),
		array('Messages Settings', 'messages.messages.settings')
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'none', // none if parent or single menu
			'menu_text' 		=> 'Messages', 
			'menu_link' 		=> 'messages', 
			'menu_perm' 		=> 'messages.messages.link',
			'menu_icon' 		=> 'fa fa-paperclip', 
			'menu_order' 		=> 3, 
			'menu_active' 		=> 1
		)
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'message_id'			=> array('type' => 'INT', 'constraint' => 10, 'auto_increment' => TRUE, 'unsigned' => TRUE, 'null' => FALSE),
			'message_from_id'		=> array('type' => 'INT', 'constraint' => 11, 'unsigned' => TRUE, 'null' => TRUE),
			'message_to_id'			=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'message_subject'	    => array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'message_content' 		=> array('type' => 'TEXT'),
			'message_status'		=> array('type' => 'VARCHAR', 'constraint' => 20, 'null' => FALSE),
			
			'message_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'message_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'message_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'message_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'message_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'message_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('message_id', TRUE);
		$this->dbforge->add_key('message_from_id');
		$this->dbforge->add_key('message_to_id');

		$this->dbforge->add_key('message_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}