<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2018, 
 * @link		http://www.rchristianobias.com
 */
class Migration_Add_permissions extends CI_Migration 
{
	private $_permissions = array(
		array('Newsfeed Link', 'accounts.newsfeed.link'),
		array('Newsfeed List', 'accounts.newsfeed.list'),

		array('Messages Link', 'accounts.message.link'),
		array('Messages List', 'accounts.message.list'),
		array('Messages Compose', 'accounts.message.compose'),

		array('Contacts Link', 'accounts.contact.link'),
		array('Contacts List', 'accounts.contact.list'),
		array('Contact Invite', 'accounts.contact.invite'),
		array('Contact Add', 'accounts.contact.add'),
		array('Contact Edit', 'accounts.contact.edit'),
		array('Contact Delete', 'accounts.contact.delete'),

		array('Document Link', 'accounts.document.link'),
		array('Document List', 'accounts.document.list'),
		array('Document Add', 'accounts.document.add'),
		array('Document Edit', 'accounts.document.edit'),
		array('Document Delete', 'accounts.document.delete'),

		array('Template Link', 'accounts.document_template.link'),
		array('Template List', 'accounts.document_template.list'),
		array('Template Add', 'accounts.document_template.add'),
		array('Template Edit', 'accounts.document_template.edit'),
		array('Template Delete', 'accounts.document_template.delete'),

		array('Schedules Link', 'accounts.schedule.link'),
		array('Schedules List', 'accounts.schedule.list'),
		array('Schedule Add', 'accounts.schedule.add'),
		array('Schedule Edit', 'accounts.schedule.edit'),
		array('Schedule Delete', 'accounts.schedule.delete')
	);

	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$this->migrations_model->add_permissions($this->_permissions);
	}

	public function down()
	{
		$this->migrations_model->delete_permissions($this->_permissions);
	}
}