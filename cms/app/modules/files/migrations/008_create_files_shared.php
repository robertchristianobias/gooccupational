<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, 
 * @link		http://www.rchristianobias.com
 */
class Migration_Create_files_shared extends CI_Migration 
{
	private $_table = 'files_shared';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('core/migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'file_shared_id' 		=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'file_shared_file_id'	=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'file_shared_to_id'		=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'file_shared_from_id'	=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'file_shared_type'		=> array('type' => 'VARCHAR', 'constraint' => 20, 'null' => FALSE),

			'file_shared_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'file_shared_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'file_shared_modified_by' => array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'file_shared_modified_on' => array('type' => 'DATETIME', 'null' => TRUE),
			'file_shared_deleted' 	=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE, 'default' => 0),
			'file_shared_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('file_shared_id', TRUE);
		$this->dbforge->add_key('file_shared_file_id');
		$this->dbforge->add_key('file_shared_to_id');
		$this->dbforge->add_key('file_shared_from_id');

		$this->dbforge->add_key('file_shared_deleted');
		$this->dbforge->create_table($this->_table, TRUE);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table);
	}
}