/**
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, 
 * @link		http://www.rchristianobias.com
 */



$(function() {

	var element = 'images_photo';
		dropzone_element = $('#' + element + '_dropzone');
		 
	// Prevent dropzone autodiscover
	Dropzone.autoDiscover = false;

	dropzone_element.dropzone({
		url: site_url + 'files/images/upload',
		clickable: true,
		addRemoveLinks: true,
		maxFiles: 1,
		maxFilesize: 2,
		acceptedFile: 'image/*',
		init: function() {

			this.on('success', function(file, data) { // Successfully process the file

				var response = $.parseJSON(data);
				
				if(response.status == 'success') {
					$('#'+element).val(response.image);
					$('#photo-wrapper').html('<a href="'+site_url+'files/images/select" data-toggle="modal" data-target="#modal"><img src="'+site_url+response.small+'" class="img-responsive" /></a>');

				} else {
					$('#error-'+element).html('<span class="text-danger">'+response.error+'</span>');
				}
				
			});

			// Remove file when it exceeds the allowed a
			this.on('maxfilesexceeded', function(file) {
				this.removeFile(file);
			}); 

			// TODO
			this.on("removedfile", function(file) {
				$('#'+element).val(''); 
				$('#error-'+element).html('');
			});
		}
	});
});
