/**
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, 
 * @link		http://www.rchristianobias.com
 */

$(function() {

	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "documents/datatables",
		"lengthMenu": [[12, 20, 50, 100, 300, -1], [12, 20, 50, 100, 300, "All"]],
		"pagingType": "simple",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next',
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "desc" ]],
		"stateSave": true,
		"aoColumnDefs":
		[
			{
				"aTargets": [0],
				"mRender": function (data, type, full) {

					return '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12  file-box">' +
						   '	<div class="file">' +
						   '		<a href="' + site_url + 'files/documents/share/' + full['0'] + '" data-toggle="modal" data-target="#modal">' +
						   '			<div class="icon">'+
						   '				<i class="zmdi zmdi-file-text"></i>' +
						   '			</div> ' +
						   '			<div class="file-name"> ' + full[1] +
						   '			<br />' +
						   '			<span></span>' +
						   '			</div>' +
						   '		</a>' +
						   '	</div>' +
						   '</div>';
				},
			},
			{
				"aTargets": [0,1,2,3],
				"mRender": function (data, type, full) {
					return '<span class="d-none">' + data + '</span>';
				},
			},
		],
		"fnDrawCallback": function( oTable ) {
			// hide the table
			$('#datatables').hide();

			// then recreate the table as divs
			var html = '';
			$('tr', this).each(function() {
				$('td', this).each(function() {
					html += $(this).html();
					// console.log(html);
				});
			});

			$('#thumbnails').html(html);
		}
	});

	// positions the button next to searchbox
	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {
		// eg. destroys the wysiwyg editor
	});

	equalHeight($("#thumbnails .thumbnail .caption"));

});

$(window).resize(function() {
	equalHeight($("#thumbnails .thumbnail .caption"));
});

function equalHeight(group) {
	var tallest = 0;
	group.each(function() {
		var thisHeight = $(this).height();
		if(thisHeight > tallest) {
			tallest = thisHeight;
		}
	});
	group.each(function() { $(this).height(tallest); });
}