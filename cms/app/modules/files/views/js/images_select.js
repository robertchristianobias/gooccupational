/**
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, RC MediaPH
 * @link		http://www.rcmediaph.com
 */

$(function() {

    handle_single_image_select_dropzone('images_photo', site_url + 'files/images/upload');
    
    // renders the datatables (datatables.net)
	var oTable = $('#dt-images').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": site_url + "files/images/datatables",
		"lengthMenu": [[4, 27, 50, 100, 300, -1], [4, 27, 50, 100, 300, "All"]],
		"pagingType": "simple",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next',
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "desc" ]],
		"aoColumnDefs": 
		[
			{
				"aTargets": [5],
				"mRender": function (data, type, full) {
                    var buttons = '';
                    
                    buttons = '<button class="btn btn-xs btn-default btn-image" data-image="' + full[4] + '">Insert Image</button> ';
                    
					return '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-6"><div class="thumbnail"><div class="caption"><h4>Select the image</h4>' + buttons + '</div><img src="' + site_url + data + '" class="img-responsive" width="100%" data-id="' + full[0] + '" /></div></div>';
				},
			},
			{
				"aTargets": [0,1,2,3,4,6,7,8,9,10],
				"mRender": function (data, type, full) {
					return '<span class="hide">' + data + '</span>';
				},
			},
		],
		"fnDrawCallback": function( oTable ) {

			// hide the table
			$('#dt-images').hide();

			// then recreate the table as divs
			var html = '';
			$('tr', this).each(function() {
				$('td', this).each(function() {
					html += $(this).html();
					// console.log(html);
				});
			});

			$('#thumbnails').html(html);
		}
    });
    
    $("#image_sizes, #thumbnails").on( "mouseenter", ".thumbnail", function( event ) {
        $(this).find('.caption').slideDown(250);
    }).on( "mouseleave", ".thumbnail", function( event ) {
        $(this).find('.caption').slideUp(250);
	});
	
	$('.image-select-close').click(function() {
		$('#modal').modal('hide');

		return false;
	});

    $('#image_sizes, #thumbnails').on("click", ".btn-image", function() {

        var image = $(this).data('image');

        // var thumb = $(this).attr('data-thumb');
        // $('#banner_image').val(image);
        // $('#banner_thumb').val(thumb);
        // $('#preview_image_thumb').attr('src', site_url + thumb);
    
        // $('#image').addClass('hide');
        // $('#form').removeClass('hide');

        $('#images_photo').val(image);
        $('#photo-wrapper').html('<a href="'+site_url+'files/images/select" data-toggle="modal" data-target="#modal"><img src="'+site_url+image+'" class="img-responsive" /></a>');

        return false;
    });
});