
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h5 class="modal-title" id="modalLabel"><?php echo $page_heading?></h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12">
					<?php echo form_open(current_url(), 'id="image_share_form"'); ?>
						<input type="hidden" name="file_shared_file_id" value="<?php echo $this->uri->segment('4'); ?>" />
						<div class="panel-wrapper collapse in">
							<div class="panel-body pa-0">
								<div class="col-sm-12 col-xs-12">
									<div class="form-group">
										<label class="control-label">People: </label>
										<?php if($contacts) { ?>
											<select name="file_shared_to_id" class="form-control select2">
												<option value=""></option>
												<?php foreach ( $contacts as $contact ) { ?>
													<option value="<?php echo $contact->contact_user_id; ?>"><?php echo $contact->email; ?></option>
												<?php } ?>
											</select>
										<?php } ?>
										<div id="error-file_shared_to_id"></div>
									</div>
								</div>
							</div>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">
				<i class="fa fa-times"></i> <?php echo lang('button_close')?>
			</button>
			<button id="submit-share" class="btn btn-success btn-anim" type="submit" data-loading-text="<?php echo lang('processing')?>">
				<i class="fa fa-save"></i> 
				<span class="btn-text">Done</span>
			</button>
		</div>
	</div>
</div>