<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">
	<?php echo form_open('', 'class="form-horizontal" id="merchant_form"'); ?>
            <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#upload">Upload Image</a></li>
            <li><a data-toggle="tab" href="#existing">Add Existing Image</a></li>
        </ul>
        <div class="tab-content">
            <div id="upload" class="tab-pane fade in active clearfix">
                <div class="col-md-12">
                    <div class="form-group">
                        <?php if(isset($record->images_photo)) { ?>
                            <div id="dropzone-holder">
                                <div class="dropzone" id="images_photo_dropzone"></div>
                                <div id="dropzone-image">
                                    <span class="close-btn"><a href="javascript:;" class="close-x" title="Remove Photo">x</a></span>
                                    <img src="<?php echo display_image($record->images_photo, 'thumb'); ?>" />
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="dropzone" id="images_photo_dropzone"></div>
                        <?php } ?>
                        <div class="help-text">Accepts jpg, png with 2mb filesize.</div>
                        <div id="error-images_photo"></div>
                    </div>
                </div>
            </div>
            <div id="existing" class="tab-pane fade clearfix">
                <table class="table table-striped table-bordered table-hover dt-responsive" id="dt-images">
                    <thead>
                        <tr>
                            <th class="all"><?php echo lang('index_id')?></th>
                            <th class="min-desktop"><?php echo lang('index_width'); ?></th>
                            <th class="min-desktop"><?php echo lang('index_height'); ?></th>
                            <th class="min-desktop"><?php echo lang('index_name'); ?></th>
                            <th class="min-desktop"><?php echo lang('index_file'); ?></th>
                            <th class="min-desktop"><?php echo lang('index_thumb'); ?></th>

                            <th class="none"><?php echo lang('index_created_on')?></th>
                            <th class="none"><?php echo lang('index_created_by')?></th>
                            <th class="none"><?php echo lang('index_modified_on')?></th>
                            <th class="none"><?php echo lang('index_modified_by')?></th>
                            <th class="min-tablet"><?php echo lang('index_action')?></th>
                        </tr>
                    </thead>
                </table>
                <div id="thumbnails" class="row text-center"></div>
            </div>
                

	<?php echo form_close(); ?>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default image-select-close">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<button type="button" class="btn btn-success image-select-close" data-dismiss="image-select-modal" id="submit">
        <i class="fa fa-save"></i> Add Image
    </button>
</div>