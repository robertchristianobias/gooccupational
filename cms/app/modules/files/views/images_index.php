<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view pa-0">
			<div class="panel-wrapper collapse in">
				<div class="panel-body pa-0">
					<div class="">
						<div class="col-lg-3 col-md-4 file-directory pa-0">
							<div class="ibox float-e-margins">
								<div class="ibox-content">
									<div class="file-manager">
										<div class="mt-20 mb-20 ml-15 mr-15">
											<?php if ($this->acl->restrict('files.images.add', 'return')): ?>
												<a href="<?php echo site_url('files/images/form/add')?>" data-toggle="modal" data-target="#modal" class="btn btn-sm btn-primary btn-add btn-block" id="btn_add"><span class="fa fa-plus"></span> <?php echo lang('button_add')?></a>
											<?php endif; ?>
										</div>
										<h6 class="pl-15 mb-10">Tags</h6>
										<ul class="tag-list pl-15 pr-15">
											<li><a href="">Family</a></li>
											<li><a href="">Work</a></li>
											<li><a href="">Home</a></li>
											<li><a href="">Children</a></li>
											<li><a href="">Holidays</a></li>
											<li><a href="">Music</a></li>
											<li><a href="">Photography</a></li>
											<li><a href="">Film</a></li>
										</ul>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-8 file-sec pt-20">
							<div class="row">
								<div class="col-lg-12">
								<table class="table table-striped table-bordered table-hover dt-responsive" id="datatables">
									<thead>
										<tr>
											<th class="all"><?php echo lang('index_id')?></th>
											<th class="min-desktop"><?php echo lang('index_width'); ?></th>
											<th class="min-desktop"><?php echo lang('index_height'); ?></th>
											<th class="min-desktop"><?php echo lang('index_name'); ?></th>
											<th class="min-desktop"><?php echo lang('index_file'); ?></th>
											<th class="min-desktop"><?php echo lang('index_file'); ?></th>
											<th class="min-desktop"><?php echo lang('index_file'); ?></th>
											<th class="min-desktop"><?php echo lang('index_file'); ?></th>
											<th class="min-desktop"><?php echo lang('index_thumb'); ?></th>

											<th class="d-none"><?php echo lang('index_created_on')?></th>
											<th class="d-none"><?php echo lang('index_created_by')?></th>
											<th class="d-none"><?php echo lang('index_modified_on')?></th>
											<th class="d-none"><?php echo lang('index_modified_by')?></th>
											<th class="min-tablet"><?php echo lang('index_action')?></th>
										</tr>
									</thead>
								</table>
								<br>
								<div id="thumbnails" class="row text-center"></div>
								<br>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>