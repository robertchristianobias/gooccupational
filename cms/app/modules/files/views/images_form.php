<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h5 class="modal-title" id="modalLabel"><?php echo $page_heading?></h5>
		</div>
		<div class="modal-body">
			<div class="form-group">
				<div class="dropzone" id="images_photo_dropzone"></div>
				<div class="help-text">Accepts jpg, png with 2mb filesize.</div>
				<div id="error-images_photo"></div>
			</div>
		</div>
	</div>
</div>


