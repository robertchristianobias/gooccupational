<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Files_shared_model Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, 
 * @link		http://www.rchristianobias.com
 */
class Files_shared_model extends BF_Model 
{

	protected $table_name			= 'files_shared';
	protected $key					= 'file_shared_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'file_shared_created_on';
	protected $created_by_field		= 'file_shared_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'file_shared_modified_on';
	protected $modified_by_field	= 'file_shared_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'file_shared_deleted';
	protected $deleted_by_field		= 'file_shared_deleted_by';
}