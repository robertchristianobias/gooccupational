<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Images Language Image (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2016, 
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']		= 'Files';

// Labels
$lang['image_size_large']	= 'Image Size (Large)';
$lang['image_size_medium']	= 'Image Size (Medium)';
$lang['image_size_small']	= 'Image Size (Small)';
$lang['image_size_thumb']	= 'Image Size (Thumb)';
$lang['youtube_api_key']	= 'Youtube API Key';

// Settings Function
$lang['settings_heading']	= 'Files Settings';
$lang['settings_subhead']	= 'Configure the files module here';
$lang['settings_success']	= 'File settings have been successfully updated';