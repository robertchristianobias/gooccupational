<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
	function run($module = '', $group = '') 
	{
		(is_object($module)) AND $this->CI =& $module;
		return parent::run($group);
	}

	public function valid_date($date)
	{
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') === $date;
	}

	/**
	 * Is Unique
	 *
	 * Extend is_unique validation to work when updating the record
	 * 
	 * @param string $str
	 * @param string $params
	 * @return boolean
	 * @author Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function is_unique($str, $params)
    {
		list($table, $field, $key, $val, $deleted) = explode('.', $params);

		if($val != 0)
		{
			$where = array(
				$field 	   => $str, 
				$key.' !=' => $val,
				$deleted   => '0'
			);

			$rows = $this->CI->db->limit('1')
						         ->get_where($table, $where)
						 	     ->num_rows();
		}
		else
		{
			$where = array(
				$field 	 => $str,
				$deleted => '0'
			);

			$rows = $this->CI->db->get_where($table, $where)
								 ->num_rows();
		}
		
        return $rows > 0 ? FALSE : TRUE;
	}
}
/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */