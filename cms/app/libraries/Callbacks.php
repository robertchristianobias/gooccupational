<?php  if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Callbacks Class
 *
 *
 * @package		Callback
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 */
class Callbacks {

    // --------------------------------------------------------------------
	/**
	 * Images
	 *
	 * @access	public
	 * @param	array $params
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
    public static function images($params)
    {
        $data = array();

        if($params)
        {
            $counter = 0;
            $arr = array();
            foreach($params as $key=>$val)
            {
                $arr = array(
                    'image_created_on' => date('M d, Y', strtotime($val['image_created_on']))
                );
                
                $data[$counter] = array_merge($params[$counter], $arr);
                $counter++;
            }
        }
        
        return $data;
    }
    
}