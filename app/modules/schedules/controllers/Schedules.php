<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Schedules extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
		$this->load->library('ion_auth');

		$this->load->model('schedules_model');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index() 
	{
		$data['page_heading']    = 'Schedules';
		$data['page_subheading'] = '';

		$this->template->write_view('content', 'schedules_index', $data);
		$this->template->render();
	}

	public function form($action = 'add', $id = 0)
	{

		if ($_POST)
		{
			$this->form_validation->set_rules('schedule_title', lang('schedule_title'), 'required');
			$this->form_validation->set_rules('schedule_description', lang('schedule_description'), 'required');
			$this->form_validation->set_rules('schedule_date', lang('schedule_date'), 'required');
			$this->form_validation->set_rules('schedule_time_from', lang('schedule_time_from'), 'required');
			$this->form_validation->set_rules('schedule_time_to', lang('schedule_time_to'), 'required');

			$this->form_validation->set_message('required', 'This field is required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'schedule_title' 		=> form_error('schedule_title'),
					'schedule_description'  => form_error('schedule_description'),		
					'schedule_date'		 	=> form_error('schedule_date'),
					'schedule_time_from'	=> form_error('schedule_time_from'),
					'schedule_time_to'		=> form_error('schedule_time_to')
				);
			}
			else
			{
			}

			echo json_encode($response); exit;
		}

		$data = array();

		$this->template->set_template('modal');

		$this->template->write_view('content', 'schedules_form', $data);
		$this->template->render();
	}

	public function calendar_schedules()
	{
		$schedules = $this->schedules_model->get_schedules();
		$schedules_arr = array();

		if ( $schedules )
		{
			foreach ( $schedules as $schedule )
			{
				$schedules_arr[] = array(
					'id'	=> $schedule->schedule_id,
					'title' => $schedule->schedule_title,
					'url'	=> '',
					'class' => 'event-important',
					'start'	=> strtotime($schedule->schedule_date_from) * 1000,
					'end'   => strtotime($schedule->schedule_date_to) * 1000
				);
			}
		}

		echo json_encode(array(
			'success' => 1,
			'result'  => $schedules_arr
		)); exit;
	}
}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */