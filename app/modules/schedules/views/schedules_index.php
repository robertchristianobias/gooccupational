<div class="row">
    <div class="col-md-9 col-md-push-3">
        <div class="mail-box">
            <div class="row">
                <aside class="col-md-12">
                    <div class="panel panel-refresh pa-0">
                        <div class="panel-heading pt-20 pb-20 pl-15 pr-15">
                            <div class="pull-left">
                                <div class="h4">Appointments</div>                                
                            </div>
                            <div class="pull-right">
                                <!-- <a href="<?php echo site_url('accounts/schedules/form/add'); ?>" class="open-popup-ajax btn btn-success btn-block">
                                    Add Schedule
                                </a> -->
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body inbox-body pa-0">
                               <div class="row">
                                    <div class="col-md-12">
                                        <h3></h3>
                                        <div class="pull-right form-inline">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
                                                <button class="btn" data-calendar-nav="today">Today</button>
                                                <button class="btn btn-primary" data-calendar-nav="next">Next >></button>
                                            </div>
                                            <div class="btn-group">
                                                <button class="btn btn-warning" data-calendar-view="year">Year</button>
                                                <button class="btn btn-warning active" data-calendar-view="month">Month</button>
                                                <button class="btn btn-warning" data-calendar-view="week">Week</button>
                                                <button class="btn btn-warning" data-calendar-view="day">Day</button>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class="row">
                                    <div class="col-md-12">
                                        <div id="calendar"></div>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-md-pull-9">
        <?php echo $this->load->view('accounts/accounts_menu', array(), TRUE); ?>
    </div>
</div>