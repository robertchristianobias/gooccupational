<div class="popup-content">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
        <div class="popup-align">
            <?php echo form_open(current_url(), 'id="schedules_form" class="form-horizontal"'); ?>
                <h3 class="h3 text-center">Add Schedule</h3>
                <div class="empty-space col-xs-b30"></div>

                
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-10">
                        <?php echo form_input('schedule_title', set_value('schedule_title'), 'class="form-control"'); ?>
                        <div id="error-schedule_title" class="error"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Description</label>
                    <div class="col-lg-10">
                        <?php echo form_textarea('schedule_description', set_value('schedule_description'), 'class="form-control" rows="15"'); ?>
                        <div id="error-schedule_description" class="error"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Date</label>
                    <div class="col-lg-10">
                        <?php echo form_input('schedule_date', set_value('schedule_date'), 'class="form-control"'); ?>
                        <div id="error-schedule_date" class="error"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Time</label>
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo form_input('schedule_time_from', set_value('schedule_time_from'), 'class="form-control"'); ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo form_input('schedule_time_to', set_value('schedule_time_to'), 'class="form-control"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label class="col-lg-2 control-label">&nbsp;</label>
                    <div class="col-lg-10">
                        <div class="button size-1 style-3">
                            <span class="button-wrapper">
                                <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt="" /></span>
                                <span class="text">Save</span>
                            </span>
                            <input type="submit" name="save" id="save_schedule" />
                        </div>
                    </div>
                    
                </div>
            <?php echo form_close(); ?>
        </div>
        <div class="button-close"></div>
    </div>
</div>