<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Groups_model Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		robertchristianobias@gmail.com
 */
class Groups_model extends BF_Model 
{
	protected $table_name			= 'groups';
	protected $key					= 'id';
	protected $log_user				= FALSE;
	protected $set_created			= FALSE;
	protected $set_modified			= FALSE;
	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'deleted';

	public function get_groups()
	{
		$groups = $this->where('deleted', 0)
					   ->where('id !=', 1)
					   ->order_by('name', 'ASC')
					   ->find_all();

		return $groups;
	}

	public function get_groups_dropdown()
	{
		$groups = $this->where('deleted', 0)
					   ->where('id !=', 1)
					   ->order_by('name', 'ASC')
					   ->format_dropdown('id', 'name', TRUE);

		return $groups;
	}
}