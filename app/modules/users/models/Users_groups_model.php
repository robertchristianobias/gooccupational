<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Users_model Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		robertchristianobias@gmail.com
 */
class Users_groups_model extends BF_Model 
{
	protected $table_name			= 'users_groups';
	protected $key					= 'id';
	protected $log_user				= FALSE;
	protected $set_created			= FALSE;
	protected $set_modified			= FALSE;
	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'deleted';

	public function get_role($user_id)
	{
		$role = $this->join('groups', 'groups.id = users_groups.group_id', 'LEFT')
					 ->where('users_groups.deleted', 0)
					 ->find_by('users_groups.user_id', $user_id);

		return $role;
	}
}