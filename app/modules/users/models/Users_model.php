<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Users_model Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		robertchristianobias@gmail.com
 */
class Users_model extends BF_Model 
{
	protected $table_name			= 'users';
	protected $key					= 'id';
	protected $log_user				= FALSE;
	protected $set_created			= FALSE;
	protected $set_modified			= FALSE;
	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'deleted';

	public function get_user($user_id)
	{
		$user = $this->where('deleted', 0)
					 ->where('active', 1)
					 ->find($user_id);

		return $user;
	}

	public function get_addable_users($user_id)
	{
		$users = $this->where('deleted', 0)
					  ->where('id !=', $user_id)
					  ->find_all();

		return $users;
	}
}