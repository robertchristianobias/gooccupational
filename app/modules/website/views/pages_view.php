<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="h3 col-xs-b5 col-sm-b30"><?php echo $record->page_title; ?></h1>
            </div>
            
        </div>
        <div class="simple-article size-2">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://player.vimeo.com/video/47911018?color=b8cd06&amp;portrait=0"></iframe>
            </div>
            
            <?php echo $record->page_content; ?>

        </div>
        <div class="empty-space col-xs-b30"></div>
    </div>
</div>