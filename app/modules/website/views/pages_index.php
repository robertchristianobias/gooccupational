<div class="section-header">
    <img src="<?php echo site_url('themes/gooccupational/img/avatar-sm.jpg'); ?>" alt="" class="section-header-image">
    <div class="section-header-content">
        <span class="typing">What would you like to see?</span>
        <ul class="nav nav-pills filter show-after-typing" data-filter-list="#posts-list">
            <li class="nav-item"><a href="#" class="button nav-link active" data-filter="*">All</a></li>
            <?php if ( $article_categories ) { ?>
                <?php foreach ( $article_categories as $category ) { ?>
                    <li class="nav-item"><a href="#" class="button nav-link" data-filter=".cat_<?php echo $category->article_category_id; ?>"><?php echo $category->article_category_name; ?></a></li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<div id="posts-list" class="row masonry filter-list">  
    <div class="masonry-sizer col-md-4 col-sm-6 "></div>
    <?php if ( $articles) { ?>
        <?php foreach ( $articles as $article ) { ?>
            <div class="masonry-item col-md-4 col-sm-6 cat_<?php echo $article->article_category_id; ?>">
                <article class="post item">
                    <div class="post-image">
                        <a href="<?php echo site_url('article/'.$article->article_slug); ?>">
                            <img src="<?php echo getenv('CMS_URL') . $article->article_photo; ?>" alt="<?php echo $article->article_title; ?>">
                        </a>
                    </div>
                    <div class="post-content">
                        <h4 class="post-title">
                            <a href="<?php echo site_url('article/'.$article->article_slug); ?>"><?php echo $article->article_title; ?></a>
                        </h4>
                        <ul class="post-meta">
                            <li><a href="<?php echo site_url('article/'.$article->article_slug); ?>"><?php echo date('d F Y', strtotime($article->article_created_on)); ?></a></li>
                            <li><a href="<?php echo site_url('article/'.$article->article_slug); ?>"><?php echo $article->article_category_name; ?></a></li>
                        </ul>
                    </div>
                </article>
            </div>
        <?php } ?>
    <?php } ?>
</div>