<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Pages extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');

		$this->load->model('pages_model');
		$this->load->model('articles/articles_model');
		$this->load->model('articles/articles_categories_model');

		$this->load->library('ion_auth');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
		$data['page_heading'] = 'Home';
		$data['articles']	= $this->articles_model->get_articles();
		$data['article_categories'] = $this->articles_categories_model->get_article_categories();
		
		$this->template->write_view('content', 'pages_index', $data);
		$this->template->render();
	}

	public function view($slug)
	{
		$page = $this->pages_model->get_page($slug);

		$data['page_heading'] = $page->page_title;
		$data['record'] = $page;
		
		$this->template->write_view('content', 'pages_view', $data);
		$this->template->render();
	}
}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */