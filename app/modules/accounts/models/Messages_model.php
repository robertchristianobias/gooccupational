<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Messages_model Class
 *
 * @package		RainCode
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2017, RC MediaPH
 * @link		http://www.rcmediaph.com
 */
class Messages_model extends BF_Model {

	protected $table_name			= 'messages';
	protected $key					= 'message_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'message_created_on';
	protected $created_by_field		= 'message_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'message_modified_on';
	protected $modified_by_field	= 'message_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'message_deleted';
	protected $deleted_by_field		= 'message_deleted_by';

	public $metatag_key				= 'message_metatag_id';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables($user_id)
	{
		$fields = array(
			'message_id',
			'CONCAT(users.first_name, " ", users.last_name)',
			'message_subject'
		);

		return $this->join('users', 'id = message_from_id', 'LEFT')
					->where('message_to_id', $user_id)
					->order_by('message_created_on', 'DESC')
					->datatables($fields);
	}

	public function get_message($message_id)
	{
		$message = $this->find($message_id);

		return $message;
	}

	public function get_sent_datatables($user_id)
	{
		$fields = array(
			'message_id',
			'CONCAT(users.first_name, " ", users.last_name)',
			'message_subject',
			'message_status'
		);

		return $this->join('users', 'id = message_from_id', 'LEFT')
					->where('message_from_id', $user_id)
					->order_by('message_created_on', 'DESC')
					->datatables($fields);
	}
}