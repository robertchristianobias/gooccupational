<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Contacts_model Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Contacts_model extends BF_Model 
{

	protected $table_name			= 'contacts';
	protected $key					= 'contact_id';

	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'contact_created_on';
	protected $created_by_field		= 'contact_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'contact_modified_on';
	protected $modified_by_field	= 'contact_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'contact_deleted';
	protected $deleted_by_field		= 'contact_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function get_datatables($user_id)
	{
		$fields = array(
			'contact_id',
			'CONCAT(users.first_name, " ", users.last_name)',
			'DATE_FORMAT(contact_created_on, "%M %d, %Y")',
			'contact_user_id'
		);

		$callback = array(
            array(
                'method'  => array('Callbacks', 'contacts')
            )
        );

		return $this->join('users', 'id = contact_user_id', 'LEFT')
					->where('contact_deleted', 0)
					->where('contact_by_user_id', $user_id)
					->where('contact_status', 1)
					->order_by('contact_id', 'DESC')
					->datatables($fields, $callback);
	}

	public function get_contacts_by_user($user_id)
	{
		$contacts = $this->join('users', 'id = contact_user_id', 'LEFT')
					 	 ->where('contact_deleted', 0)
						 ->where('contact_by_user_id', $user_id)
						 ->find_all();

		return $contacts;
	}
}