/**
 * @package     Codeigniter
 * @version     1.0
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2016, 
 * @link        http://www.rchristianobias.com
 */
$(function() {

	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"bPaginate": false,
		"bFilter": false,
		"sAjaxSource": site_url + "accounts/messages/sent_datatables",
		"lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
		"pagingType": "full_numbers",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next',
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "asc" ]],
		"aoColumnDefs": [
			{
				"aTargets": [0],
				"sClass": "text-center col-md-1",
			},
			{
				"aTargets": [1],
				"sClass": "col-md-4",
				"mRender": function (data, type, full) {
					html = '';
					if ( full['3'] == 'Unread') {
						html = '<a href="'+site_url+'accounts/messages/view/'+full[0]+'" class="open-popup-ajax"> ' + full['1'] + '</a>';
					} else {
						html = full['1'];
					}

					return html;
				}
			},
			{
				"aTargets": [2],
				"sClass": "col-md-6",
			},
			{
				"aTargets": [3],
				"sClass": "text-center col-md-2",
				"mRender": function (data, type, full) {
					html = '<a title="View Message" href="'+site_url+'accounts/messages/view/'+full[0]+'" class="open-popup-ajax"><i class="fa fa-eye" aria-hidden="true"></i></a>';


					return html;
				}
			},
		]
	});

	// positions the button next to searchbox
	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {        
		// eg. destroys the wysiwyg editor
	});

});