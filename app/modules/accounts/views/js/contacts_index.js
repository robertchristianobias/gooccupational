/**
 * @package     Codeigniter
 * @version     1.0
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2016, 
 * @link        http://www.rchristianobias.com
 */
$(function() {

	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"bPaginate": false,
		"bFilter": false,
		"sAjaxSource": "contacts/datatables",
		"lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
		"pagingType": "full_numbers",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next',
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "asc" ]],
		"aoColumnDefs": [
			{
				"aTargets": [0],
				"sClass": "text-center",
			},
			{
				"aTargets": [4],
				"sClass": "text-center col-md-2",
				"mRender": function (data, type, full) {
					html = '<a href="'+site_url+'accounts/contacts/remove" id="delete" data-id="'+full[0]+'" title="Remove Contact"><span class=""><i class="fa fa-times" aria-hidden="true"></i></span></a>';
					return html;
				}
			},


			
		]
	});

	// positions the button next to searchbox
	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {        
		// eg. destroys the wysiwyg editor
	});

});