<div class="row">
    <div class="col-md-9 col-md-push-3">
        <div class="mail-box">
            <div class="row">
                <aside class="col-md-12">
                    <div class="panel panel-refresh pa-0">
                        <div class="panel-heading pt-20 pb-20 pl-15 pr-15">
                            <div class="pull-left">
                                <h3>Profile of <?php echo $user->first_name . ' ' . $user->last_name; ?></h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                    </div>
                </aside>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-md-pull-9">
        <?php echo $this->load->view('accounts_menu', array(), TRUE); ?>
    </div>
</div>