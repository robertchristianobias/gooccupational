<div class="popup-content">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
        <div class="popup-align">
            <?php echo form_open(current_url(), 'id="contact-invite"'); ?>
            <h3 class="h3">Invite User</h3>
            <div class="empty-space col-xs-b30"></div>
            <!-- <div class="form-group">
                <?php // echo form_input('first_name', set_value('first_name'), 'class="form-control" placeholder="First Name"'); ?>
                <div id="error-first_name" class="error"></div>
            </div>
            <div class="form-group">
                <?php // echo form_input('last_name', set_value('last_name'), 'class="form-control" placeholder="Last Name"'); ?>
                <div id="error-last_name" class="error"></div>
            </div> -->
            <div class="form-group">
                <?php echo form_dropdown('group', $groups, set_value('group'), 'class="SlectBox" placeholder="Select Role"'); ?>
                <div id="error-group" class="error"></div>
            </div>
            <div class="form-group">
                <?php echo form_input('email', set_value('email'), 'class="form-control" placeholder="Enter email"'); ?>
                <div id="error-email" class="error"></div>
            </div>
            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            <div class="row">
                <div class="col-sm-6 col-xs-b10 col-sm-b0">
                    &nbsp;
                </div>
                <div class="col-sm-6 text-right">
                    <div class="button size-2 style-3">
                        <span class="button-wrapper">
                            <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt="" /></span>
                            <span class="text">submit</span>
                        </span>
                        <input type="submit" name="submit_invite" id="submit-invite" />
                    </div>  
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="button-close"></div>
    </div>
</div>