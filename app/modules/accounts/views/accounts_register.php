<div class="popup-content">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
        <div class="popup-align">
            <?php echo form_open(current_url(), 'id="register-form"'); ?>
            <h3 class="h3 text-center"><?php echo $page_heading; ?></h3>
            
            <?php if ($groups) { ?>
            <div class="empty-space col-xs-b30"></div>
            <div class="form-group">
                <label>Role</label>
                <select name="role" class="SlectBox">
                    <option disabled="disabled" selected="selected">Choose Role</option>
                    <?php foreach ($groups as $group) { ?>
                        <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
                    <?php } ?>
                </select>
                <div id="error-role" class="error"></div>
            </div>
            <?php } ?>

            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            <div class="form-group">
                <?php echo form_input('first_name', set_value('first_name'), 'class="simple-input" placeholder="Your firstname" '); ?>
                <div id="error-first_name" class="error"></div>
            </div>
            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            <div class="form-group">
                <?php echo form_input('last_name', set_value('last_name'), 'class="simple-input" placeholder="Your lastname" '); ?>
                <div id="error-last_name" class="error"></div>
            </div>
            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            <div class="form-group">
                <?php echo form_input('email', set_value('email'), 'class="simple-input" placeholder="Your email" '); ?>
                <div id="error-email" class="error"></div>
            </div>
            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            <div class="form-group">
                <input name="password" class="simple-input" type="password" value="" placeholder="Your password" />
                <div id="error-password" class="error"></div>
            </div>
            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="button size-2 style-3">
                        <span class="button-wrapper">
                            <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt="" /></span>
                            <span class="text">submit</span>
                        </span>
                        <input type="submit" name="register" id="register" />
                    </div>  
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="button-close"></div>
    </div>
</div>