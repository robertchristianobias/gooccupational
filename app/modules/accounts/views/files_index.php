<div class="row">
    <div class="col-md-9 col-md-push-3">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <div class="h4">Files</div>                                
                </div>
                <div class="pull-right">
                    <a href="<?php echo site_url('accounts/files/upload'); ?>" class="open-popup-ajax btn btn-sm btn-primary btn-block">
                        <span class="fa fa-plus"></span> Upload Image
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="empty-space col-xs-b30"></div>
        <div class="row">
            <div class="col-md-12 file-sec pt-20">
               
            </div>
        </div>
    </div>
    <div class="col-md-3 col-md-pull-9">
        <?php echo $this->load->view('accounts_menu', array(), TRUE); ?>
    </div>
</div>