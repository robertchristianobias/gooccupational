<div class="h4 col-xs-b10">Navigation</div>
<ul class="categories-menu transparent">
    <li>
        <a href="<?php echo site_url('accounts/newsfeed'); ?>" class="notification">
            <span>Newsfeed</span>
            <!-- <span class="badge">3</span> -->
        </a>
    </li>
    <li>
        <a href="<?php echo site_url('accounts/messages'); ?>">
            <span>Messages</span>
        </a>
        <ul>
            <li><a href="<?php echo site_url('accounts/messages/sent'); ?>">Sent</a></li>
        </ul>
    </li>
    <li>
        <a href="<?php echo site_url('accounts/contacts'); ?>" class="notification">
            <span>Contacts</span>
            <!-- <span class="badge">2</span> -->
        </a>
    </li>
    <li>
        <a href="<?php echo site_url('accounts/documents'); ?>">Documents</a>
        <!-- <div class="toggle"></div>
        <ul>
            <li><a href="<?php echo site_url('accounts/documents/templates'); ?>">Templates</a></li>
        </ul> -->
    </li>
    <li>
        <a href="<?php echo site_url('accounts/schedules'); ?>">Schedules</a>
    </li>
    <li>
        <a href="<?php echo site_url('accounts/profile'); ?>">Profile & Settings</a>
    </li>
</ul>
<div class="empty-space col-xs-b25 col-sm-b50"></div>