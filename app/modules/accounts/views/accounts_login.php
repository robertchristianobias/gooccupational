<div class="popup-content">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
        <div class="popup-align">
            <?php echo form_open(current_url(), 'id="accounts-login"'); ?>
            <h3 class="h3 text-center">
                <?php echo $page_heading; ?><br />
                <small><?php echo $page_subheading; ?></small>
            </h3>
            <div class="empty-space col-xs-b30"></div>
            <input name="email" class="simple-input" type="text" value="" placeholder="Your email" required/>
            <div id="error-email" class="error"></div>
            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            <input name="password" class="simple-input" type="password" value="" placeholder="Enter password" />
            <div id="error-password" class="error"></div>
            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            <div class="row">
                <div class="col-sm-6 col-xs-b10 col-sm-b0">
                    <div class="empty-space col-sm-b5"></div>
                    <a class="simple-link">Forgot password?</a>
                    <div class="empty-space col-xs-b5"></div>
                    <a class="simple-link">register now</a>
                </div>
                <div class="col-sm-6 text-right">
                    <div class="button size-2 style-3">
                        <span class="button-wrapper">
                            <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt="" /></span>
                            <span class="text">submit</span>
                        </span>
                        <input type="submit" name="login" id="login" />
                    </div>  
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="button-close"></div>
    </div>
</div>