
<div class="popup-content">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
        <div class="popup-align">
            <?php echo form_open(current_url(), 'id="contact-form"'); ?>
            <h3 class="h3">Add Contact</h3>
            <div class="empty-space col-xs-b30"></div>
            <div class="form-group">
                <select name="contacts[]" class="form-control select2" multiple="multiple">
                    <?php if( $users ) { ?>
                        <?php foreach($users as $user) { ?>
                            <option value="<?php echo $user->id; ?>"><?php echo $user->email; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
                <div id="error-contacts" class="error"></div>
            </div>
            
            <div class="empty-space col-xs-b10 col-sm-b20"></div>
            <div class="row">
                <div class="col-sm-6 col-xs-b10 col-sm-b0">
                    &nbsp;
                </div>
                <div class="col-sm-6 text-right">
                    <div class="button size-2 style-3">
                        <span class="button-wrapper">
                            <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt="" /></span>
                            <span class="text">submit</span>
                        </span>
                        <input type="submit" name="submit_contact" id="submit_contact" />
                    </div>  
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="button-close"></div>
        
    </div>
</div>
