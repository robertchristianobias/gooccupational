<div class="popup-content">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
        <div class="popup-align">
            <?php echo form_open(current_url(), 'id="message-form" class="form-horizontal"'); ?>
                <h3 class="h3 text-center"><?php echo $message->message_subject; ?></h3>
                <div class="empty-space col-xs-b30"></div>

                <div class="form-group">
                    <div class="simple-article size-4">
                   <?php echo $message->message_content; ?>

                    </div>
                </div>
          
            <?php echo form_close(); ?>
        </div>
        <div class="button-close"></div>
    </div>
</div>