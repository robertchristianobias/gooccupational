<div class="popup-content">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
        <div class="popup-align">
            <?php echo form_open(current_url(), 'id="message-form" class="form-horizontal"'); ?>
                <h3 class="h3 text-center">Compose Message</h3>
                <div class="empty-space col-xs-b30"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">To</label>
                    <div class="col-lg-10">
                        <?php if($contacts) { ?>
                            <select name="message_to_id" class="form-control" class="select2">
                                <?php foreach ( $contacts as $contact ) { ?>
                                    <option value="<?php echo $contact->contact_user_id; ?>"><?php echo $contact->email; ?></option>
                                <?php } ?>
                            </select>
                        <?php } ?>
                        <div id="error-message_to_id" class="error"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Subject</label>
                    <div class="col-lg-10">
                        <?php echo form_input('message_subject', set_value('message_subject'), 'class="form-control"'); ?>
                        <div id="error-message_subject" class="error"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Message</label>
                    <div class="col-lg-10">
                        <?php echo form_textarea('message_content', set_value('message_content'), 'class="textarea_editor form-control" rows="15"'); ?>
                        <div id="error-message_content" class="error"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label class="col-lg-2 control-label">&nbsp;</label>
                    <div class="col-lg-10">
                        <div class="button size-1 style-3">
                            <span class="button-wrapper">
                                <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt="" /></span>
                                <span class="text">send</span>
                            </span>
                            <input type="submit" name="send" id="send-message" />
                        </div>
                    </div>
                    
                </div>
            <?php echo form_close(); ?>
        </div>
        <div class="button-close"></div>
    </div>
</div>