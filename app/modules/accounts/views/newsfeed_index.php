<div class="row">
    <div class="col-md-9 col-md-push-3">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <div class="h4">Newsfeed</div>                                
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="empty-space col-xs-b15 col-sm-b50 col-md-b50"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="streamline user-activity">
                    
                    <?php if ( $newsfeeds ) {  ?>
                        <?php foreach ( $newsfeeds as $newsfeed ) { ?>
                            <div class="sl-item">
                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                    <img class="img-responsive img-circle" src="<?php echo site_url('themes/gooccupational/img/user.png'); ?>" alt="avatar">
                                </div>
                                <div class="sl-content">
                                    <p class="inline-block">
                                        <span class="capitalize-font txt-success mr-5 weight-500">
                                            <a href="<?php echo site_url('accounts/profile/'.encode($newsfeed->id)); ?>">
                                                <?php echo $newsfeed->first_name . ' ' . $newsfeed->last_name; ?>
                                            </a>
                                        </span>
                                        <span><?php echo $newsfeed->newsfeed_message; ?></span>
                                    </p>
                                    <span class="block txt-grey font-12 capitalize-font"> <?php echo time_since(time() - strtotime($newsfeed->newsfeed_created_on)) . ' ' . date('h:i D d M Y', strtotime($newsfeed->newsfeed_created_on)); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <p>nothing to show</p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-md-pull-9">
        <?php echo $this->load->view('accounts_menu', array(), TRUE); ?>
    </div>
</div>
<div class="empty-space col-xs-b15 col-sm-b50 col-md-b50"></div>