<div class="row">
    <div class="col-md-9 col-md-push-3">
        <div class="mail-box">
            <div class="row">
                <aside class="col-md-12">
                    <div class="panel panel-refresh pa-0">
                        <div class="panel-heading pt-20 pb-20 pl-15 pr-15">
                            <div class="pull-left">
                                <div class="h4">Sent Messages</div>                                
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo site_url('accounts/messages/compose'); ?>" class="open-popup-ajax btn btn-success btn-block">
                                    Compose
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body inbox-body pa-0">
                                <div class="table-responsive mb-0">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" id="datatables">
                                        <thead>
                                            <tr>
                                                <th class="all"><?php echo lang('index_id')?></th>
                                                <th class="all"><?php echo lang('index_from_id'); ?></th>
                                                <th class="min-tablet"><?php echo lang('index_subject'); ?></th>
                                                <th class="all"><?php echo lang('index_action'); ?></th>
                                            </tr>
                                        </thead>
                                    </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-md-pull-9">
        <?php echo $this->load->view('accounts_menu', array(), TRUE); ?>
    </div>
</div>