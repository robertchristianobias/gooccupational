<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Newsfeed Class
*
* @package		Codeigniter
* @version		1.1
* @author 		Robert Christian Obias <robertchristianobias@gmail.com>
* @copyright 	Copyright (c) 2015-2016, 
* @link		http://www.rchristianobias.com
*/
class Newsfeed extends MX_Controller 
{
/**
* Constructor
*
* @access public
*
*/
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
		$this->load->library('ion_auth');
		$this->load->library('notifications');

		$this->load->model('newsfeed_model');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
    public function index() 
    {
			// $this->acl->restrict('accounts.newsfeed.list');

			$data['page_heading'] 	 = 'Newsfeed';
			$data['newsfeeds'] 		 = $this->newsfeed_model->get_newsfeeds();
			$data['total_newsfeeds'] = $this->notifications->total_newsfeed();
					
			$this->template->write_view('content', 'newsfeed_index', $data);
			$this->template->render();
    }
}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */