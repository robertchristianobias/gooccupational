<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Accounts extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
		$this->load->library('ion_auth');

		$this->load->model('users/users_model');
		$this->load->model('users/groups_model');

	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index() {}
    
    public function login()
    {
		$data['page_heading'] = 'Login';
		$data['page_subheading'] = 'Please enter your login info';
		
		if ( $_POST )
		{
			$this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
			$this->form_validation->set_rules('password', lang('password'), 'required');

			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'email'		=> form_error('email'),
					'password'	=> form_error('password'),
				);
			}
			else
			{
				$email    = $this->input->post('email');
				$password = $this->input->post('password');
				$remember = FALSE;

				if ($this->ion_auth->login($email, $password, $remember))
				{
					$response['success'] = TRUE;
					$response['message'] = 'Successfully login';
					$response['redirect'] = site_url('accounts/newsfeed');
				}
				else
				{
					$response['success'] = FALSE;
					$response['message'] = 'Incorrect login. Please try again';
				}
			}

			echo json_encode($response);
			exit;
		}
	
		$this->template->set_template('modal');

		$this->template->write_view('content', 'accounts_login', $data);
		$this->template->render();
    }

	public function logout()
	{
		$logout = $this->ion_auth->logout();

		redirect(site_url());
	}

	public function register()
	{
		$data['page_heading'] = 'Register Account';
		$data['groups'] = $this->groups_model->get_groups();

		if ( $_POST )
		{
			$this->form_validation->set_rules('role', lang('role'), 'required');
			$this->form_validation->set_rules('first_name', lang('first_name'), 'required');
			$this->form_validation->set_rules('last_name', lang('last_name'), 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email.id.0.deleted]');
			$this->form_validation->set_rules('password', lang('password'), 'required');

			$this->form_validation->set_message('required', 'This field is required');
			$this->form_validation->set_message('is_unique', '%s already exist');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'role'		 => form_error('role'),
					'first_name' => form_error('first_name'),
					'last_name'  => form_error('last_name'),		
					'email'		 => form_error('email'),
					'password'	 => form_error('password'),
				);
			}
			else
			{
				// add to users
				$username = $this->input->post('email');
                $password = $this->input->post('password');
				$email    = $username;
				$role     = $this->input->post('role');

                $additional_data = array(
                    'first_name'	=> $this->input->post('first_name'),
                    'last_name'		=> $this->input->post('last_name'),
                    'company'		=> '',
                    'phone'			=> '',
                    'photo'			=> 'img/user1.png',
                );
				$groups = array($role);
				
				$user_id = $this->ion_auth->register($username, $password, $email, $additional_data, $groups);
				
				if ( $user_id )
				{
					$response['success'] = TRUE;
					$response['message'] = 'Successfully registered the user';
					$response['redirect'] = site_url();
				}
			}

			echo json_encode($response); exit;
		}

		$this->template->set_template('modal');
		$this->template->write_view('content', 'accounts_register', $data);
		$this->template->render();
	}

	public function profile($user_id)
	{
		$user_id = decode($user_id);
		$user = $this->users_model->get_user($user_id);

		if ( !$user)
		{
			show_404();
		}

		$data['page_heading'] = 'Profile';
		$data['user'] = $user;

		$this->template->write_view('content', 'accounts_profile', $data);
		$this->template->render();
	}
}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */