<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Contacts Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Contacts extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
        $this->load->library('ion_auth');
        
        $this->load->model('contacts_model');
        $this->load->model('newsfeed_model');

        $this->load->model('users/users_model');
        $this->load->model('users/groups_model');
        $this->load->model('users/users_groups_model');

        $this->lang->load('contacts');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
    public function index() 
    {
        $data['page_heading'] = lang('contacts');
  
        $this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
        $this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');
        
        $this->template->add_js(module_js('accounts', 'contacts_index'), 'embed');

		$this->template->write_view('content', 'contacts_index', $data);
		$this->template->render();
    }

    public function datatables()
	{
		echo $this->contacts_model->get_datatables($this->session->userdata('user_id'));
    }

    
    public function invite()
    {
        $data['page_heading'] = lang('invite');
        $data['groups']  = $this->groups_model->get_groups_dropdown();
        
        if ( $_POST )
        {
            $this->form_validation->set_rules('group', lang('group'), 'required');
            $this->form_validation->set_rules('email', lang('email'), 'required|valid_email');

            $this->form_validation->set_message('required', 'This field is required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
                    'group'     => form_error('group'),
					'email'		=> form_error('email'),
				);
			}
			else
			{
                // add to users
                $group = $this->input->post('group');
                $username = $this->input->post('email');
                $password = '';
                $email    = $username;
                $additional_data = array(
                    'first_name'	=> $this->input->post('first_name'),
                    'last_name'		=> $this->input->post('last_name'),
                    'company'		=> '',
                    'phone'			=> '',
                    'photo'			=> 'img/user1.png',
                );

                $groups  = array($group);
                $user_id = $this->ion_auth->register($username, $password, $email, $additional_data, $groups);
        
                if ($user_id)
                {
                    // add to contacts
                    $contacts_data = array(
                        'contact_user_id'    => $user_id,
                        'contact_by_user_id' => $this->session->userdata('user_id'),
                        'contact_status'     => 2
                    );

                    $insert = $this->contacts_model->insert($contacts_data);

                    // send an invite email
                    if ( $insert )
                    {
                        $user_id = $this->session->userdata('user_id');
                        $user = $this->users_model->find($user_id);

                        $this->load->library('email');

                        $this->email->from($user->email, $user->first_name . ' ' . $user->last_name);
                        $this->email->to($email);

                        $this->email->subject('You have been invited');
                        $this->email->message('Click <a href="'.site_url('accounts/register/'.encode($user_id)).'">here</a> to register');

                        $send = $this->email->send();

                        if ( $send )
                        {
                            $response['success'] = TRUE;
                            $response['message'] = 'User Invited';
                            $response['redirect'] = site_url('accounts/contacts');
                        }
                        else
                        {
                            $response['success'] = FALSE;
                            $response['message'] = 'Email not sent. Please check if email is configured correctly.';
                        }
                    }
                }
                else
                {
                    $response['success'] = FALSE;
                    $response['message'] = 'Failed to invite user. Please try again';
                }
            }

            echo json_encode($response); exit;

        }
        $this->template->set_template('modal');
		$this->template->write_view('content', 'contacts_invite', $data);
		$this->template->render();
    }

	public function form($action)
	{
        $data['page_heading'] = $action . ' ' . lang('contact');
        $data['users'] = $this->users_model->get_addable_users($this->session->userdata('user_id'));

        $this->template->set_template('modal');
        
        //$this->template->add_css('components/select2/dist/css/select2.css');
        //$this->template->add_js('components/select2/dist/js/select2.full.min.js');
        
		$this->template->write_view('content', 'contacts_form', $data);
		$this->template->render();
    }
    
    public function process_add_contact()
    {
        $this->form_validation->set_rules('contacts[]', lang('contacts'), 'required');

        $this->form_validation->set_message('required', 'This field is required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        
        if ($this->form_validation->run($this) == FALSE)
        {
            $response['success'] = FALSE;
            $response['message'] = lang('validation_error');
            $response['errors'] = array(					
                'contacts'	=> form_error('contacts[]'),
            );
        }
        else
        {
            $contacts = $this->input->post('contacts');

            // check if already a contact
            $is_contact = $this->contacts_model->where_in('contact_user_id', $contacts)
                               ->where('contact_deleted', 0)
                               ->where('contact_by_user_id', $this->session->userdata('user_id') )
                               ->find_all();

            if ( $is_contact ) // contact already exists
            {
                $response['success'] = FALSE;
                $response['message'] = 'Contact already exists';
            }
            else
            {
                $contacts_data = array();
                if ( $contacts )
                {
                    foreach ($contacts as $contact )
                    {
                        $contacts_data[] = array(
                            'contact_user_id'    => $contact,
                            'contact_by_user_id' => $this->session->userdata('user_id'),
                            'contact_status'     => 1
                        );
                    }
    
                    $this->contacts_model->insert_batch($contacts_data);
                }

                // add to newsfeed
                $newsfeed_data = array(
                    'newsfeed_user_id' => $this->session->userdata('user_id'),
                    'newsfeed_message' => 'added a new contact/s'
                );

                $this->newsfeed_model->insert($newsfeed_data);
    
                $response['success'] = TRUE;
                $response['title']   = 'Add Contact';
                $response['message'] = 'User/s added';
                $response['redirect'] = site_url('accounts/contacts');
            }
        }

        echo json_encode($response); exit;
    }

    public function remove()
    {
        pr($_POST);
        exit;
    }


}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */