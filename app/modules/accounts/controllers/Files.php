<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Files Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Files extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
		$this->load->library('ion_auth');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
    public function index() 
    {
        $data['page_heading'] = 'Files';

		$this->template->write_view('content', 'files_index', $data);
		$this->template->render();
    }

    public function upload()
    {
        $data['page_heading'] = 'Upload File';

        $this->template->set_template('modal');
        $this->template->write_view('content', 'files_upload', $data);
        $this->template->render();
    }

}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */