<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Messages Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Messages extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
		$this->load->library('ion_auth');

		$this->load->model('contacts_model');
		$this->load->model('messages_model');

		$this->lang->load('messages');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index() 
	{
		$data['page_heading'] = 'Messages';
		
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');

		$this->template->add_js(module_js('accounts', 'messages_index'), 'embed');

		$this->template->write_view('content', 'messages_index', $data);
		$this->template->render();
	}

	public function datatables()
	{
		echo $this->messages_model->get_datatables($this->session->userdata('user_id'));
	}

	public function compose()
	{
		$data['page_heading'] = 'Compose Message';
		$data['contacts'] = $this->contacts_model->get_contacts_by_user($this->session->userdata('user_id'));

		if ( $_POST )
		{
			$this->form_validation->set_rules('message_to_id', lang('message_to_id'), 'required');
			$this->form_validation->set_rules('message_subject', lang('message_subject'), 'required');
			$this->form_validation->set_rules('message_content', lang('message_content'), 'required');

			$this->form_validation->set_message('required', 'This field is required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'message_to_id'		=> form_error('message_to_id'),
					'message_subject'	=> form_error('message_subject'),
					'message_content'	=> form_error('message_content')
				);
			}
			else
			{
				$data = array(
					'message_from_id'	=> $this->session->userdata('user_id'),
					'message_to_id'		=> $this->input->post('message_to_id'),
					'message_subject'	=> $this->input->post('message_subject'),
					'message_content'	=> $this->input->post('message_content'),
					'message_status'	=> 'Unread'
				);

				// insert to messages
				$this->messages_model->insert($data);

				$response['success'] = TRUE;
				$response['message'] = 'Message sent';
			}

			echo json_encode($response); exit;
		}

		$this->template->set_template('modal');

		$this->template->write_view('content', 'messages_compose', $data);
		$this->template->render();
	}

	public function view($message_id)
	{
		$data['page_heading'] = 'View Message';
		$data['message'] = $this->messages_model->get_message($message_id);
	
		$this->template->set_template('modal');

		$this->template->write_view('content', 'messages_view', $data);
		$this->template->render();
	}

	public function sent()
	{
		$data['page_heading'] = 'Sent Messages';
		
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');

		$this->template->add_js(module_js('accounts', 'messages_sent_index'), 'embed');

		$this->template->write_view('content', 'messages_sent_index', $data);
		$this->template->render();
	}

	public function sent_datatables()
	{
		echo $this->messages_model->get_sent_datatables($this->session->userdata('user_id'));
	}

}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */