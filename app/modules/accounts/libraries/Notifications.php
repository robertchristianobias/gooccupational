<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications
{
	public function __construct()
	{
		$this->CI =& get_instance();

        $this->CI->load->model('accounts/newsfeed_model');
    }
    
    public function total_newsfeed()
    {
        $newsfeeds = $this->CI->newsfeed_model->get_newsfeeds();

        return count($newsfeeds);
    }

}