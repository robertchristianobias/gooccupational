<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Contactst';

// Labels
$lang['contacts']				= 'Contacts';
$lang['contact'] = 'Contact';
$lang['add_contact'] = 'Add Contact';

// Buttons
$lang['button_add']					= 'Add Page';
$lang['button_update']				= 'Save Changes';
$lang['button_delete']				= 'Delete Page';
$lang['button_edit_this']			= 'Edit This';

$lang['index_id'] = 'ID';
$lang['index_name'] = 'Name';
$lang['index_role'] = 'Role';
$lang['index_date_added'] = 'Date Added';
$lang['index_action'] = 'Action';

