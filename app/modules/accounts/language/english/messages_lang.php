<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Messages';

$lang['index_id'] = 'ID';
$lang['index_from_id'] = 'From';
$lang['index_subject'] = 'Subject';
$lang['index_action'] = 'Action';



