
<div class="empty-space col-xs-b15 col-sm-b30"></div>
<div class="breadcrumbs">
    <a href="#">home</a>
    <a href="#">articles</a>
</div>
<div class="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
<div class="text-center">
    <div class="h2">saved articles</div>
    <div class="title-underline center"><span></span></div>
</div>
        
<div class="empty-space col-xs-b35 col-md-b70"></div>
<div class="row">
    <div class="col-md-9">
        <?php if ( $articles ) { ?>
            <?php foreach ( $articles as $article ) { ?>
                <div class="blog-shortcode style-3">
                    <a class="preview rounded-image simple-mouseover" href="#"><img class="rounded-image" src="<?php echo site_url('themes/gooccupational/img/thumbnail-77.jpg'); ?>" alt=""></a>
                    <div class="date">
                        <span><?php echo date('d', strtotime($article->article_created_on)); ?></span> <?php echo date('M', strtotime($article->article_created_on)); ?> / <?php echo date('y', strtotime($article->article_created_on)); ?> 
                    </div>
                    <div class="content">
                        <div class="blog-comments"><i class="fa fa-comment-o" aria-hidden="true"></i> 5 &nbsp;&nbsp;&nbsp;<i class="fa fa-heart-o" aria-hidden="true"></i> 20</div>
                        <h4 class="title h4">
                            <a href="<?php echo site_url('article/' . $article->article_slug); ?>">
                                <?php echo $article->article_title; ?>
                            </a>
                        </h4>
                        <div class="description-article simple-article size-2">
                            <?php echo $article->article_content; ?>
                        </div>
                        <a class="button size-2 style-3" href="<?php echo site_url('article/' . $article->article_slug); ?>">
                            <span class="button-wrapper">
                                <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt=""></span>
                                <span class="text">learn more</span>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="empty-space col-xs-b35 col-md-b70"></div>
            <?php } ?>
        <?php } ?>

        

    </div>
    <div class="col-md-3">
        <div class="single-line-form">
            <input class="simple-input small" type="text" value="" placeholder="Enter keyword">
            <div class="submit-icon">
                <i class="fa fa-search" aria-hidden="true"></i>
                <input type="submit">
            </div>
        </div>
        <div class="empty-space col-xs-b25 col-sm-b50"></div>

        <div class="h4 col-xs-b10">categories</div>
        <ul class="categories-menu transparent">
            <li>
                <a href="#">laptops &amp; computers</a>
            </li>
            <li>
                <a href="#">video &amp; photo cameras</a>
            </li>
            <li>
                <a href="#">smartphones</a>
            </li>
            <li>
                <a href="#">tv &amp; audio</a>
            </li>
            <li>
                <a href="#">gadgets</a>
            </li>
        </ul>

        <div class="empty-space col-xs-b25 col-sm-b50"></div>

        <div class="h4 col-xs-b25">Popular Tags</div>
        <div class="blog-shortcode style-2">
            <a href="#" class="preview rounded-image simple-mouseover"><img class="rounded-image" src="img/thumbnail-73.jpg" alt=""></a>
            <div class="title h6"><a href="#">Phasellus rhoncus in nunc sit</a></div>
            <div class="description simple-article size-1 grey uppercase">apr 07 / 15 &nbsp;&nbsp;<a class="color" href="#">john wick</a> &nbsp;&nbsp;<a class="color" href="#">Gadgets</a></div>
        </div>
        <div class="empty-space col-xs-b25"></div>
        <div class="blog-shortcode style-2">
            <a href="#" class="preview rounded-image simple-mouseover"><img class="rounded-image" src="img/thumbnail-74.jpg" alt=""></a>
            <div class="title h6"><a href="#">Fusce viverra id diam nec</a></div>
            <div class="description simple-article size-1 grey uppercase">apr 07 / 15 &nbsp;&nbsp;<a class="color" href="#">john wick</a> &nbsp;&nbsp;<a class="color" href="#">Gadgets</a></div>
        </div>

        <div class="empty-space col-xs-b25 col-sm-b50"></div>

        <div class="h4 col-xs-b25">Popular Tags</div>
        <div class="tags light clearfix">
            <a class="tag">headphoness</a>
            <a class="tag">accessories</a>
            <a class="tag">new</a>
            <a class="tag">wireless</a>
            <a class="tag">cables</a>
            <a class="tag">devices</a>
            <a class="tag">gadgets</a>
            <a class="tag">brands</a>
            <a class="tag">replacements</a>
            <a class="tag">cases</a>
            <a class="tag">cables</a>
            <a class="tag">professional</a>
        </div>

        <div class="empty-space col-xs-b25 col-sm-b50"></div>

        <div class="h4 col-xs-b25">youtube chanel</div>

        <div class="swiper-container swiper-swiper-unique-id-4 initialized swiper-container-horizontal" id="swiper-unique-id-4">
            <div class="swiper-button-prev hidden swiper-button-prev-swiper-unique-id-4 swiper-button-disabled"></div>
            <div class="swiper-button-next hidden swiper-button-next-swiper-unique-id-4"></div>
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-slide-active" style="width: 480px;">
                    <div class="blog-shortcode style-2">
                        <a class="preview rounded-image">
                            <img class="rounded-image" src="img/thumbnail-75.jpg" alt="">
                            <span class="play-button open-video" data-src="https://www.youtube.com/embed/kQT2y3UiosQ?autoplay=1&amp;loop=1&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;color=white&amp;theme=light&amp;wmode=transparent"></span>
                        </a>
                        <div class="title h6"><a href="#">Phasellus rhoncus in nunc sit</a></div>
                        <div class="description simple-article size-1 grey">Duis fringilla felis et faucibus semper. Aliquam gravida elit et lectus viverra porta.</div>
                    </div>
                </div>
                <div class="swiper-slide swiper-slide-next" style="width: 480px;">
                    <div class="blog-shortcode style-2">
                        <a class="preview rounded-image">
                            <img class="rounded-image" src="img/thumbnail-76.jpg" alt="">
                            <span class="play-button open-video" data-src="https://www.youtube.com/embed/kQT2y3UiosQ?autoplay=1&amp;loop=1&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;color=white&amp;theme=light&amp;wmode=transparent"></span>
                        </a>
                        <div class="title h6"><a href="#">Phasellus rhoncus in nunc sit</a></div>
                        <div class="description simple-article size-1 grey">Duis fringilla felis et faucibus semper. Aliquam gravida elit et lectus viverra porta.</div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination relative-pagination-small swiper-pagination-swiper-unique-id-4 swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>
        </div>


        <div class="empty-space col-xs-b25 col-sm-b50"></div>

    </div>
</div>
        