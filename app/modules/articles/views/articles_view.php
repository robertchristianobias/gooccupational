<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-sm-8">
                <div class="simple-article size-1 grey uppercase col-xs-b10"><?php echo date('M d / y', strtotime($record->article_created_on) ); ?> </div>
                <h1 class="h3 col-xs-b5 col-sm-b30">
                    <?php echo $record->article_title; ?>
                </h1>
            </div>
        </div>
        <div class="simple-article size-2">
           
            <?php echo $record->article_content; ?>
        </div>
        <div class="empty-space col-xs-b30"></div>
    </div>
    <div class="col-md-3">
        <div class="h4 col-xs-b10">categories</div>
        <?php if ($articles_categories) { ?>
            <ul class="categories-menu transparent">
                <?php foreach ( $articles_categories as $category ) { ?>
                <li>
                    <a href="#"><?php echo $category->article_category_name; ?></a>
                </li>
                <?php } ?>
            </ul>
        <?php } ?>
        <div class="empty-space col-xs-b25 col-sm-b50"></div>
    </div>
</div>