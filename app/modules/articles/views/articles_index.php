
<div class="empty-space col-xs-b15 col-sm-b30"></div>
<div class="breadcrumbs">
    <a href="<?php echo site_url(); ?>">home</a>
    <a href="<?php echo site_url('articles'); ?>">articles</a>
</div>
<div class="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
<div class="text-center">
    <div class="h2">articles</div>
    <div class="title-underline center"><span></span></div>
</div>
        
<div class="empty-space col-xs-b35 col-md-b70"></div>
<div class="row">
    <div class="col-md-9">
        <?php if ( $articles ) { ?>
            <?php foreach ( $articles as $article ) { ?>
                <div class="blog-shortcode style-3">
                    <a class="preview rounded-image simple-mouseover" href="<?php echo site_url('article/'.$article->article_slug); ?>">
                        <img class="rounded-image" src="<?php echo site_url('themes/gooccupational/img/thumbnail-77.jpg'); ?>" alt="">
                    </a>
                    <div class="date">
                        <span><?php echo date('d', strtotime($article->article_created_on)); ?></span> <?php echo date('M', strtotime($article->article_created_on)); ?> / <?php echo date('y', strtotime($article->article_created_on)); ?> 
                    </div>
                    <div class="content">
                        <div class="blog-comments"><i class="fa fa-comment-o" aria-hidden="true"></i> 5 &nbsp;&nbsp;&nbsp;<i class="fa fa-heart-o" aria-hidden="true"></i> 20</div>
                        <h4 class="title h4">
                            <a href="<?php echo site_url('article/' . $article->article_slug); ?>">
                                <?php echo $article->article_title; ?>
                            </a>
                        </h4>
                        <div class="description-article simple-article size-2">
                            <?php echo $article->article_content; ?>
                        </div>
                        <a class="button size-2 style-3" href="<?php echo site_url('article/' . $article->article_slug); ?>">
                            <span class="button-wrapper">
                                <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt=""></span>
                                <span class="text">learn more</span>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="empty-space col-xs-b35 col-md-b70"></div>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="col-md-3">
        <div class="h4 col-xs-b10">categories</div>
        <?php if ($articles_categories) { ?>
            <ul class="categories-menu transparent">
                <?php foreach ( $articles_categories as $category ) { ?>
                <li>
                    <a href="#"><?php echo $category->article_category_name; ?></a>
                </li>
                <?php } ?>
            </ul>
        <?php } ?>
        <div class="empty-space col-xs-b25 col-sm-b50"></div>
    </div>
</div>
        