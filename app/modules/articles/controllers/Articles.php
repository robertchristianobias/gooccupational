<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Articles Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Articles extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public $user;
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');

		$this->load->model('ion_auth_model');

		$this->user = $this->ion_auth_model->user()->row();

		$this->load->model('articles_model');
		$this->load->model('articles_categories_model');

		$this->load->library('ion_auth');

	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	public function index()
	{
        $data['page_heading'] = 'Articles';
		$data['articles'] = $this->articles_model->get_articles();
		$data['articles_categories'] = $this->articles_categories_model->get_article_categories();
		
		$this->template->write_view('content', 'articles_index', $data);
		$this->template->render();
    }
    
    public function view_article($slug)
    {   
        $record = $this->articles_model->get_article($slug);

        $data['page_heading'] = 'Article ' . $record->article_title; 
		$data['record'] = $record;
		$data['articles_categories'] = $this->articles_categories_model->get_article_categories();

        $this->template->write_view('content', 'articles_view', $data);
		$this->template->render();
	}
	
	public function saved()
	{
		if ( $this->user )
		{
			$data['page_heading'] = 'Articles Saved';
			$data['articles'] = array();

			$this->template->write_view('content', 'articles_saved', $data);
			$this->template->render();
		}
		else
		{
			$data['page_heading'] = 'Login';
			$data['page_subheading'] = 'Please login to access this function';

			$this->template->set_template('modal');

			$this->template->write_view('content', 'accounts/accounts_login', $data);
			$this->template->render();
		}
	}
}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */