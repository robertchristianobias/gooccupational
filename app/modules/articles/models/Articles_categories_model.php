<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Articles_categories_model Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Articles_categories_model extends BF_Model 
{

	protected $table_name			= 'article_categories';
	protected $key					= 'article_category_id';

	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'article_category_created_on';
	protected $created_by_field		= 'article_category_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'article_category_modified_on';
	protected $modified_by_field	= 'article_category_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'article_category_deleted';
	protected $deleted_by_field		= 'article_category_deleted_by';

	public $metatag_key				= 'article_category_metatag_id';

	public function get_article_categories()
	{
		$articles = $this->where('article_category_deleted', 0)
						 ->order_by('article_category_name', 'ASC')
						 ->find_all();
			
		return $articles;
	}
}