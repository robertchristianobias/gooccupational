<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Articles_model Class
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */
class Articles_model extends BF_Model 
{

	protected $table_name			= 'articles';
	protected $key					= 'article_id';

	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'article_created_on';
	protected $created_by_field		= 'article_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'article_modified_on';
	protected $modified_by_field	= 'article_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'article_deleted';
	protected $deleted_by_field		= 'article_deleted_by';

	public $metatag_key				= 'article_metatag_id';

	public function get_articles()
	{
		$articles = $this->join('article_categories', 'article_categories.article_category_id = articles.article_category_id', 'LEFT')
						 ->where('article_deleted', 0)
						 ->where('article_status', 'Published')
						 ->order_by('article_created_on', 'DESC')
						 ->find_all();
			
		return $articles;
	}

	public function get_article($slug)
	{
		$article = $this->where('article_deleted', 0)
						->where('article_status', 'Published')
						->find_by('article_slug', $slug);
						
		return $article;
	}
}