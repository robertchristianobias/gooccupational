<div class="popup-content">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
        <div class="popup-align">
        <?php echo form_open(current_url(), 'id="message-form" class="form-horizontal"'); ?>
            <h3 class="h3 text-center"><?php echo $page_heading; ?></h3>
            <div class="empty-space col-xs-b30"></div>
                <div class="form-group">
                    <h4 class="h4">Shareable link</h4>
                    <?php echo form_input('', site_url('files/' . random_string('alnum', 40).'-'.$document_id), 'class="form-control"'); ?>
                </div>
                <hr />
                <div class="form-group">
                    <h4 class="h4">Contacts</h4>
                    <select name="contacts[]" class="form-control select2" multiple="multiple">
                        <?php if( $contacts ) { ?>
                            <?php foreach($contacts as $user) { ?>
                                <option value="<?php echo $user->id; ?>"><?php echo $user->email; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <div id="error-contacts" class="error"></div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="button size-2 style-3">
                            <span class="button-wrapper">
                                <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt=""></span>
                                <span class="text">done</span>
                            </span>
                            <input type="submit" name="file-shared" id="file-shared">
                        </div>  
                    </div>
                </div>

            <?php echo form_close(); ?>
        </div>
        
    </div>
</div>