<?php echo form_open(current_url(), 'id="document_form"'); ?>
    <div class="row">
        <div class="col-md-9 col-md-push-3">
            <div class="mail-box">
                <div class="row">
                    <aside class="col-md-12">
                        <div class="panel panel-refresh pa-0">
                            <div class="panel-heading pt-20 pb-20 pl-15 pr-15">
                                <div class="pull-left">
                                    <div class="h4"><?php echo $page_heading; ?></div>                                
                                </div>
                                <div class="pull-right">
                                <div class="button size-1 style-3">
                                    <span class="button-wrapper">
                                        <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt=""></span>
                                        <span class="text">submit</span>
                                    </span>
                                    <input type="submit" name="submit_document" id="submit_document">
                                </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body inbox-body pa-0">
                                    <div class="row m10">
                                        <div class="col-md-12">
                                            <?php echo form_dropdown('document_template_id', $templates, set_value('document_template_id'), 'class="simple-input" id="document_template_id" '); ?>
                                            <div id="error-document_template_id" class="error"></div>
                                            <div class="empty-space col-xs-b20"></div>
                                        </div>
                                    </div>
                                    <div class="row m10">
                                        <div class="col-md-12">
                                            <?php echo form_input('document_name', set_value('document_name'), 'class="simple-input" placeholder="Name"'); ?>
                                            <div id="error-document_name" class="error"></div>
                                            <div class="empty-space col-xs-b20"></div>
                                        </div>
                                    </div>
                                    <div class="row m10">
                                        <div class="col-md-12">
                                            <?php echo form_textarea('document_content', set_value('document_content'), 'class="simple-textarea ckeditor" placeholder="Content"'); ?>
                                            <div id="error-document_content" class="error"></div>
                                            <div class="empty-space col-xs-b20"></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-pull-9">
            <?php echo $this->load->view('accounts/accounts_menu', array(), TRUE); ?>
        </div>
    </div>
<?php echo form_close(); ?>