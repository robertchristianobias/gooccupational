<div class="row">
    <div class="col-md-9 col-md-push-3">
        <div class="mail-box">
            <div class="row">
                <aside class="col-md-12">
                    <div class="panel panel-refresh pa-0">
                        <div class="panel-heading pt-20 pb-20 pl-15 pr-15">
                            <div class="pull-left">
                                <div class="h4"><?php echo $page_heading; ?></div>                                
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo site_url('accounts/documents/templates/form/add'); ?>" class="btn btn-success btn-block">
                                    <?php echo lang('button_add'); ?>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body inbox-body pa-0">
                                <div class="table-responsive mb-0">
                                    <table class="table table-inbox table-hover mb-0">
                                        <tbody>
                                            <?php if ( $templates ) { ?>
                                                <?php foreach ( $templates as $template ) { ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $template->documentation_template_title; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <a href="<?php echo site_url('documents/templates/share_template/' . $template->documentation_template_id); ?>" class="open-popup-ajax">
                                                                <i class="fa fa-share inline-block mr-15 font-16"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-md-pull-9">
        <?php echo $this->load->view('accounts/accounts_menu', array(), TRUE); ?>
    </div>
</div>