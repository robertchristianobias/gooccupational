/**
 * @package     Codeigniter
 * @version     1.0
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2016, 
 * @link        http://www.rchristianobias.com
 */
$(function() {

	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"bPaginate": false,
		"bFilter": false,
		"sAjaxSource": site_url + "documents/datatables",
		"lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
		"pagingType": "full_numbers",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next',
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "asc" ]],
		"aoColumnDefs": [
			{
				"aTargets": [0],
				"sClass": "text-center col-md-1",
			},
			{
				"aTargets": [1],
				"sClass": "col-md-9"
			},
	
			{
				"aTargets": [2],
				"sClass": "text-center col-md-2",
				"mRender": function (data, type, full) {
					html = '<a title="Share File" href="'+site_url + 'documents/share/'+full[0]+'" class="open-popup-ajax"><i class="fa fa-share-alt" aria-hidden="true"></i></a>';


					return html;
				}
			},
		]
	});

	// positions the button next to searchbox
	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {        
		// eg. destroys the wysiwyg editor
	});

});