/**
 * @package     Codeigniter
 * @version     1.0
 * @author      Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright   Copyright (c) 2016, 
 * @link        http://www.rchristianobias.com
 */
$(function() {

	$('#submit_form').click(function(e) {

		e.preventDefault();
		
		var $form = $('#referral-form');
		var $data = $form.serializeArray();
		var $url = $(this).attr('action');

		$.post($url, $data, function(data) {
			var $response = $.parseJSON(data);
					
			if ($response.success === false) {
				swal("Opps!", $response.message, "error");
				for (var form_name in $response.errors) {
					$('#error-' + form_name).html($response.errors[form_name]);
				}
			} else {
				swal("Saved success", $response.message, "success");
			}
		});
	});

});