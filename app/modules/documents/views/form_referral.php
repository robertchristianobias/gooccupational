<div class="row">
    <div class="col-md-12">
        <div class="h2"><?php echo $page_heading; ?></div>
        <br /><br />

        <?php echo form_open(current_url(), 'id="referral-form"'); ?>
            <div class="row">
                <div class="col-md-6">
                    <h3 class="h3">General Information</h4>
                    <hr />
                    <div class="form-group">
                        <h5 class="h5">Child's Name</h5>
                        <?php echo form_input('form_childs_name', set_value('form_childs_name'), 'class="simple-input"'); ?>
                        <div id="error-form_childs_name" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Date of Birth</h5>
                        <?php echo form_input('form_date_of_birth', set_value('form_date_of_birth'), 'class="simple-input"'); ?>
                        <div id="error-form_date_of_birth" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Gender</h5>
                        <br />
                        <?php 
                            $gender_arr = create_dropdown('array', 'Male,Female');
                        ?>
                        <?php foreach ( $gender_arr as $gender ) { ?>
                            <label class="checkbox-entry radio">
                                <input type="radio" name="form_gender" value="<?php echo $gender; ?>" ><span><?php echo $gender; ?></span>
                            </label>
                            <div class="empty-space col-xs-b20"></div>
                        <?php } ?>
                        <div id="error-form_gender" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">NRIC / Passport / FIN</h5>
                        <?php echo form_input('form_passport', set_value('form_passport'), 'class="simple-input"'); ?>
                        <div id="error-form_passport" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Citizenship</h5>
                        <?php echo form_input('form_citizenship', set_value('form_citizenship'), 'class="simple-input"'); ?>
                        <div id="error-form_citizenship" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Home address(es)</h5>
                        <?php echo form_input('form_address', set_value('form_address'), 'class="simple-input"'); ?>
                        <div id="error-form_address" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Contact Number</h5>
                        <?php echo form_input('form_contact', set_value('form_contact'), 'class="simple-input"'); ?>
                        <div id="error-form_contact" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Ethnicity (optional)</h5>
                        <?php echo form_input('form_ethnicity', set_value('form_ethnicity'), 'class="simple-input"'); ?>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Languages Preferred</h5>
                        <br /><br />
                        <?php
                            $lang_arr = create_dropdown('array', 'English  / Little English mixed with (add languages or dialects),No English / Only in (add languages or dialects),English instructions can be translated fluently by caregiver');
                        ?>
                        <?php foreach ( $lang_arr as $lang ) { ?>
                            <label class="checkbox-entry">
                                <input name="form_language_preferred[]" type="checkbox"><span> <?php echo $lang; ?></span>
                            </label>
                            <div class="empty-space col-xs-b20"></div>
                        <?php } ?>
                        <div id="error-form_language_preferred[]" class="error"></div>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <h3 class="h3">Medical Information</h3>
                    <small>All information is confidential and will not be shared with other parties without the client’s consent</small>
                    <hr />
                    <div class="form-group">
                        <h5 class="h5">Does the child have a medical diagnosis? (e.g. global developmental delay, ASD, cerebral palsy learning disabilities, intellectual disabilities, genetic disorders, life limiting conditions)</h5>
                        <?php echo form_textarea('form_medical_diagnosis', set_value('form_medical_diagnosis'), 'class="simple-input"'); ?>
                        <div id="error-form_medical_diagnosis" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Medical History</h5>
                
                        <?php echo form_textarea('form_medical_history', set_value('form_medical_history'), 'class="simple-input"'); ?>
                        <div id="error-form_medical_history" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Medications</h5>
                
                        <?php echo form_textarea('form_medications', set_value('form_medications'), 'class="simple-input"'); ?>
                        <div id="error-form_medications" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Details of persons with parental responsibility</h5>
                        <br /><br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5 class="h5">Name</h5>
                                    <?php echo form_input('form_parental_name_1', set_value('form_parental_name_1'), 'class="simple-input"'); ?>
                                    <div id="error-form_parental_name_1" class="error"></div>
                                </div>
                                <div class="form-group">
                                    <h5 class="h5">Relationship to child</h5>
                                    <?php echo form_input('form_parental_relationship_1', set_value('form_parental_relationship_1'), 'class="simple-input"'); ?>
                                    <div id="error-form_parental_relationship_1" class="error"></div>
                                </div>
                                <div class="form-group">
                                    <h5 class="h5">Contact Number</h5>
                                    <?php echo form_input('form_parental_contact_1', set_value('form_parental_contact_1'), 'class="simple-input"'); ?>
                                    <div id="error-form_parental_contact_1" class="error"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5 class="h5">Name</h5>
                                    <?php echo form_input('form_parental_name_2', set_value('form_parental_name_2'), 'class="simple-input"'); ?>
                                </div>
                                <div class="form-group">
                                    <h5 class="h5">Relationship to child</h5>
                                    <?php echo form_input('form_parental_relationship_2', set_value('form_parental_relationship_2'), 'class="simple-input"'); ?>
                                </div>
                                <div class="form-group">
                                    <h5 class="h5">Contact Number</h5>
                                    <?php echo form_input('form_parental_contact_2', set_value('form_parental_contact_2'), 'class="simple-input"'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Educational Setting</h5>
                        <br /><br />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h5 class="h5">Name of School</h5>
                                    <?php echo form_input('form_school_name', set_value('form_school_name'), 'class="simple-input"'); ?>
                                    <div id="error-form_school_name" class="error"></div>
                                </div>
                                <div class="form-group">
                                    <h5 class="h5">Level (if applicable)</h5>
                                    <?php echo form_input('form_school_level', set_value('form_school_level'), 'class="simple-input"'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3 class="h3">Reason for Referral</h3>
                    <hr />

                    <div class="form-group">
                        <h5 class="h5">Is the child experiencing difficulties below what is expected for the child’s developmental age or cognitive ability in any of the following occupational performance areas?</h5>
                        <br /><br />
                        <?php
                            $arr = create_dropdown('array', 'Dressing,Toileting,Feeding,Handwriting,Hand skills,Seating,Sensory processing (extreme sensitivity or poor awareness of touch, sound, taste, smell or movement)');
                        ?>
                        <?php foreach ( $arr as $val ) { ?>
                            <label class="checkbox-entry">
                                <input type="checkbox" name="form_difficulties[]" value="<?php echo $val; ?>"><span> <?php echo $val; ?></span>
                            </label>
                            <div class="empty-space col-xs-b20"></div>
                        <?php } ?>
                        <div id="error-form_difficulties" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Please provide further details</h5>
                
                        <?php echo form_textarea('form_difficulties_details', set_value('form_difficulties_details'), 'class="simple-input"'); ?>
                        <div id="error-form_difficulties_details" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Has your child been assessed by occupational therapy before?</h5>
                        <?php
                            $arr = create_dropdown('array', 'Yes,No');
                        ?>
                        <?php foreach ( $arr as $val ) { ?>
                            <label class="checkbox-entry radio">
                                <input type="radio" name="form_occupational_therapy" value="<?php echo $val; ?>"><span><?php echo $val; ?></span>
                            </label>
                            <div class="empty-space col-xs-b20"></div>
                        <?php } ?>
                        <div id="error-form_occupational_therapy" class="error"></div>
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Are there currently any other professionals involved with the child?</h5>
                        <?php
                            $arr = create_dropdown('array', 'Yes,No');
                        ?>
                        <?php foreach ( $arr as $val ) { ?> 
                            <label class="checkbox-entry radio">
                                <input type="radio" name="form_professional_involved" value="<?php echo $val; ?>"><span><?php echo $val; ?></span>
                            </label>
                            <div class="empty-space col-xs-b20"></div>
                        <?php } ?>
                        <div id="error-form_professional_involved" class="error"></div>
                        
                    </div>
                    <div class="form-group">
                        <h5 class="h5">Please provide further details</h5>
                
                        <?php echo form_textarea('form_professional_involved_details', set_value('form_professional_involved_details'), 'class="simple-input"'); ?>
                        <div id="error-form_professional_involved_details" class="error"></div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <h4 class="h4">Consent</h4>
                <hr />

                <div class="form-group">
                    <h5 class="h5">The parents or authorised guardian of this child gives consent for the enclosed information for therapy use by Lifeweavers and their staff.</h5>

                    <div class="empty-space col-xs-b20"></div>
                    <div class="col-md-6">
                        <?php echo form_input('form_parent_signature', set_value('form_parent_signature'), 'class="simple-input"'); ?>
                        <h5 class="h5">Signature of Parent or Authorised Guardian</h5>
                        <div id="error-form_parent_signature" class="error"></div>
                    </div>
                    <div class="col-md-6">
                        <?php echo form_input('form_parent_contact', set_value('form_parent_contact'), 'class="simple-input"'); ?>
                        <h5 class="h5">Contact No.</h5>
                        <div id="error-form_parent_contact" class="error"></div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-6">
                    <br /><br /><br />
                    <div class="button size-2 style-3">
                        <span class="button-wrapper">
                            <span class="icon"><img src="<?php echo site_url('themes/gooccupational/img/icon-4.png'); ?>" alt="" /></span>
                            <span class="text">submit</span>
                        </span>
                        <input type="submit" name="submit" id="submit_form" />
                    </div>  
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
<br /><br /><br />