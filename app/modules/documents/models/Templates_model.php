<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Templates_model Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Templates_model extends BF_Model {

	protected $table_name			= 'documentations_template';
	protected $key					= 'documentation_template_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'documentation_template_created_on';
	protected $created_by_field		= 'documentation_template_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'documentation_template_modified_on';
	protected $modified_by_field	= 'documentation_template_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'documentation_template_deleted';
	protected $deleted_by_field		= 'documentation_template_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'documentation_template_id',
			'documentation_template_title'
		);

		return $this->datatables($fields);
	}

	public function get_templates()
	{
		$templates = $this->where('documentation_template_deleted', 0)
						  ->find_all();

		return $templates;
	}

	public function get_template($id)
	{
		$template = $this->where('documentation_template_deleted', 0)
						 ->find($id);
			
		return $template;
	}

	public function get_template_dropdown()
	{
		$templates = $this->where('documentation_template_deleted', 0)
						  ->format_dropdown('documentation_template_id', 'documentation_template_title', TRUE);

		return $templates;
	}
}