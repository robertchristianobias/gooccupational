<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Forms_model Class
 *
 * @package		GoOccupational
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, GoOccupational
 * @link		http://www.rchristianobias.com
 */
class Forms_model extends BF_Model {

	protected $table_name			= 'forms';
	protected $key					= 'form_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'form_created_on';
	protected $created_by_field		= 'form_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'form_modified_on';
	protected $modified_by_field	= 'form_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'form_deleted';
	protected $deleted_by_field		= 'form_deleted_by';
	
}