<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Documents Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Documents extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
		$this->load->library('ion_auth');

		$this->load->model('templates_model');
		$this->load->model('documents_model');
		$this->load->model('files_shared_model');

		$this->load->model('accounts/contacts_model');
		$this->load->model('accounts/newsfeed_model');

		$this->lang->load('documents');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
    public function index() 
    {
		$this->acl->restrict('accounts.document.link');

		$data['page_heading'] = 'Documents';
		
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');

		$this->template->add_js(module_js('documents', 'documents_index'), 'embed');

		$this->template->write_view('content', 'documents_index', $data);
		$this->template->render();
	}

	public function datatables()
	{
		echo $this->documents_model->get_datatables();
	}
	
	public function share($document_id)
	{
		$data['page_heading'] = 'Share File';
		$data['document_id'] = encode($document_id);
		$data['contacts'] = $this->contacts_model->get_contacts_by_user($this->session->userdata('user_id'));
		
		if ($_POST)
		{

			$this->form_validation->set_rules('contacts[]', lang('contacts'), 'required');

			$this->form_validation->set_message('required', 'This field is required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'contacts'		=> form_error('contacts[]'),
				);
			}
			else
			{
				// add to shared contacts
				$contacts = $this->input->post('contacts');
				if ( $contacts )
				{
					$data_arr = array();
					foreach ( $contacts as $contact )
					{
						$data_arr[] = array(
							'file_shared_file_id' => $document_id,
							'file_shared_to_id' => $contact,
							'file_shared_from_id' => $this->session->userdata('user_id'),
							'file_shared_type' => 'Document'
						);
					}

					$this->files_shared_model->insert_batch($data_arr);

					// add to newsfeed
					$newsfeed_data = array(
						'newsfeed_user_id' => $this->session->userdata('user_id'),
						'newsfeed_message' => 'shared files to ' . count($contacts) . ' contacts'
					);

					$this->newsfeed_model->insert($newsfeed_data);
				}
				
				$response['success'] = 'TRUE';
				$response['message'] = 'Documents shared successfully';
			}

			echo json_encode($response); exit;
		}
		$this->template->set_template('modal');

		$this->template->write_view('content', 'document_share', $data);
		$this->template->render();
	}


	// public function form($action)
	// {
	// 	$data['page_heading'] 	 = lang($action . '_document');
	// 	$data['page_subheading'] = '';
	// 	$data['templates'] 		 = $this->templates_model->get_template_dropdown();

	// 	if ( $_POST )
	// 	{
	// 		$this->form_validation->set_rules('document_template_id', lang('document_template_id'), 'required');
	// 		$this->form_validation->set_rules('document_name', lang('document_name'), 'required');
	// 		$this->form_validation->set_rules('document_content', lang('document_content'), 'required');
	
	// 		$this->form_validation->set_message('required', 'This field is required');
	// 		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			
	// 		if ($this->form_validation->run($this) == FALSE)
	// 		{
	// 			$response['success'] = FALSE;
	// 			$response['message'] = lang('validation_error');
	// 			$response['errors'] = array(
	// 				'document_template_id' 	=> form_error('document_template_id'),
	// 				'document_name'			=> form_error('document_name'),
	// 				'document_content'		=> form_error('document_content'),
	// 			);
	// 		}
	// 		else
	// 		{
	// 			$data = array(
	// 				'document_template_id'  => $this->input->post('document_template_id'),
	// 				'document_name'			=> $this->input->post('document_name'),
	// 				'document_content' 		=> $this->input->post('document_content')
	// 			);

	// 			$this->documents_model->insert($data);

	// 			$response['success'] = TRUE;
	// 			$response['message'] = 'Successfully added the document';
	// 			$response['redirect'] = site_url('accounts/documents');
	// 		}

	// 		echo json_encode($response);
	// 		exit;
	// 	}

	// 	$this->template->add_js('components/ckeditor/ckeditor.js');

	// 	$this->template->write_view('content', 'document_form', $data);
	// 	$this->template->render();
	// }

	// public function process_template()
	// {
	// 	$template_id = (int) $this->input->post('template_id');
	// 	$template = $this->templates_model->get_template($template_id);

	// 	echo $template->documentation_template_content;
	// }


}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */