<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Forms Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Forms extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
		$this->load->library('ion_auth');

		$this->load->model('forms_model');
		$this->lang->load('forms');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
    public function index() 
    {
		$this->acl->restrict('accounts.document.link');

		$data['page_heading'] = 'Documents';
		
		$this->template->add_css('components/datatables/media/css/jquery.dataTables.min.css');
		$this->template->add_js('components/datatables/media/js/jquery.dataTables.min.js');

		$this->template->add_js(module_js('documents', 'documents_index'), 'embed');

		$this->template->write_view('content', 'documents_index', $data);
		$this->template->render();
	}

	public function referral()
	{
		$data['page_heading'] = lang('referral_form');

		if ($_POST)
		{
			$this->form_validation->set_rules('form_childs_name', lang('form_childs_name'), 'required');
			$this->form_validation->set_rules('form_date_of_birth', lang('form_date_of_birth'), 'required');
			$this->form_validation->set_rules('form_passport', lang('form_passport'), 'required');

			$this->form_validation->set_rules('form_citizenship', lang('form_citizenship'), 'required');
			$this->form_validation->set_rules('form_address', lang('form_address'), 'required');
			$this->form_validation->set_rules('form_contact', lang('form_contact'), 'required');
			$this->form_validation->set_rules('form_medical_diagnosis', lang('form_medical_diagnosis'), 'required');
			$this->form_validation->set_rules('form_medical_history', lang('form_medical_history'), 'required');
			$this->form_validation->set_rules('form_medications', lang('form_medications'), 'required');
			$this->form_validation->set_rules('form_parental_name_1', lang('form_parental_name_1'), 'required');
			$this->form_validation->set_rules('form_parental_relationship_1', lang('form_parental_relationship_1'), 'required');
			$this->form_validation->set_rules('form_parental_contact_1', lang('form_parental_contact_1'), 'required');
			$this->form_validation->set_rules('form_school_name', lang('form_school_name'), 'required');
			$this->form_validation->set_rules('form_school_level', lang('form_school_level'), 'required');
			$this->form_validation->set_rules('form_difficulties_details', lang('form_difficulties_details'), 'required');
			$this->form_validation->set_rules('form_professional_involved_details', lang('form_professional_involved_details'), 'required');
			$this->form_validation->set_rules('form_parent_signature', lang('form_parent_signature'), 'required');
			$this->form_validation->set_rules('form_passport', lang('form_passport'), 'required');


			$this->form_validation->set_message('required', 'This field is required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'form_childs_name'		=> form_error('form_childs_name'),
					'form_date_of_birth'	=> form_error('form_date_of_birth'),
					'form_passport'	=> form_error('form_passport'),

					'form_citizenship'	=> form_error('form_citizenship'),
					'form_address'	=> form_error('form_address'),
					'form_contact'	=> form_error('form_contact'),
					'form_medical_diagnosis' => form_error('form_medical_diagnosis'),
					'form_medical_history'	=> form_error('form_medical_history'),
					'form_medications'	=> form_error('form_medications'),
					'form_parental_name_1'	=> form_error('form_parental_name_1'),
					'form_parental_relationship_1'	=> form_error('form_parental_relationship_1'),
					'form_school_name'	=> form_error('form_school_name'),
					'form_school_level'	=> form_error('form_school_level'),
					'form_difficulties_details'	=> form_error('form_difficulties_details'),
					'form_professional_involved_details'	=> form_error('form_professional_involved_details'),
					'form_parent_signature'	=> form_error('form_parent_signature'),
					'form_parent_contact'	=> form_error('form_parent_contact')
				);
			}
			else
			{
				$data = array(
					'form_type' => 'Referral',
					'form_details' => json_encode($_POST)
				);

				$this->forms_model->insert($data);
				
				$response['success'] = TRUE;
				$response['message'] = 'Successfully saved the form';
			}

			echo json_encode($response); exit;
		}
		
		$this->template->add_js(module_js('documents', 'form_referral'), 'embed');

		$this->template->write_view('content', 'form_referral', $data);
		$this->template->render();
	}



}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */