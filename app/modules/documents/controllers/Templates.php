<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Templates Class
 *
 * @package		Codeigniter
 * @version		1.1
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015-2016, 
 * @link		http://www.rchristianobias.com
 */
class Templates extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
		$this->load->library('ion_auth');

		$this->load->model('templates_model');
		$this->load->model('shared_templates_model');
		$this->load->model('accounts/contacts_model');

		$this->lang->load('templates');
	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
    public function index() 
    {
		$this->acl->restrict('accounts.document_template.list');

		$data['page_heading'] = 'Templates';
		$data['templates'] = $this->templates_model->get_templates();
        
		$this->template->write_view('content', 'templates_index', $data);
		$this->template->render();
	}
	
	public function form($action)
	{
		$data['page_heading'] = lang($action . '_template');
		$data['page_subheading'] = '';

		if ( $_POST )
		{
			$this->form_validation->set_rules('documentation_template_title', lang('documentation_template_title'), 'required');
			$this->form_validation->set_rules('documentation_template_content', lang('documentation_template_content'), 'required');
	
			$this->form_validation->set_message('required', 'This field is required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			
			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(
					'documentation_template_title' 		=> form_error('documentation_template_title'),
					'documentation_template_content'	=> form_error('documentation_template_content'),
				);
			}
			else
			{
				$data = array(
					'documentation_template_title'   => $this->input->post('documentation_template_title'),
					'documentation_template_content' => $this->input->post('documentation_template_content')
				);

				$this->templates_model->insert($data);

				$response['success'] = TRUE;
				$response['message'] = 'Successfully added the template';
				$response['redirect'] = site_url('accounts/documents/templates');
			}

			echo json_encode($response);
			exit;
		}

		$this->template->add_js('components/ckeditor/ckeditor.js');

		$this->template->write_view('content', 'template_form', $data);
		$this->template->render();
	}

	public function share_template($template_id)
	{
		$data['page_heading'] = 'Share template';
		$data['contacts'] = $this->contacts_model->get_contacts_by_user($this->session->userdata('user_id'));
		$data['template_id'] = $template_id;

        $this->template->set_template('modal');
        
		$this->template->write_view('content', 'template_share', $data);
		$this->template->render();
	}

	public function process_share_template()
	{
		$this->form_validation->set_rules('contacts[]', lang('contacts'), 'required');

        $this->form_validation->set_message('required', 'This field is required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        
        if ($this->form_validation->run($this) == FALSE)
        {
            $response['success'] = FALSE;
            $response['message'] = lang('validation_error');
            $response['errors'] = array(					
                'contacts'	=> form_error('contacts[]'),
            );
        }
        else
        {
            $contacts = $this->input->post('contacts');
            $contacts_data = array();
            if ( $contacts )
            {
                foreach ($contacts as $contact )
                {
                    $contacts_data[] = array(
                        'st_template_id'  => $this->input->post('template_id'),
						'st_contact_id'   => $contact, 
						'st_shared_by_id' => $this->session->userdata('user_id')
                    );
                }

                $this->shared_templates_model->insert_batch($contacts_data);
            }

            $response['success'] = TRUE;
            $response['message'] = 'Template shared';
            $response['redirect'] = site_url('accounts/documents/templates');

        }

        echo json_encode($response); exit;
	}
}

/* End of file Pages.php */
/* Location: ./application/modules/website/controllers/Pages.php */