<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']	= 'Forms';

$lang['referral_form'] = 'Referral Form';




