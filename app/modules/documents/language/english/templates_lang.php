<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Templates';

// Labels
$lang['add_template'] = 'Add Template';

// Buttons
$lang['button_add']					= 'Add Template';
$lang['button_save']				= 'Save Changes';
$lang['button_delete']				= 'Delete Template';
$lang['button_edit_this']			= 'Edit This';


