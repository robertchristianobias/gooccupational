<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Pages Language File (English)
 *
 * @package		Codeigniter
 * @version		1.0
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2015, 
 * @link		http://www.rchristianobias.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Documents';

// Labels
$lang['add_document'] = 'Add Document';

// Buttons
$lang['button_add']					= 'Add Document';
$lang['button_save']				= 'Save Changes';
$lang['button_delete']				= 'Delete Document';
$lang['button_edit_this']			= 'Edit This';

$lang['index_id'] = 'ID';
$lang['index_name'] = 'Name';
$lang['index_action'] = 'Action';


