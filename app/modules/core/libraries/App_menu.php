<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * App_menu Class
 *
 * @package		Codeigniter
 * @version		1.5
 * @author 		Robert Christian Obias <robertchristianobias@gmail.com>
 * @copyright 	Copyright (c) 2014-2017, Robert Christian Obias
 * @link		robertchristianobias@gmail.com
 */
class App_menu {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->driver('cache', $this->CI->config->item('cache_drivers'));

		log_message('debug', "App_menu Class Initialized");
	}

	// --------------------------------------------------------------------

	/**
	 * show
	 *
	 * Generates the navigation menu
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	function show($type = FALSE)
	{
		// get the user's groups
		$user_groups = $this->CI->ion_auth->get_users_groups($this->CI->session->userdata('user_id'))->result();
		$group_ids = array();
		foreach ($user_groups as $group) $group_ids[] = $group->id;

		// get the grants
		$app_grants = $this->_get_grants();
		$grants = array();
		foreach ($app_grants as $grant)
		{
			if (in_array($grant->grant_group_id, $group_ids) AND $grant->grant_access == 1)
			{
				$grants[] = $grant->permission_code;
			}
		}

		// get the menus
		$app_menus = $this->_get_menus();

		// pr($menus); exit;
		$navs = array();
		$subnavs = array();
		
		foreach ($app_menus as $menu)
		{
			if ($menu->menu_parent > 0)
			{
				$active = $this->_check_active($app_menus, $menu); 
				$subnavs[] = array(
					'menu_id'			=> $menu->menu_id,
					'menu_text'			=> $menu->menu_text,
					'menu_link'			=> $menu->menu_link,
					'menu_perm'			=> $menu->menu_perm,
					'menu_icon'			=> $menu->menu_icon,
					'menu_active'		=> $active,
					'menu_parent'		=> $menu->menu_parent,
				);
			}
			else
			{
				$active = $this->_check_active($app_menus, $menu, TRUE); 
				$navs[$menu->menu_id] = array(
					'menu_id'			=> $menu->menu_id,
					'menu_text'			=> $menu->menu_text,
					'menu_link'			=> $menu->menu_link,
					'menu_perm'			=> $menu->menu_perm,
					'menu_icon'			=> $menu->menu_icon,
					'menu_active'		=> $active,
				);
			}
		}


		foreach ($subnavs as $subnav)
		{
			$navs[$subnav['menu_parent']]['menu_child'][] = array(
				'menu_id'			=> $subnav['menu_id'],
				'menu_text'			=> $subnav['menu_text'],
				'menu_link'			=> $subnav['menu_link'],
				'menu_perm'			=> $subnav['menu_perm'],
				'menu_icon'			=> $subnav['menu_icon'],
				'menu_active'		=> $subnav['menu_active'],
			);
		}

		$html  = '<ul class="nav navbar-nav side-nav nicescroll-bar">';
		$html .= '	<li class="navigation-header">
						<span>Main</span> 
						<i class="zmdi zmdi-more"></i>
					</li>';
		
		if ( $navs )
		{
			foreach ($navs as $nav)
			{
				if (in_array($nav['menu_perm'], $grants))
				{
					$html  .= '<li>';
					$active = $nav['menu_active'] == '1' ? 'active' : '';
					$target = url_title($nav['menu_text'], '_', true);
					if (isset($nav['menu_child'])) // has submenu
					{
						$html .= '<a class="' . $active .'" href="javascript:void(0);" data-toggle="collapse" data-target="#' . $target . '"><div class="pull-left"><i class="'.$nav['menu_icon'].' mr-20"></i><span class="right-nav-text">'.$nav['menu_text'].'</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>';
						$html .= '<ul id="' . $target . '" class="collapse collapse-level-1">';
						
						foreach($nav['menu_child'] as $submenu)
						{
							if ( in_array($submenu['menu_perm'], $grants))
							{
								$html .= '	<li>';
								$html .= '		<a href="'.site_url($submenu['menu_link']).'">'.$submenu['menu_text'].'</a>';
								$html .= '	</li>';
							}
						}
					
						$html .= '</ul>';
					}
					else
					{
						$html .= '	<a href="'.site_url($nav['menu_link']).'"><div class="pull-left"><i class="'.$nav['menu_icon'].' mr-20"></i><span class="right-nav-text">'.$nav['menu_text'].'</span></div><div class="clearfix"></div></a>';
					}

					$html .= '</li>';
				}
			}
		}
		
		return $html;
		
	}

	// --------------------------------------------------------------------

	/**
	 * _check_active
	 *
	 * Generates the navigation menu
	 *
	 * @access	private
	 * @param	object $menus
	 * @param	object $menu
	 * @param	integer $parent
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _check_active($menus, $menu, $parent = FALSE)
	{
		if ($parent && $this->_check_child($menus, $menu))
		{
			return TRUE;
		}
		// else if (site_url($menu->menu_link) == current_url())
		else if (preg_match("/" . urlencode(site_url($menu->menu_link)) . "/", urlencode(current_url())))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * _check_child
	 *
	 * Generates the navigation menu
	 *
	 * @access	private
	 * @param	object $menus
	 * @param	integer $parent
	 * @author 	Robert Christian Obias <robertchristianobias@gmail.com>
	 */
	private function _check_child($menus, $parent)
	{
		$return = FALSE;

		foreach ($menus as $child)
		{
			// if this menu is a child of the parent menu
			if ($child->menu_parent == $parent->menu_id)
			{
				// check if this child is the active menu
				// if (site_url($child->menu_link) == current_url())
				if (preg_match("/" . urlencode(site_url($child->menu_link)) . "/", urlencode(current_url())))
				{
					$return = TRUE;
					break;
				}
			}
		}

		return $return;
	}

	// --------------------------------------------------------------------

	/**
	 * _get_grants
	 *
	 */
	public function _get_grants()
	{
		// get the grants
		if (! $app_grants = $this->CI->cache->get('app_grants'))
		{
			$app_grants = $this->CI->db
				->join('permissions', 'permission_id = grant_permission_id', 'LEFT')
				->get('grants')
				->result();

			$this->CI->cache->save('app_grants', $app_grants, 86400); // 1 day
		}

		return $app_grants;
	}

	// --------------------------------------------------------------------

	/**
	 * _get_menus
	 *
	 */
	public function _get_menus()
	{
		// get the menus
		if (! $app_menu = $this->CI->cache->get('app_menu'))
		{
			$app_menu = $this->CI->db->where('menu_active', 1)->where('menu_deleted', 0)
				->order_by('menu_order')->get('menus')->result();

			$this->CI->cache->save('app_menu', $app_menu, 86400); // 1 day
		}

		return $app_menu;
	}
}

/* End of file App_menu.php */
/* Location: ./application/libraries/App_menu.php */