<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['mailtype']     = "html";
$config['charset']      = "utf-8";
$config['crlf']         = '\n';
$config['newline']      = '\r\n';
$config['protocol']     = 'mail';

// $config['smtp_host']    = '';
// $config['smtp_user']    = '';
// $config['smtp_pass']    = '';
// $config['smtp_port']    = 25;