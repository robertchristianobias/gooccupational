<?php // Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
	header('X-Frame-Options: SAMEORIGIN'); // FF 3.6.9+ Chrome 4.1+ IE 8+ Safari 4+ Opera 10.5+
    $user = $this->ion_auth->user()->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <link href="<?php echo site_url('themes/gooccupational/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo site_url('themes/gooccupational/css/bootstrap.extension.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo site_url('themes/gooccupational/css/style.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo site_url('themes/gooccupational/css/swiper.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo site_url('themes/gooccupational/css/sumoselect.css'); ?>" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo site_url('components/select2/dist/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo site_url('components/sweetalert/dist/sweetalert.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo site_url('components/bootstrap-calendar-master/css/calendar.min.css'); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo site_url('themes/gooccupational/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo site_url('themes/gooccupational/css/custom.css'); ?>" rel="stylesheet" type="text/css" />

    <!-- <link rel="shortcut icon" href="<?php echo site_url('themes/gooccupational/img/favicon.ico'); ?>" /> -->
      
    <title><?php echo $page_heading; ?> | <?php echo config_item('app_name'); ?></title>
</head>
<body>

    <!-- LOADER -->
    <div id="loader-wrapper"></div>

    <div id="content-block">
        <!-- HEADER -->
        <header>
            <div class="header-top">
                <div class="content-margins">
                    <div class="row">
                        <div class="col-md-7 hidden-xs hidden-sm">
                            <div class="entry lvv"><a href="<?php echo site_url('about-us'); ?>"><img src="<?php echo site_url('themes/gooccupational/images/lwvlogo00.png'); ?>"  /></a></div>
                            <div class="entry docu"><a href="<?php echo site_url('documentations'); ?>"><img src="<?php echo site_url('themes/gooccupational/images/edocumentationlogo.png'); ?>" /></a></div>
                        </div>
                        <div class="col-md-5 col-md-text-right">
                            <div class="entry">
                                <?php if ( $user ) { ?>
                                    <a href="<?php echo site_url('accounts/newsfeed'); ?>"><b>Welcome <?php echo $user->first_name; ?></b></a> | <a href="<?php echo site_url('accounts/logout'); ?>"><b>Logout</b></a>
                                <?php } else { ?>
                                    <a class="open-popup-ajax" href="<?php echo site_url('accounts/login'); ?>" ><b>login</b></a>&nbsp; or &nbsp;
                                    <a class="open-popup-ajax" href="<?php echo site_url('accounts/register'); ?>"><b>register</b></a>
                                <?php } ?>                                
                            </div>
                            <div class="entry language">
                                <div class="title"><b>en</b></div>
                                <div class="language-toggle header-toggle-animation">
                                    <a href="index1.html">fr</a>
                                    <a href="index1.html">ru</a>
                                    <a href="index1.html">it</a>
                                    <a href="index1.html">sp</a>
                                </div>
                            </div>
                            <div class="entry hidden-xs hidden-sm">
                                <a href="<?php echo site_url('articles/saved'); ?>" class="open-popup-ajax">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                </a>
                            </div>
                           
                            <div class="hamburger-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom">
                <div class="content-margins">
                    <div class="row clearfix">
                        <div class="col-xs-6 col-sm-4 col-md-4">
                            <a id="logo" href="<?php echo site_url(); ?>">
                                <img src="<?php echo site_url('themes/gooccupational/img/logo.png'); ?>" alt="" />
                            </a>  
                        </div>
                        <div class="col-xs-6 col-sm-8 col-md-8 text-right">
                            <div class="nav-wrapper">
                                <div class="nav-close-layer"></div>
                                <nav>
                                    <ul>
                                        <li>
                                            <a href="#">Home</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('about-us'); ?>">about us</a>
                                        </li>
                                      
                                        <li>
                                            <a href="<?php echo site_url('articles'); ?>">Articles</a>
                                        </li>
                                      
                                    </ul>
                                    <div class="navigation-title">
                                        Navigation
                                        <div class="hamburger-icon active">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                            <div class="header-bottom-icon visible-rd">
                                <i class="fa fa-heart-o" aria-hidden="true"></i>
                            </div>
                            <div class="header-bottom-icon visible-rd">
                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                <span class="cart-label">5</span>
                            </div>
                        </div>
                    </div>
                    <div class="header-search-wrapper">
                        <div class="header-search-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                                        <form>
                                            <div class="search-submit">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                                <input type="submit"/>
                                            </div>
                                            <input class="simple-input style-1" type="text" value="" placeholder="Enter keyword" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="button-close"></div>
                        </div>
                    </div>
                </div>
            </div>

        </header>

        <div class="header-empty-space"></div>

        <div class="container">

            <div class="empty-space col-xs-b15 col-sm-b50 col-md-b50"></div>
            <?php echo $content; ?>
            
        </div>

        <!-- FOOTER -->
        <footer>
            <div class="container">
                <div class="footer-top">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <img src="<?php echo site_url('themes/gooccupation/img/logo-1.png'); ?>" alt="" />
                            <div class="empty-space col-xs-b20"></div>
                            <div class="simple-article size-2 light fulltransparent">Integer posuere orci sit amet feugiat pellent que. Suspendisse vel tempor justo, sit amet posuere orci dapibus auctor</div>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-contact"><i class="fa fa-mobile" aria-hidden="true"></i> contact us: <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                            <div class="footer-contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> email: <a href="mailto:office@exzo.com">office@exzo.com</a></div>
                            <div class="footer-contact"><i class="fa fa-map-marker" aria-hidden="true"></i> address: <a href="#">1st, new york, usa</a></div>
                        </div>
                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <h6 class="h6 light">queck links</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-column-links">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a href="#">home</a>
                                        <a href="#">about us</a>
                                        <a href="#">products</a>
                                        <a href="#">services</a>
                                        <a href="#">blog</a>
                                        <a href="#">gallery</a>
                                        <a href="#">contact</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <a href="#">privacy policy</a>
                                        <a href="#">warranty</a>
                                        <a href="#">login</a>
                                        <a href="#">registration</a>
                                        <a href="#">delivery</a>
                                        <a href="#">pages</a>
                                        <a href="#">our stores</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear visible-sm"></div>
                        <div class="col-sm-6 col-md-3 col-xs-b30 col-sm-b0">
                            <h6 class="h6 light">some posts</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-post-preview clearfix">
                                <a class="image" href="#"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-1.jpg'); ?>" alt="" /></a>
                                <div class="description">
                                    <div class="date">apr 07 / 15</div>
                                    <a class="title">Fusce tincidunt accumsan pretium sit amet</a>
                                </div>
                            </div>
                            <div class="footer-post-preview clearfix">
                                <a class="image" href="#"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-2.jpg'); ?>" alt="" /></a>
                                <div class="description">
                                    <div class="date">apr 07 / 15</div>
                                    <a class="title">Fusce tincidunt accumsan pretium sit amet</a>
                                </div>
                            </div>
                            <div class="footer-post-preview clearfix">
                                <a class="image" href="#"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-3.jpg'); ?>" alt="" /></a>
                                <div class="description">
                                    <div class="date">apr 07 / 15</div>
                                    <a class="title">Fusce tincidunt accumsan pretium sit amet</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h6 class="h6 light">popular tags</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="tags clearfix">
                                <a class="tag">headphoness</a>
                                <a class="tag">accessories</a>
                                <a class="tag">new</a>
                                <a class="tag">wireless</a>
                                <a class="tag">cables</a>
                                <a class="tag">devices</a>
                                <a class="tag">gadgets</a>
                                <a class="tag">brands</a>
                                <a class="tag">replacements</a>
                                <a class="tag">cases</a>
                                <a class="tag">cables</a>
                                <a class="tag">professional</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
                            <div class="copyright">&copy; 2015 All rights reserved. Development by <a href="http://themeforest.net/user/unionagency" target="_blank">UnionAgency</a></div>
                            <div class="follow">
                                <a class="entry" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="entry" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="entry" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="entry" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="entry" href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-text-center col-lg-text-right">
                            <div class="footer-payment-icons">
                                <a class="entry"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-4.jpg'); ?>" alt="" /></a>
                                <a class="entry"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-5.jpg'); ?>" alt="" /></a>
                                <a class="entry"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-6.jpg'); ?>" alt="" /></a>
                                <a class="entry"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-7.jpg'); ?>" alt="" /></a>
                                <a class="entry"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-8.jpg'); ?>" alt="" /></a>
                                <a class="entry"><img src="<?php echo site_url('themes/gooccupational/img/thumbnail-9.jpg'); ?>" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- modal -->
    <div class="popup-wrapper">
        <div class="bg-layer"></div>

        <div class="popup-content" data-rel="1">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Log in</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <input class="simple-input" type="text" value="" placeholder="Your email" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <input class="simple-input" type="password" value="" placeholder="Enter password" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <div class="empty-space col-sm-b5"></div>
                            <a class="simple-link">Forgot password?</a>
                            <div class="empty-space col-xs-b5"></div>
                            <a class="simple-link">register now</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a class="button size-2 style-3" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                    <span class="text">submit</span>
                                </span>
                            </a>  
                        </div>
                    </div>
                    <div class="popup-or">
                        <span>or</span>
                    </div>
                    <div class="row m5">
                        <div class="col-sm-4 col-xs-b10 col-sm-b0">
                            <a class="button facebook-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                    <span class="text">facebook</span>
                                </span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-b10 col-sm-b0">
                            <a class="button twitter-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                    <span class="text">twitter</span>
                                </span>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a class="button google-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                    <span class="text">google+</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content" data-rel="2">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">register</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <input class="simple-input" type="text" value="" placeholder="Your name" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <input class="simple-input" type="text" value="" placeholder="Your email" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <input class="simple-input" type="password" value="" placeholder="Enter password" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <input class="simple-input" type="password" value="" placeholder="Repeat password" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-7 col-xs-b10 col-sm-b0">
                            <div class="empty-space col-sm-b15"></div>
                            <label class="checkbox-entry">
                                <input type="checkbox" /><span><a href="#">Privacy policy agreement</a></span>
                            </label>
                        </div>
                        <div class="col-sm-5 text-right">
                            <a class="button size-2 style-3" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                    <span class="text">submit</span>
                                </span>
                            </a>  
                        </div>
                    </div>
                    <div class="popup-or">
                        <span>or</span>
                    </div>
                    <div class="row m5">
                        <div class="col-sm-4 col-xs-b10 col-sm-b0">
                            <a class="button facebook-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                    <span class="text">facebook</span>
                                </span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-b10 col-sm-b0">
                            <a class="button twitter-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                    <span class="text">twitter</span>
                                </span>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a class="button google-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                    <span class="text">google+</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content"></div>
       

    </div>
  
    <script>
		var site_url = "<?php echo site_url(); ?>";
    </script>
    
    <script src="<?php echo site_url('themes/gooccupational/js/jquery-2.2.4.min.js'); ?>"></script>
    <script src="<?php echo site_url('themes/gooccupational/js/swiper.jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>

    <!-- styled select -->
    <script src="<?php echo site_url('themes/gooccupational/js/jquery.sumoselect.min.js'); ?>"></script>
    <script src="<?php echo site_url('components/select2/dist/js/select2.min.js '); ?>"></script>
    <script src="<?php echo site_url('components/imagesloaded/imagesloaded.pkgd.min.js'); ?>"></script>
    <script src="<?php echo site_url('components/masonry-layout/dist/masonry.pkgd.min.js'); ?>"></script>
    <script src="<?php echo site_url('components/waypoints/lib/jquery.waypoints.min.js'); ?>"></script>
    <script src="<?php echo site_url('components/jquery.appear/jquery.appear.js'); ?>"></script>
    <script src="<?php echo site_url('components/typed.js/dist/typed.min.js'); ?>"></script>
    <script src="<?php echo site_url('components/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <script src="<?php echo site_url('components/ckeditor/ckeditor.js'); ?>"></script>
    <script src="<?php echo site_url('components/bootstrap-calendar-master/js/calendar.min.js'); ?>"></script>
    <script src="<?php echo site_url('components/underscore.js'); ?>"></script>

    <script src="<?php echo site_url('themes/gooccupational/js/global.js'); ?>"></script>

    
    <!-- counter -->
    <script src="<?php echo site_url('themes/gooccupational/js/jquery.classycountdown.js'); ?>"></script>
    <script src="<?php echo site_url('themes/gooccupational/js/jquery.knob.js'); ?>"></script>
    <script src="<?php echo site_url('themes/gooccupational/js/jquery.throttle.js'); ?>"></script>

    <?php echo $_scripts; // loads additional js files from the module ?>
</body>
</html>
