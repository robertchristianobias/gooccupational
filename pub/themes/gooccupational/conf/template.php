<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// gooccupational template
$template['gooccupational']['template']       = 'gooccupational/template';
$template['gooccupational']['regions']        = array('head', 'styles', 'header', 'content', 'footer', 'scripts');
$template['gooccupational']['parser']         = 'parser';
$template['gooccupational']['parser_method']  = 'parse';
$template['gooccupational']['parse_template'] = FALSE;

// blank
$template['blank']['template'] = 'gooccupational/blank';
$template['blank']['regions'] = array('styles', 'header', 'content', 'footer', 'scripts');
$template['blank']['parser'] = 'parser';
$template['blank']['parser_method'] = 'parse';
$template['blank']['parse_template'] = FALSE;

// modal
$template['modal']['template'] = 'gooccupational/modal';
$template['modal']['regions'] = array('styles', 'content', 'scripts');
$template['modal']['parser'] = 'parser';
$template['modal']['parser_method'] = 'parse';
$template['modal']['parse_template'] = FALSE;

// confirm
$template['confirm']['template'] = 'gooccupational/confirm';
$template['confirm']['regions'] = array('content');
$template['confirm']['parser'] = 'parser';
$template['confirm']['parser_method'] = 'parse';
$template['confirm']['parse_template'] = FALSE;