<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// theme config
$config = array(
	'theme_code' 			=> 'gooccupational',
	'theme_name' 			=> 'GoOccupational',
	'theme_author' 			=> 'Robert Christian Obias',
	'theme_copyright' 		=> 'gooccupational',
	'theme_version' 		=> '1.0',
	'theme_layouts' 		=> array(),
	'theme_widget_sections' => array()
);