<?php // Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN'); // FF 3.6.9+ Chrome 4.1+ IE 8+ Safari 4+ Opera 10.5+
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title><?php echo config_item('app_name'); ?> - <?php echo $page_heading; ?></title>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo site_url('themes/material/img/favicon.ico'); ?>">
		<link rel="icon" href="<?php echo site_url('themes/material/img/favicon.ico'); ?>" type="image/x-icon">
		
		<link href="<?php echo site_url('components/sweetalert/dist/sweetalert.css'); ?>" rel="stylesheet" type="text/css">

		<!-- vector map CSS -->
		<link href="<?php echo site_url('components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
		
		<link href="<?php echo site_url('themes/material/css/style.css'); ?>" rel="stylesheet" type="text/css">
		<link href="<?php echo site_url('themes/material/css/custom.css'); ?>" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="index.html">
						<img class="brand-img mr-10" src="<?php echo site_url('themes/material/img/logo.png'); ?>" alt="brand"/>
						<!-- <span class="brand-text"><?php echo config_item('app_name'); ?></span> -->
					</a>
				</div>
			</header>
			<?php echo $content; ?>
		</div>
		
		<!-- jQuery -->
		<script src="<?php echo site_url('components/jquery/dist/jquery.min.js'); ?>"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="<?php echo site_url('components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
		<script src="<?php echo site_url('components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js'); ?>"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="<?php echo site_url('themes/material/js/jquery.slimscroll.js'); ?>"></script>
		<script src="<?php echo site_url('components/sweetalert/dist/sweetalert.min.js'); ?>"></script>
		<?php echo $_scripts; // loads additional js files from the module ?>
		
		<!-- Init JavaScript -->
		<script src="<?php echo site_url('themes/material/js/init.js'); ?>"></script>
	</body>
</html>
