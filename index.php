
        <?php include('header.php'); ?>
        <?php include('banner.php'); ?>
        <div class="row nopadding">
            <div class="col-md-6">
                <div class="block-entry" style="background-image: url('images/webgra1.png');">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="cell-view simple-banner-height middle text-center">
                                    <div class="empty-space col-xs-b35 col-sm-b70"></div>
                                    <div class="simple-article size-3 light transparent uppercase col-xs-b5">we offer</div>
                                    <h2 class="h2 light">HOME BASED THERAPY PROGRAMS</h2>
                                    <div class="title-underline light center"><span></span></div>
                                    <div class="simple-article light transparent size-4">
                                        By offering a gold standard service, we aim to achieve better outcomes and a more holistic treatment for the patient. Change takes time, it is a process and maintenance is key. Our therapists believe getting to know the patient and their needs is fundamental to good therapy. High quality assessment involves taking time to build a relationship with the patient and their support system to achieve the best person centred outcomes. Pick a program for more information on our services. (Only in Singapore.)
                                    </div>
                                    <div class="empty-space col-xs-b35 col-sm-b70"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-entry">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                <!-- Begin Mailchimp Signup Form -->
                                <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                                <style type="text/css">
                                    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                                    /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                                    We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                                </style>
                                <div id="mc_embed_signup">
                                    <form action="https://gmail.us3.list-manage.com/subscribe/post?u=90c85fc2f941add511a317d09&amp;id=c5f44c7f4e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                        <div id="mc_embed_signup_scroll">
                                            <h2>Subscribe to our email newsletter to receive updates on the latest news. </h2>
                                            <div class="indicates-required">
                                                <span class="asterisk">*</span> indicates required
                                            </div>
                                            <div class="mc-field-group">
                                                <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span></label>
                                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                                            </div>
                                            <div class="mc-field-group">
                                                <label for="mce-FNAME">First Name </label>
                                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
                                            </div>
                                            <div class="mc-field-group">
                                                <label for="mce-LNAME">Last Name </label>
                                                <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
                                            </div>
                                            <div id="mce-responses" class="clear">
                                                <div class="response" id="mce-error-response" style="display:none"></div>
                                                <div class="response" id="mce-success-response" style="display:none"></div>
                                            </div>
                                            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                                <input type="text" name="b_90c85fc2f941add511a317d09_c5f44c7f4e" tabindex="-1" value="">
                                            </div>
                                            <div class="clear">
                                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($)
                                {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                                <!--End mc_embed_signup-->
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                            <div class="cell-view simple-banner-height middle">
                                <div class="empty-space col-xs-b35 col-sm-b70"></div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="col-xs-text-center col-sm-text-left">
                                                <div class="simple-article size-2 uppercase color col-xs-b5 program-title">Program</div>
                                                <h5 class="h5 col-xs-b5">Dementia Management Program</h5>
                                                <div class="simple-article size-2 program-content">
                                                    Occupational therapists help people with dementia retain their existing 
                                                    functions for as long as possible, as well as working with their family and 
                                                    caregivers for an all-rounded strategy of care. <br /><br /><a class="open-popup" data-rel="1">More</a>
                                                </div>
                                            </div>
                                            <div class="empty-space col-xs-b30 col-sm-b60"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="col-xs-text-center col-sm-text-left">
                                                <div class="simple-article size-2 uppercase color col-xs-b5 program-title">Program</div>
                                                <h5 class="h5 col-xs-b5">Falls Management Program</h5>
                                                <div class="simple-article size-2 program-content">
                                                    OTs have the skills and knowledge to prevent falls, which often result in hip fractures, traumatic head injury and reduction in ability to perform daily living tasks. <br /><br /><a class="open-popup" data-rel="2">More</a>
                                                </div>
                                            </div>
                                            <div class="empty-space col-xs-b30 col-sm-b60"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="col-xs-text-center col-sm-text-left">
                                                <div class="simple-article size-2 uppercase color col-xs-b5 program-title">Package</div>
                                                <h5 class="h5 col-xs-b5">Environmental Modification Package</h5>
                                                <div class="simple-article size-2 program-content">
                                                    A primary focus of occupational therapy is to ensure the environment you live, work and socialise in fits your needs and maintains your safety. A health condition can create environmental barriers to living a full and happy life. We can help identify environmental risks and offer tailored solutions to maintain your independence and health. <br /><br /><a class="open-popup" data-rel="3">More</a>
                                                </div>
                                            </div>
                                            <div class="empty-space col-xs-b30 col-sm-b60"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="col-xs-text-center col-sm-text-left">
                                                <div class="simple-article size-2 uppercase color col-xs-b5 program-title">program</div>
                                                <h5 class="h5 col-xs-b5">Chronic Condition Management Program</h5>
                                                <div class="simple-article size-2 program-content">
                                                    To design a comprehensive plan to help one cope with a health condition that poses new challenges physically, functionally, socially and psychologically. <br /><br /><a class="open-popup" data-rel="4">More</a>
                                                </div>
                                            </div>
                                            <div class="empty-space col-xs-b30 col-sm-b60"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="col-xs-text-center col-sm-text-left">
                                                <div class="simple-article size-2 uppercase color col-xs-b5 program-title">Training</div>
                                                <h5 class="h5 col-xs-b5">Motorised Mobility Device Training</h5>
                                                <div class="simple-article size-2 program-content">
                                                    We will help choose the most suitable equipment in the market and teach you and your family how to use it safely and effectively. <br /><br /><a class="open-popup" data-rel="5">More</a>
                                                </div>
                                            </div>
                                            <div class="empty-space col-xs-b30 col-sm-b0"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="col-xs-text-center col-sm-text-left">
                                                <div class="simple-article size-2 uppercase color col-xs-b5 program-title">package</div>
                                                <h5 class="h5 col-xs-b5">Therapy Technician Maintenance Package</h5>
                                                <div class="simple-article size-2 program-content">
                                                    For individuals who are making progress, but require a longer term therapeutic option to maintain and develop their skills, our technician maintenance package can be the ideal next step. Our technicians work under the guidance of your allocated therapist to continue delivering high quality therapeutic interventions tailored for your needs. <br /><br /><a class="open-popup" data-rel="6">More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="empty-space col-xs-b35 col-sm-b70"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <div class="container" id="faq-container" style="background-image: url(images/faq-bg.png); background-size: cover; background-repeat: no-repeat;">
            <div class="text-center">
                <div class="h2">FREQUENTLY ASKED QUESTIONS</div>
                <div class="title-underline center"><span></span></div>
            </div>
        </div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <div class="block-entry">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                            What's the difference with Lifeweavers' services compared to seeing an OT in the hospital?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p>
                                                    We are a last mile service, we can help with transitioning home after a hospital stay, help those in the community who have functional difficulties or those who want to prevent complications arising due to lifestyle or a recent diagnosis. We can offer long interventions and high frequency sessions for long term therapy goals. 
                                                </p>
                                                <p>
                                                    Unlike in the hospital, you don't need a doctor's referral, you can self refer. That also means you can get direct feedback and recommendations.
                                                </p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                            Is there anything special with the kind of therapy Lifeweavers do?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p>
                                                    Open Minded <br />
                                                    Some patients have come to us because they want to try different types of therapy from what they’ve previously received. In these cases, we help explore what the other available options are, advise on the pros and cons and guide them through interventions they want to try. We are here to help people make informed decisions and support them through treatments that are meaningful to them. We are also proudly open minded.  
                                                </p>
                                                <p>
                                                    One-On-One <br />
                                                    The one-on-one sessions with a therapist are intensive. The therapist facilitates all the activities, giving direct feedback. This allows the therapist to make ongoing assessment and on-the-spot evaluations so treatment can be adjusted immediately to reap maximum benefits. 
                                                </p>
                                                <p>
                                                    Post Therapy Goals <br />
                                                    We provide frequent and long term one-to-one sessions depending on the client's tolerance and goals. We also make 'post-therapy goals' to help clients plan how they are going to maintain the benefits they have gained.
                                                </p>
                                                <p>
                                                    Couples <br />
                                                    If there are two people in the home who require therapy, we can design programs for both of them at the same time! For example, when assessing grandma and grandpa, we can create individual and joint goals, and then show them how to support each other. Additional session and documentation time will be required for the initial assessment.
                                                </p>
                                                <p>
                                                    Balanced Approach <br />
                                                    We work with both ‘client-centered’ and ‘family-centric’ models of care depending on the case. A client-centered model ensures all care, goal setting, decision making and participation is focused around the individual receiving treatment. This promotes autonomy and self resilience.                                            
                                                </p>
                                                <p>
                                                    A family-centric model requires more involvement from the clients social support, which means they must be present for the sessions too. If people cannot attend due to other commitments, the therapist will request for a spokesperson to be appointed to receive and feedback session information. It's important for the family to be on the same page for this work, frequent changes in decisions will impact the flow of the therapy. 
                                                </p>
                                                <p>
                                                    Telephone Consultations <br />
                                                    Some follow up sessions can be done over the phone. For example, reinforcing strategies or to solve a particular issue on the go. These can range from 15 minutes up to 1 hour of call time and will be built into an intervention.
                                                </p>
                                                <p>
                                                    We can also offer telephone consultation for those who do no want to engage face to face, or for those enquiring on behalf of a loved one. However, there will be limitations to this. In the event that a person feels this is the only suitable option we are happy to help.
                                                </p>
                                            </div>
                                        </div>
                                                                           
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                            How do I know how much therapy do I need?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p>
                                                    It's all about your goals. The therapist can advise on this after the initial assessment. If the patient cannot be present or a caregiver is looking for advice, we can arrange a meeting without the patient. There may be limitations to knowing how much therapy is needed in this case.
                                                </p>
                                                <p>
                                                    That is why we always start with a comprehensive initial assessment. This will help the therapist to discuss how realistic the goals are and assess how much therapy is needed to achieve them.
                                                </p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                            How does a program differ from single sessions? 
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p>
                                                    Programs are designed based on current evidence to holistically address issues. We, both the therapists and patient, will not get the full picture when parts of the program is missing, akin to missing a piece of a jigsaw puzzle. Hence programs aims to yield higher quality outcomes. However, if you have specific therapy goals you can choose to buy single sessions. 
                                                </p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                            Why are some of the programs so long?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p>
                                                    We follow international guidelines for gold standard services. Based on comprehensive guidelines for a complete intervention, missing modules may reduce the effectiveness of therapy. By following gold standard, we aim to achieve better outcomes and a more holistic treatment for the patient. Change takes time. It is a process and maintenance is key. It also takes time for our therapists to build a relationship with the patient and their support system. Single sessions are not silver bullets.
                                                </p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                                            Why, in some packages, there is a whole session on education?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p>
                                                    Occupational therapy investigates problems, analyses the issues, then, develops the logic and reason for solution. Occupational therapists help people become more informed as a patient and caregiver, so education is key. We devote several sessions in a packaged program for education, sometimes with the patient’s family and support system, so they can develop ’the therapist brain’. Understanding the therapist’s logic in relation to your case will enable you to be your own problem solver. This is an important skill and will help you sustain your achievements in therapy. Together with the caregivers and family, it will be a concerted effort towards more effective recovery or rehabilitation. 
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                                            Why do some sessions require the patient to be absent?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p>
                                                    "We involve patients as much as possible in every aspect of their therapy, however there are some circumstances where meeting with family or carers without the person present may be appropriate or necessary."
                                                </p>
                                                <p>
                                                    Patients who have cognitive impairments, such as difficulties with memory and thinking may not be able to give accurate information during the initial assessment. They may become distressed or confused if a caregiver is answering on their behalf. For example, a person may say they have never fallen down or that they are still working and become angry if someone tries to correct them. This will make it difficult for the therapist to gather an accurate background to develop a baseline. A session without the patient present will help the therapist assess the extent of the cognitive issues from another observer and factor this into subsequent sessions.
                                                </p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                                            What is a program by Lifeweavers like?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse8" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p>
                                                    Programs are designed to structure around evidence based practice. They can range from a month to a year, with an OT following up every session or at planned intervals if some of the sessions are carried out by a therapy technician. Each program has predetermined goals which we based on our research to ensure all important areas are holistically addressed. 
                                                </p>
                                                <p>
                                                    Everything starts with a detailed Initial Assessment. We use international gold standard methods and tools, combined with years of experience to determine the functional ability, challenges and potential problems to help establish individualised short and long term goals to be achieved and maintained. 
                                                </p>
                                                <p>
                                                    We look into the setting of the home, work and/or places of interest that is meaningful to the patient and work closely with their support network to ensure the program is delivered with relevance to the individual. Our therapists will also build up a rapport with the patient and caregivers to help manage the case with better context. 
                                                </p>
                                                <p>
                                                    Programs are also important to help a client determine how much therapy they need to commit to get the best results. Change takes time, which is why programs are good to help client follow through on their commitment, to achieve their goals. This also allows the therapist to better understand the patient, in order to decide on the most effective approach, design strategies and activities that are holistic and personal. With a series of sessions, the therapist will enable better evaluation of the patient’s progress and identify when to correct, re-enforce or move to the next step with better control. 
                                                </p>
                                                <p>
                                                    When the main program is over, the patient can opt to switch to a maintenance package with our therapy technicians to upkeep on their current performance or work on long term goals. Your therapist will still oversee the case through technician reports and follow-up assessments.
                                                </p>
                                                <p>
                                                    Reach out to us to discuss on how we can help, email us at <a href="mailto:AskForOT@lifeweavers.org">AskForOT@lifeweavers.org</a>
                                                </p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <div class="slider-wrapper">
            <div class="swiper-button-prev visible-lg"></div>
            <div class="swiper-button-next visible-lg"></div>
            <div class="swiper-container" data-parallax="1" data-auto-height="1">
                <div class="swiper-wrapper">
                    <!-- <div class="swiper-slide">
                        <div class="block-entry" style="background-image: url(images/imgs-cl-1.png); background-size: cover;">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="cell-view simple-banner-height big">
                                            <div class="empty-space col-xs-b35 col-sm-b70"></div>
                                            <div data-swiper-parallax-x="-600">
                                                <h2 class="h1 light">QUALITY OF LIFE</h2>
                                                <div class="title-underline left light"><span></span></div>
                                            </div>
                                            <div data-swiper-parallax-x="-400">
                                                <div class="simple-article light size-4">Live, not just survive</div>
                                            </div>
                                            <div class="empty-space col-xs-b70"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-background right hidden-xs">
                                </div>
                            </div>
                        </div>                     
                    </div> -->
                    <div class="swiper-slide slide-2">
                        <div class="block-entry" style="background-image: url(images/imgs-cl-4.png); background-size: cover;">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="cell-view simple-banner-height big">
                                            <div class="empty-space col-xs-b35 col-sm-b70"></div>
                                            <div data-swiper-parallax-x="-600">
                                                <h2 class="h1 light">DON'T JUST TREAT IT, PREVENT IT!</h2>
                                                <div class="title-underline left light"><span></span></div>
                                            </div>
                                            <div data-swiper-parallax-x="-400">
                                                <div class="simple-article light size-4">
                                                    When we are young and fit, we can be guilty of taking our health for granted. We think about the here and now, with little thought about consequences in the future. Prevention does not mean living under restrictions and having no fun in life. By educating ourselves and understanding that we can have control over our health, will not only mean living life with increased awareness, but can be an empowering process. 
                                                    <br />
                                                    <a href="dont-just-treat-it-prevent-it.php">
                                                        <b>read more</b>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="empty-space col-xs-b70"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-background right hidden-xs">
                                </div>
                            </div>
                        </div>                     
                    </div>
                    <div class="swiper-slide">
                        <div class="block-entry" style="background-image: url(images/imgs-cl-5.png); background-size: cover;">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="cell-view simple-banner-height big">
                                            <div class="empty-space col-xs-b35 col-sm-b70"></div>
                                            <div data-swiper-parallax-x="-600">
                                                <h2 class="h1 light">IMPROVING HEALTH BY ENGAGING IN SOCIAL AND LEISURE ACTIVITIES</h2>
                                                <div class="title-underline left light"><span></span></div>
                                            </div>
                                            <div data-swiper-parallax-x="-400">
                                                <div class="simple-article light size-4">
                                                    <a href="improving-health-by-engaging-in-social-and-leisure-activities.php">
                                                        <b>read more</b>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="empty-space col-xs-b70"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-background right hidden-xs">
                                </div>
                            </div>
                        </div>                     
                    </div>
                </div>
                <div class="swiper-pagination swiper-pagination-white hidden-lg"></div>
            </div>
        </div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <div class="container" id="contact-us">
            <div class="text-center">
                <div class="simple-article size-3 grey uppercase col-xs-b5">hello</div>
                <div class="h2">HAVE QUESTIONS?</div>
                <div class="title-underline center"><span></span></div>
            </div>
        </div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <form method="POST" action="" class="contact-form">
                        <div class="row m5">
                            <div class="col-sm-6">
                                <input class="simple-input col-xs-b20" type="text" value="" placeholder="Your Name, please" name="name" >
                                <div id="error-name" class="error-field"></div>
                            </div>
                            <div class="col-sm-6">
                                <input class="simple-input col-xs-b20" type="text" value="" placeholder="Email, so we can write you back" name="email">
                                <div id="error-email" class="error-field"></div>
                            </div>
                            <div class="col-sm-6">
                                <input class="simple-input col-xs-b20" type="text" value="" placeholder="Phone Number is optional, but will help us contact you easier" name="phone">
                            </div>
                            <div class="col-sm-6">
                                <select class="SlectBox" name="subject">
                                    <option disabled="disabled" selected="selected">Subject</option>
                                    <option value="Information about our Services">Information about our Services</option>
                                    <option value="I am looking for an Occupational Therapist">I am looking for an Occupational Therapist</option>
                                    <option value="Feedback">Feedback</option>
                                    <option value="I have a Business Opportunity, a Corporate Project or Partnership Proposal">I have a Business Opportunity, a Corporate Project or Partnership Proposal</option>
                                    <option value="Others">Others</option>
                                </select>
                                <div id="error-subject" class="error-field"></div>
                            </div>
                            <div class="col-sm-12">
                                <select class="SlectBox" name="how">
                                    <option disabled="disabled" selected="selected">How did you hear about Lifeweavers? (optional, dropdown)</option>
                                    <option value="Someone told me about you">Someone told me about you.</option>
                                    <option value="I'm being referred by a clinic or healthcare provider">I'm being referred by a clinic or healthcare provider.</option>
                                    <option value="Google / web search">Google / web search</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="YouTube">YouTube</option>
                                    <option value="Events">Events</option>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <textarea class="simple-input col-xs-b20" placeholder="Your message" name="message"></textarea>
                                <div id="error-message" class="error-field"></div>
                            </div>
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <div class="button size-2 style-3">
                                        <span class="button-wrapper">
                                            <span class="icon"><img src="img/icon-4.png" alt=""></span>
                                            <span class="text">send message</span>
                                        </span>
                                        <input type="submit" id="send-contact">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <?php include('footer.php'); ?>
        <div class="popup-wrapper">
            <div class="bg-layer"></div>
            <div class="popup-content" data-rel="1">
                <div class="layer-close"></div>
                <div class="popup-container size-1">
                    <div class="popup-align">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src="images/pu-icon-dementia.png" />
                                </div>
                                <h3 class="h3 text-center">Dementia Management Program</h3>
                                <div class="empty-space col-xs-b30"></div>
                                <p>
                                    Each session is 1 hour consisting of 45 minutes contact and 15 minutes for documentation
                                </p>
                                <p>
                                    Total package includes: 10 hours therapy time + materials
                                </p>
                                <ul>
                                    <li>Session 1 – Phone assessment. </li>
                                    <li>Session 2 – Assessment of patient with caregiver present </li>
                                    <li>Session 3 -  Education on dementia symptoms / presentation and occupation</li>
                                    <li>Session 4 - Caregiver self care, addressing caregiver stress</li>
                                    <li>Session 5 – Dementia and environments </li>
                                    <li>Session 6 – Healthy eating and exercise</li>
                                    <li>Session 7 – Falls prevention / recovery </li>
                                    <li>Session 8 – Facilitating activities / grading participation and routine</li>
                                    <li>Session 9 – Dementia  in the community</li>
                                    <li>Session 10 – Review, evaluate and goals beyond therapy.</li>
                                </ul>
                                <p><a href="#contact-us" class="close-contact">Contact us for more details.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="button-close"></div>
                </div>
            </div>
            <div class="popup-content" data-rel="2">
                <div class="layer-close"></div>
                <div class="popup-container size-1">
                    <div class="popup-align">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src="images/pu-icon-falls.png" />
                                </div>
                                <h3 class="h3 text-center">Falls Management Program</h3>
                                <div class="empty-space col-xs-b30"></div>
                                <p>
                                    Each session is 1 hour consisting of 45 minutes contact and 15 minutes for documentation
                                </p>
                                <p>
                                    Total package includes: 6 hours therapy time + materials + time needed to arrange equipment and contractors 
                                </p>
                                <ul>
                                    <li>Session 1: Initial assessment and detailed falls history and environmental assessment (2 hours) </li>
                                    <li>Session 2: Environmental adaptations / equipment recommendations / options.</li>
                                    <li>Session 3: Education on falls prevention</li>
                                    <li>Session 4: Falls recovery + CGT manual handling.</li>
                                    <li>Session 5: Review after modifications complete, evaluate success of intervention and goals beyond therapy.</li>
                                </ul>
                                <p><a href="#contact-us" class="close-contact">Contact us for more details.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="button-close"></div>
                </div>
            </div>
            <div class="popup-content" data-rel="3">
                <div class="layer-close"></div>
                <div class="popup-container size-1">
                    <div class="popup-align">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src="images/pu-icon-homemod.png" />
                                </div>
                                <h3 class="h3 text-center">Environmental Modification Package</h3>
                                <div class="empty-space col-xs-b30"></div>
                                <p>
                                    Session 1 is 2.5 hours consisting of 2 hours contact and 30 minutes for documentation
                                </p>
                                <p>
                                    Session 2 to 4 are 1 hour each consisting of 45 minutes contact and 15 mins for documentation
                                </p>
                                <p>
                                    Total package includes: 4.5 to 5.5 hours of therapy time + materials + time needed to arrange equipment and contractors
                                </p>
                                <p>
                                    Environments include: Home, community space, public transport, work/ school
                                </p>
                                <ul>
                                    <li>Session 1: Initial assessment, falls hx and detail home environment: 2 hours</li>
                                    <li>Session 2: Environmental adaptations / equipment recommendations / options.</li>
                                    <li>Session 3: Review and training on equipment / modification use</li>
                                    <li>Session 4 (optional): Review if further adaptation is needed after 3rd session.</li>
                                </ul>
                                <p><a href="#contact-us" class="close-contact">Contact us for more details.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="button-close"></div>
                </div>
            </div>
            <div class="popup-content" data-rel="4">
                <div class="layer-close"></div>
                <div class="popup-container size-1">
                    <div class="popup-align">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src="images/pu-icon-chronicc.png" />
                                </div>
                                <h3 class="h3 text-center">Chronic Condition Management Program</h3>
                                <div class="empty-space col-xs-b30"></div>
                                <p>
                                    Session 1 is 2 hours consisting of 1.5 hours contact and 30 minutes for documentation
                                </p>
                                <p>
                                    Subsequent sessions are 1 hour each, consisting of 45 mins of contact and 15 minutes for documentation
                                </p>
                                <p>
                                    Total package includes: 12 hours of therapy time + materials
                                </p>
                                
                                <ul>
                                    <li>Session 1 Understand (Initial assessment) (domain - disease + process)</li>
                                    <li>Session 2 Empower (domain - internal + disease)</li>
                                    <li>Session 3 Embrace (domain - internal + disease)</li>
                                    <li>Session 4 Exchange - bad habits for good habits (domain - health behaviour + process)</li>
                                    <li>Session 5 Enable - Overcoming your own barriers  (domain - activities + process)</li>
                                    <li>Session 6 Incharge - Overcoming fears through doing (domain - activities + process)  </li>
                                    <li>Session 7 Explore - Finding your own support network and communication (domain - social interaction + process)</li>
                                    <li>Session 8 Explain - Educating you family and planning together (domain - resource + process)</li>
                                    <li>Session 9 Exceed - Damage control and post therapy goal planning (domain - internal + process)</li>
                                    
                                </ul>
                                <p><a href="#contact-us" class="close-contact">Contact us for more details.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="button-close"></div>
                </div>
            </div>
            <div class="popup-content" data-rel="5">
                <div class="layer-close"></div>
                <div class="popup-container size-1">
                    <div class="popup-align">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src="images/pu-icon-devicetrain.png" />
                                </div>
                                <h3 class="h3 text-center">Motorised Mobility Device Training</h3>
                                <div class="empty-space col-xs-b30"></div>
                                <p>
                                    Training package (not including pre-package assessment) starts from 3 hours therapy time and 45 minutes documentation time + materials + time needed to arrange equipment and contractors + follow up phone call and short teleconsultation.
                                </p>
                                <p>
                                    Environments include: Home, community space, public transport, work/ school
                                </p>
                                <p>
                                    Pre Assessment 1.5 hours and 30 minutes documentation (total 2 hours)
                                </p>
                                <p>
                                    Training Session 1 to 3
                                </p>
                                <p><a href="#contact-us" class="close-contact">Contact us for more details.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="button-close"></div>
                </div>
            </div>
            <div class="popup-content" data-rel="6">
                <div class="layer-close"></div>
                <div class="popup-container size-1">
                    <div class="popup-align">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src="images/pu-icon-techt.png" />
                                </div>
                                <h3 class="h3 text-center">Therapy Technician Maintenance Package</h3>
                                <div class="empty-space col-xs-b30"></div>
                                <p>
                                    The purpose of our Therapy Technician Maintenance Package is to continue therapy sessions with the goal to:
                                </p>
                                <ul>
                                    <li>Continue facilitating meaningful engagement on a regular basis (for example, a person with dementia)</li>
                                    <li>Support caregiver in facilitating activities with patient (for example, after caregiver training completed)</li>
                                    <li>Continue to practise strategies prescribed by therapist </li>
                                </ul>
                                <p>
                                    This is not not solely to provide respite for caregivers as there must be a meaningful engagement goal for the patient in these sessions.
                                </p>
                                <p>
                                    Package of 10 one hour sessions, consists of 45 minutes of therapy time and 15 minutes documentation per session.
                                </p>
                                <p>
                                    2 sessions with main therapist and 8 therapy assistant sessions + tele consultation + materials for therapy activities.
                                </p>
                                <ul>
                                    <li>Session 1: Therapist and therapy technician start project with patient. Therapist plans timing for mid way review after the 5th session via teleconsultation. </li>
                                    <li>Session 2-9: Therapy technician facilitates project or activity with patient </li>
                                    <li>Session 5: Therapist will call family or patient to follow up on progress.</li>
                                    <li>Session 10: Therapist completes review. Option to extend package.</li>
                                </ul>
                                <p><a href="#contact-us" class="close-contact">Contact us for more details.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="button-close"></div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/swiper.jquery.min.js"></script>
    <script src="pub/components/sweetalert/dist/sweetalert.min.js"></script>
    <script src="js/global.js"></script>

    <!-- styled select -->
    <script src="js/jquery.sumoselect.min.js"></script>

    <!-- counter -->
    <script src="js/jquery.classycountdown.js"></script>
    <script src="js/jquery.knob.js"></script>
    <script src="js/jquery.throttle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>

</body>
</html>
